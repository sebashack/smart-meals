CREATE TABLE restaurant (
    id            UUID PRIMARY KEY,
    admin_id      UUID NOT NULL,
    name          VARCHAR(45) NOT NULL,
    email         VARCHAR(45) NOT NULL,
    city          VARCHAR(45) NOT NULL,
    country       VARCHAR(45) NOT NULL,
    address       VARCHAR(45) NOT NULL,
    phone_number  VARCHAR(45) NOT NULL,
    description   VARCHAR NOT NULL,
    created_at    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    updated_at    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT fk_admin FOREIGN KEY(admin_id) REFERENCES xuser(id)
);
