CREATE TABLE menu (
    id            UUID PRIMARY KEY,
    restaurant_id UUID NOT NULL UNIQUE,
    title         VARCHAR NOT NULL,
    created_at    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    updated_at    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT    fk_restaurant FOREIGN KEY(restaurant_id) REFERENCES restaurant(id) ON DELETE CASCADE
);

CREATE TABLE meal_type (
    id            UUID PRIMARY KEY,
    menu_id       UUID NOT NULL,
    title         VARCHAR(80) NOT NULL,
    created_at    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    updated_at    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT    fk_menu FOREIGN KEY(menu_id) REFERENCES menu(id) ON DELETE CASCADE
);
