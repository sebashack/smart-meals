CREATE TABLE xuser (
    id            UUID PRIMARY KEY,
    email         VARCHAR(45) NOT NULL UNIQUE,
    name          VARCHAR(45) NOT NULL,
    password      BYTEA NOT NULL,
    jwk           BYTEA NULL,
    country       VARCHAR(45) NOT NULL,
    city          VARCHAR(45) NOT NULL,
    is_admin      BOOLEAN NOT NULL DEFAULT false,
    birthdate     DATE NOT NULL,
    created_at    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    updated_at    TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')
);
