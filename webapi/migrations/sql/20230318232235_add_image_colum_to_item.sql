ALTER TABLE item
    ADD COLUMN image_id    UUID NULL,
    ADD CONSTRAINT fk_image FOREIGN KEY(image_id) REFERENCES image(id);
