ALTER TABLE restaurant
    ADD COLUMN background_image_id  UUID NULL,
    ADD COLUMN logo_id              UUID NULL,
    ADD CONSTRAINT fk_bg_image FOREIGN KEY(background_image_id) REFERENCES image(id),
    ADD CONSTRAINT fk_logo_image FOREIGN KEY(logo_id) REFERENCES image(id);
