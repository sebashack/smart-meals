CREATE TABLE review_like (
    id          UUID PRIMARY KEY,
    is_active  BOOLEAN NOT NULL DEFAULT true,
    user_id     UUID NOT NULL,
    review_id   UUID NOT NULL,
    created_at  TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    updated_at  TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT  fk_item FOREIGN KEY(review_id) REFERENCES review(id),
    CONSTRAINT  fk_user FOREIGN KEY(user_id) REFERENCES xuser(id),
    UNIQUE (user_id, review_id)
);

