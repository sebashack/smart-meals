use crate::crud::connection::DbConnection;
use crate::utils::jwt::AdminApiAuth;
use rocket::data::{Data, ToByteUnit};
use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::uuid::Uuid;
use rocket::Route;
use std::collections::HashMap;

use crate::utils::image::decode_into_jpeg_buffer;

use super::{
    Item, ItemWithScore, MealType, Menu, MenuWithItems, PostItemPayload, PostMenuPayload,
    PutItemPayload, ScoreFilter,
};
use crate::handlers::restaurant::Restaurant;

#[post(
    "/restaurants/<restaurant_id>/menus",
    format = "json",
    data = "<payload>"
)]
async fn post_menu(
    mut db: DbConnection,
    auth: AdminApiAuth,
    restaurant_id: Uuid,
    payload: Json<PostMenuPayload>,
) -> Result<status::Created<Json<Menu>>, (Status, String)> {
    match Restaurant::from_id(&mut db, restaurant_id).await {
        Some(restaurant) => match Menu::from_payload(auth.user_id, restaurant, payload) {
            Some(menu) => Ok(menu.create(&mut db).await),
            None => Err((Status::Unauthorized, "Unauthorized".to_string())),
        },
        None => Err((Status::NotFound, "Restaurant not found".to_string())),
    }
}

#[post(
    "/restaurants/menus/meal-types/<meal_type_id>/items/add",
    format = "json",
    data = "<payload>"
)]
async fn post_item(
    mut db: DbConnection,
    auth: AdminApiAuth,
    meal_type_id: Uuid,
    payload: Json<PostItemPayload>,
) -> Result<status::Created<Json<Item>>, (Status, String)> {
    if MealType::belongs_to_admin(&mut db, &meal_type_id, &auth.user_id).await {
        let item = Item::from_payload_item(payload, meal_type_id);
        Ok(item.create(&mut db).await)
    } else {
        Err((Status::NotFound, "Meal Type not found".to_string()))
    }
}

#[delete("/restaurants/menus/<menu_id>")]
async fn delete_menu(
    mut db: DbConnection,
    auth: AdminApiAuth,
    menu_id: Uuid,
) -> Result<(), (Status, String)> {
    if Menu::belongs_to_admin(&mut db, &menu_id, &auth.user_id).await {
        Menu::remove(&mut db, &menu_id).await;
        Ok(())
    } else {
        Err((Status::NotFound, "Menu not found".to_string()))
    }
}

#[delete("/restaurants/menus/meal-types/<meal_type_id>")]
async fn delete_meal_type(
    mut db: DbConnection,
    auth: AdminApiAuth,
    meal_type_id: Uuid,
) -> Result<(), (Status, String)> {
    if MealType::belongs_to_admin(&mut db, &meal_type_id, &auth.user_id).await {
        MealType::remove(&mut db, &meal_type_id).await;
        Ok(())
    } else {
        Err((Status::NotFound, "MealType not found".to_string()))
    }
}

#[delete("/restaurants/menus/meal-types/items/<item_id>")]
async fn delete_item(
    mut db: DbConnection,
    auth: AdminApiAuth,
    item_id: Uuid,
) -> Result<(), (Status, String)> {
    if Item::belongs_to_admin(&mut db, &item_id, &auth.user_id).await {
        Item::remove(&mut db, item_id).await;
        Ok(())
    } else {
        Err((Status::NotFound, "Item not found".to_string()))
    }
}

#[put("/restaurants/menus/<menu_id>/<title>", format = "json")]
async fn put_menu_title(
    mut db: DbConnection,
    auth: AdminApiAuth,
    menu_id: Uuid,
    title: String,
) -> Result<(), (Status, String)> {
    if Menu::belongs_to_admin(&mut db, &menu_id, &auth.user_id).await {
        Menu::update(&mut db, menu_id, title).await;
        Ok(())
    } else {
        Err((Status::NotFound, "Menu not found".to_string()))
    }
}

#[post("/restaurants/menus/<menu_id>/meal-types/add/<title>")]
async fn post_meal_type(
    mut db: DbConnection,
    auth: AdminApiAuth,
    menu_id: Uuid,
    title: String,
) -> Result<(), (Status, String)> {
    if Menu::belongs_to_admin(&mut db, &menu_id, &auth.user_id).await {
        let meal_type = MealType {
            id: Uuid::new_v4(),
            menu_id,
            title,
        };
        meal_type.create(&mut db).await;
        Ok(())
    } else {
        Err((Status::NotFound, "Menu not found".to_string()))
    }
}

#[put(
    "/restaurants/menus/meal-types/<meal_type_id>/<title>",
    format = "json"
)]
async fn put_meal_type(
    mut db: DbConnection,
    auth: AdminApiAuth,
    meal_type_id: Uuid,
    title: String,
) -> Result<(), (Status, String)> {
    if MealType::belongs_to_admin(&mut db, &meal_type_id, &auth.user_id).await {
        MealType::update(&mut db, meal_type_id, title).await;
        Ok(())
    } else {
        Err((Status::NotFound, "Mealtype not found".to_string()))
    }
}

#[put(
    "/restaurants/menu/meal-types/items/<meal_type_id>/<item_id>",
    format = "json",
    data = "<payload>"
)]
async fn put_item(
    mut db: DbConnection,
    auth: AdminApiAuth,
    item_id: Uuid,
    meal_type_id: Uuid,
    payload: Json<PutItemPayload>,
) -> Result<(), (Status, String)> {
    if Item::belongs_to_admin(&mut db, &item_id, &auth.user_id).await {
        Item::update(&mut db, item_id, payload, meal_type_id).await;
        Ok(())
    } else {
        Err((Status::NotFound, "Restaurant not found".to_string()))
    }
}

#[get("/restaurants/<restaurant_id>/menus", format = "json")]
async fn get_menu_with_items(
    mut db: DbConnection,
    restaurant_id: Uuid,
) -> Result<Json<MenuWithItems>, (Status, String)> {
    if Restaurant::exist(&mut db, &restaurant_id).await {
        match Menu::from_id(&mut db, &restaurant_id).await {
            Ok(menu) => {
                let meal_types = MealType::from_id(&mut db, &menu.id).await;
                let hash_map_items =
                    Item::get_mealtypes_with_items(&mut db, meal_types.clone()).await;

                Ok(Json(MenuWithItems {
                    menu: Menu {
                        id: menu.id,
                        restaurant_id: menu.restaurant_id,
                        title: menu.title,
                        meal_types,
                    },
                    items: hash_map_items,
                }))
            }
            Err(_) => Err((Status::NotFound, "Menu not found".to_string())),
        }
    } else {
        Err((Status::NotFound, "Restaurant not found".to_string()))
    }
}

#[put("/restaurants/menu/meal-types/items/<item_id>", data = "<raw_image>")]
async fn put_item_image(
    mut db: DbConnection,
    auth: AdminApiAuth,
    item_id: Uuid,
    raw_image: Data<'_>,
) -> Result<Json<Uuid>, (Status, String)> {
    if Item::belongs_to_admin(&mut db, &item_id, &auth.user_id).await {
        let stream = raw_image.open(3.megabytes());
        let mut buffer = vec![];
        stream.stream_to(&mut buffer).await.unwrap();

        if let Some(jpeg) = decode_into_jpeg_buffer(buffer) {
            let img_id = Item::store_image(&mut db, item_id, jpeg).await;
            Ok(Json(img_id))
        } else {
            Err((
                Status::BadRequest,
                "Invalid image. Must be one [JPEG, PNG] maximum 3MB size".to_string(),
            ))
        }
    } else {
        Err((Status::NotFound, "Restaurant not found".to_string()))
    }
}

#[get("/restaurants/menus/meal-types/<meal_type_id>/global-search/items/<global_average>")]
async fn get_items_by_global_average(
    mut db: DbConnection,
    meal_type_id: Uuid,
    global_average: f64,
) -> Result<Json<Vec<ItemWithScore>>, (Status, String)> {
    MealType::get_items_with_average_scores(
        &mut db,
        &meal_type_id,
        global_average,
        global_average + 1.0,
    )
    .await
}

#[get("/restaurants/menus/meal-types/<meal_type_id>/items?<scores>")]
async fn get_items_by_filters(
    mut db: DbConnection,
    meal_type_id: Uuid,
    scores: Vec<ScoreFilter>,
) -> Result<Json<Vec<ItemWithScore>>, (Status, String)> {
    MealType::get_items_with_average_scores_by_filter(&mut db, &meal_type_id, scores).await
}

#[get("/restaurants/menus/items/<item_id>/global-scores", format = "json")]
async fn get_item_global_scores(
    mut db: DbConnection,
    item_id: Uuid,
) -> Result<Json<HashMap<String, f64>>, (Status, String)> {
    Item::get_category_averages(&mut db, item_id).await
}

#[get(
    "/restaurants/menus/meal-types/<meal_type_id>/global-scores",
    format = "json"
)]
async fn get_meal_type_global_scores(
    mut db: DbConnection,
    meal_type_id: Uuid,
) -> Result<Json<HashMap<String, f64>>, (Status, String)> {
    MealType::get_category_averages(&mut db, meal_type_id).await
}

#[get("/restaurants/menus/<menu_id>/global-scores", format = "json")]
async fn get_menu_global_scores(
    mut db: DbConnection,
    menu_id: Uuid,
) -> Result<Json<HashMap<String, f64>>, (Status, String)> {
    Menu::get_category_averages(&mut db, menu_id).await
}

pub fn menu_routes() -> Vec<Route> {
    routes![
        delete_item,
        delete_meal_type,
        delete_menu,
        get_item_global_scores,
        get_items_by_filters,
        get_items_by_global_average,
        get_menu_with_items,
        get_menu_global_scores,
        get_meal_type_global_scores,
        post_item,
        post_meal_type,
        post_menu,
        put_item,
        put_item_image,
        put_meal_type,
        put_menu_title,
    ]
}
