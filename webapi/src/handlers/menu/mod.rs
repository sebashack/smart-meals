pub mod api;

use crate::crud::connection::PoolConnectionPtr;

use rocket::form::error::ErrorKind;
use rocket::form::{self, FromFormField, ValueField};
use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::uuid::Uuid;
use rocket::serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::hash::{Hash, Hasher};

use crate::crud::image;
use crate::crud::menu::{self, MenuSql};
use crate::handlers::restaurant::Restaurant;
use crate::handlers::review::Category;
use crate::rocket::http::uncased::AsUncased;

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct MealType {
    pub id: Uuid,
    pub menu_id: Uuid,
    pub title: String,
}

impl MealType {
    // Given a JSON payload, return new MealType.
    pub fn from_payload(menu_id: Uuid, meal_types_str: Vec<String>) -> Vec<Self> {
        meal_types_str
            .into_iter()
            .map(|title| MealType {
                id: Uuid::new_v4(),
                menu_id,
                title,
            })
            .collect()
    }

    // Retrieve all MealTypes associated to a menu_id.
    pub async fn from_id(db: &mut PoolConnectionPtr, menu_id: &Uuid) -> Vec<Self> {
        let meal_types_sql = menu::select_meal_types_by_menu_id(db, menu_id).await;

        meal_types_sql
            .into_iter()
            .map(|mt| MealType {
                id: mt.id,
                menu_id: mt.menu_id,
                title: mt.title,
            })
            .collect()
    }

    // Insert MealType in database.
    pub async fn create(&self, db: &mut PoolConnectionPtr) {
        menu::insert_meal_type(db, self).await;
    }

    // Update MealType title.
    pub async fn update(db: &mut PoolConnectionPtr, mealtype_id: Uuid, title: String) {
        menu::update_meal_type(db, &mealtype_id, title.as_str()).await;
    }

    // Remove MealType from database.
    pub async fn remove(db: &mut PoolConnectionPtr, meal_type_id: &Uuid) {
        menu::delete_meal_type(db, meal_type_id).await;
    }

    // Determine if this MealType blongs to admin with admin_id.
    pub async fn belongs_to_admin(
        db: &mut PoolConnectionPtr,
        meal_type_id: &Uuid,
        admin_id: &Uuid,
    ) -> bool {
        menu::select_if_mealtype_belongs_to_admin(db, meal_type_id, admin_id).await
    }

    // Given a meal_type_id return all of its related items within a range together with their average score in all categories.
    pub async fn get_items_with_average_scores(
        db: &mut PoolConnectionPtr,
        meal_type_id: &Uuid,
        min_score: f64,
        up_bound: f64,
    ) -> Result<Json<Vec<ItemWithScore>>, (Status, String)> {
        if menu::select_if_meal_type_exists(db, meal_type_id).await {
            Ok(Json(
                menu::select_items_by_global_score(db, meal_type_id, min_score, up_bound).await,
            ))
        } else {
            Err((Status::NotFound, "Meal type not found".to_string()))
        }
    }

    pub async fn get_items_with_average_scores_by_filter(
        db: &mut PoolConnectionPtr,
        meal_type_id: &Uuid,
        scores: Vec<ScoreFilter>,
    ) -> Result<Json<Vec<ItemWithScore>>, (Status, String)> {
        if menu::select_if_meal_type_exists(db, meal_type_id).await {
            let mut unique_scores = HashSet::new();

            for s in scores {
                unique_scores.insert(s);
            }

            let mut unique_scores: Vec<ScoreFilter> = unique_scores.into_iter().collect();

            unique_scores.sort_by(|s1, s2| s1.0.partial_cmp(&s2.0).unwrap());

            let categories: Vec<Category> = unique_scores.iter().map(|s| s.0).collect();
            let items = menu::select_items_with_score_by_meal_type_id(db, meal_type_id).await;
            let mut filtered_items = Vec::new();

            for it in items.into_iter() {
                let mut avgs = menu::select_item_averages(db, &it.id, categories.as_slice()).await;
                avgs.sort_by(|a1, a2| a1.category.partial_cmp(&a2.category).unwrap());
                let mut passes_all_filters = true;

                for i in 0..avgs.len() {
                    let category = unique_scores[i].0;
                    let score = unique_scores[i].1;

                    assert!(category == avgs[i].category);

                    if !(avgs[i].score >= score && avgs[i].score < score + 1.0) {
                        passes_all_filters = false;
                        break;
                    }
                }

                if passes_all_filters && !avgs.is_empty() {
                    filtered_items.push(it);
                }
            }

            Ok(Json(filtered_items))
        } else {
            Err((Status::NotFound, "Meal type not found".to_string()))
        }
    }

    pub async fn get_category_averages(
        db: &mut PoolConnectionPtr,
        meal_type_id: Uuid,
    ) -> Result<Json<HashMap<String, f64>>, (Status, String)> {
        if menu::select_if_meal_type_exists(db, &meal_type_id).await {
            let mut avgs = HashMap::new();

            let quality_avg =
                menu::select_meal_type_category_average(db, &meal_type_id, Category::Quality).await;
            let healthiness_avg =
                menu::select_meal_type_category_average(db, &meal_type_id, Category::Healthiness)
                    .await;
            let fairness_avg =
                menu::select_meal_type_category_average(db, &meal_type_id, Category::PriceFairness)
                    .await;
            let ecology_avg =
                menu::select_meal_type_category_average(db, &meal_type_id, Category::Ecology).await;

            avgs.insert("quality".to_string(), quality_avg.score);
            avgs.insert("healthiness".to_string(), healthiness_avg.score);
            avgs.insert("price_fairness".to_string(), fairness_avg.score);
            avgs.insert("ecology".to_string(), ecology_avg.score);

            Ok(Json(avgs))
        } else {
            Err((Status::NotFound, "Meal Type not found".to_string()))
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Menu {
    pub id: Uuid,
    pub restaurant_id: Uuid,
    pub title: String,
    pub meal_types: Vec<MealType>,
}

impl Menu {
    // Given a JSON payload, return new Menu.
    pub fn from_payload(
        admin_id: Uuid,
        restaurant: Restaurant,
        p: Json<PostMenuPayload>,
    ) -> Option<Self> {
        if restaurant.admin_id == admin_id {
            let id = Uuid::new_v4();
            let meal_types = MealType::from_payload(id, p.meal_types.clone());
            Some(Menu {
                id,
                restaurant_id: restaurant.id,
                title: p.title.clone(),
                meal_types,
            })
        } else {
            None
        }
    }

    // Retrieve menu from database by restaurant_id, and return it.
    pub async fn from_id(
        db: &mut PoolConnectionPtr,
        restaurant_id: &Uuid,
    ) -> Result<MenuSql, (Status, String)> {
        match menu::select_menu_by_restaurant_id(db, restaurant_id).await {
            Some(menu) => Ok(menu),
            None => Err((Status::NotFound, "Menu not found".to_string())),
        }
    }

    // Insert Menu in database.
    pub async fn create(&self, db: &mut PoolConnectionPtr) -> status::Created<Json<Menu>> {
        menu::insert_menu(db, self).await;

        for meal_type in self.meal_types.iter() {
            meal_type.create(db).await;
        }

        status::Created::new("/restaurants/menus").body(Json(self.clone()))
    }

    // Update Menu title.
    pub async fn update(db: &mut PoolConnectionPtr, menu_id: Uuid, title: String) {
        menu::update_menu_title(db, &menu_id, title.as_str()).await;
    }

    // Determine if this Menu belongs to admin with admin_id.
    pub async fn belongs_to_admin(
        db: &mut PoolConnectionPtr,
        menu_id: &Uuid,
        admin_id: &Uuid,
    ) -> bool {
        menu::select_if_menu_belongs_to_admin(db, menu_id, admin_id).await
    }

    // Remove menu from database.
    pub async fn remove(db: &mut PoolConnectionPtr, menu_id: &Uuid) {
        menu::delete_menu(db, menu_id).await;
    }

    pub async fn get_category_averages(
        db: &mut PoolConnectionPtr,
        menu_id: Uuid,
    ) -> Result<Json<HashMap<String, f64>>, (Status, String)> {
        if menu::select_if_menu_exists(db, &menu_id).await {
            let mut avgs = HashMap::new();

            let quality_avg =
                menu::select_category_average(db, &menu_id, Category::Quality).await;
            let healthiness_avg =
                menu::select_category_average(db, &menu_id, Category::Healthiness).await;
            let fairness_avg =
                menu::select_category_average(db, &menu_id, Category::PriceFairness).await;
            let ecology_avg =
                menu::select_category_average(db, &menu_id, Category::Ecology).await;

            avgs.insert("quality".to_string(), quality_avg.score);
            avgs.insert("healthiness".to_string(), healthiness_avg.score);
            avgs.insert("price_fairness".to_string(), fairness_avg.score);
            avgs.insert("ecology".to_string(), ecology_avg.score);

            Ok(Json(avgs))
        } else {
            Err((Status::NotFound, "Menu not found".to_string()))
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Item {
    pub id: Uuid,
    pub title: String,
    pub image_id: Option<Uuid>,
    pub description: String,
    pub price: i64,
    pub meal_type_id: Uuid,
}

impl Item {
    // Given a JSON payload, return new Item.
    pub fn from_payload_item(p: Json<PostItemPayload>, meal_type_id: Uuid) -> Self {
        let price = p.price;
        Item {
            id: Uuid::new_v4(),
            title: p.title.clone(),
            image_id: None,
            description: p.description.clone(),
            price,
            meal_type_id,
        }
    }

    // Inser item in database.
    pub async fn create(&self, db: &mut PoolConnectionPtr) -> status::Created<Json<Item>> {
        menu::insert_item(db, self).await;
        status::Created::new("/restaurants/menus/meal-types/items").body(Json(self.clone()))
    }

    // Determine if this Item blongs to admin with admin_id.
    pub async fn belongs_to_admin(
        db: &mut PoolConnectionPtr,
        item_id: &Uuid,
        admin_id: &Uuid,
    ) -> bool {
        menu::select_if_item_belongs_to_admin(db, item_id, admin_id).await
    }

    // Remove item from database.
    pub async fn remove(db: &mut PoolConnectionPtr, item_id: Uuid) {
        menu::delete_item(db, &item_id).await;
    }

    // Given a PutItemPayload, update the item's properties.
    pub async fn update(
        db: &mut PoolConnectionPtr,
        item_id: Uuid,
        p: Json<PutItemPayload>,
        meal_type_id: Uuid,
    ) {
        menu::update_item(db, &meal_type_id, &item_id, p).await;
    }

    pub async fn get_category_averages(
        db: &mut PoolConnectionPtr,
        item_id: Uuid,
    ) -> Result<Json<HashMap<String, f64>>, (Status, String)> {
        if menu::select_if_item_exists(db, &item_id).await {
            let mut avgs = HashMap::new();

            let quality_avg =
                menu::select_item_category_average(db, &item_id, Category::Quality).await;
            let healthiness_avg =
                menu::select_item_category_average(db, &item_id, Category::Healthiness).await;
            let fairness_avg =
                menu::select_item_category_average(db, &item_id, Category::PriceFairness).await;
            let ecology_avg =
                menu::select_item_category_average(db, &item_id, Category::Ecology).await;

            avgs.insert("quality".to_string(), quality_avg.score);
            avgs.insert("healthiness".to_string(), healthiness_avg.score);
            avgs.insert("price_fairness".to_string(), fairness_avg.score);
            avgs.insert("ecology".to_string(), ecology_avg.score);

            Ok(Json(avgs))
        } else {
            Err((Status::NotFound, "Item not found".to_string()))
        }
    }

    // Given a vector of MealTypes, return a HashMap from  MealType Uuid to the vectors of items that belong to it.
    pub async fn get_mealtypes_with_items(
        db: &mut PoolConnectionPtr,
        meal_types: Vec<MealType>,
    ) -> HashMap<Uuid, Vec<ItemWithScore>> {
        let mut mealtypes_to_items: HashMap<Uuid, Vec<ItemWithScore>> = HashMap::new();

        for meal_type in meal_types {
            let items = menu::select_items_with_score_by_meal_type_id(db, &meal_type.id).await;
            mealtypes_to_items.insert(meal_type.id, items);
        }

        mealtypes_to_items
    }

    // Store an item's image as a binary buffer in database.
    pub async fn store_image(db: &mut PoolConnectionPtr, item_id: Uuid, buffer: Vec<u8>) -> Uuid {
        let image_id = Uuid::new_v4();
        let previous_id = menu::select_item_image_id(db, &item_id).await;

        image::insert(db, &image_id, buffer).await;
        menu::update_item_image(db, &item_id, &image_id).await;

        if let Some(id) = previous_id {
            image::delete(db, &id).await;
        }

        image_id
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PostMealTypePayload {
    pub title: String,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PutMealTypePayload {
    pub title: String,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PostMenuPayload {
    pub title: String,
    pub meal_types: Vec<String>,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PutMenuPayload {
    pub title: String,
    pub meal_types: Vec<MealType>,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PostItemPayload {
    title: String,
    description: String,
    price: i64,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PutItemPayload {
    pub title: String,
    pub description: String,
    pub price: i64,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct MenuWithItems {
    menu: Menu,
    items: HashMap<Uuid, Vec<ItemWithScore>>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(crate = "rocket::serde")]
pub struct ItemWithScore {
    pub id: Uuid,
    pub title: String,
    pub image_id: Option<Uuid>,
    pub description: String,
    pub price: i64,
    pub meal_type_id: Uuid,
    pub global_score: f64,
}

#[derive(Debug)]
pub struct ScoreFilter(Category, f64);

impl<'r> FromFormField<'r> for ScoreFilter {
    fn from_value(field: ValueField<'r>) -> form::Result<'r, Self> {
        let v = field.value.as_uncased().to_string();
        if v.contains('-') {
            let parts: Vec<&str> = v.split('-').collect();
            if parts.len() == 2 {
                if let Ok(score) = parts[1].parse() {
                    let category = parts[0];
                    match category {
                        "quality" => Ok(ScoreFilter(Category::Quality, score)),
                        "healthiness" => Ok(ScoreFilter(Category::Healthiness, score)),
                        "pricefairness" => Ok(ScoreFilter(Category::PriceFairness, score)),
                        "ecology" => Ok(ScoreFilter(Category::Ecology, score)),
                        _ => Err(ErrorKind::Unexpected.into()),
                    }
                } else {
                    Err(ErrorKind::Unexpected.into())
                }
            } else {
                Err(ErrorKind::Unexpected.into())
            }
        } else {
            Err(ErrorKind::Unexpected.into())
        }
    }
}

impl PartialEq for ScoreFilter {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl Eq for ScoreFilter {}

impl Hash for ScoreFilter {
    fn hash<H>(&self, s: &mut H)
    where
        H: Hasher,
    {
        (self.0 as i16).hash(s)
    }
}
