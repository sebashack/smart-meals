pub mod api;

use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::uuid::Uuid;
use rocket::serde::{Deserialize, Serialize};

use crate::crud::connection::PoolConnectionPtr;
use crate::crud::image;
use crate::crud::restaurant;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct Restaurant {
    pub id: Uuid,
    pub admin_id: Uuid,
    pub name: String,
    pub email: String,
    pub country: String,
    pub city: String,
    pub address: String,
    pub phone_number: String,
    pub description: String,
    pub background_image_id: Option<Uuid>,
    pub logo_id: Option<Uuid>,
}

impl Restaurant {
    // Given a Json payload, return a new Restaurant.
    pub fn from_payload(admin_id: Uuid, p: Json<PostRestaurantPayload>) -> Self {
        Restaurant {
            id: Uuid::new_v4(),
            admin_id,
            name: p.name.clone(),
            email: p.email.clone(),
            country: p.country.clone(),
            city: p.city.clone(),
            address: p.address.clone(),
            phone_number: p.phone_number.clone(),
            description: p.description.clone(),
            background_image_id: None,
            logo_id: None,
        }
    }

    // Given an id, retrieve its associated restaurant from database and return it if it exists.
    pub async fn from_id(db: &mut PoolConnectionPtr, id: Uuid) -> Option<Restaurant> {
        restaurant::select(db, &id).await
    }

    // Insert restaurant in database.
    pub async fn create(&self, db: &mut PoolConnectionPtr) -> status::Created<Json<Restaurant>> {
        restaurant::insert(db, self).await;
        status::Created::new("/restaurants").body(Json(self.clone()))
    }

    // Given a PutRestaurantPayload, update restaurant attributes.
    pub async fn update(
        db: &mut PoolConnectionPtr,
        restaurant_id: Uuid,
        payload: Json<PutRestaurantPayload>,
    ) {
        restaurant::update(db, restaurant_id, payload).await;
    }

    // Determine if this Restaurant blongs to admin with admin_id.
    pub async fn belongs_to_admin(
        db: &mut PoolConnectionPtr,
        restaurant_id: &Uuid,
        admin_id: &Uuid,
    ) -> bool {
        restaurant::select_if_belongs_to_admin(db, restaurant_id, admin_id).await
    }

    // Determine restaurant exists in databse.
    pub async fn exist(db: &mut PoolConnectionPtr, restaurant_id: &Uuid) -> bool {
        restaurant::select_if_exists(db, restaurant_id).await
    }

    // Remove restaurant from database.
    pub async fn remove(db: &mut PoolConnectionPtr, restaurant_id: &Uuid) {
        restaurant::delete(db, restaurant_id).await;
    }

    // Given an restaurant_id return its admin_id.
    pub async fn get_admin_id(db: &mut PoolConnectionPtr, restaurant_id: Uuid) -> Option<Uuid> {
        match restaurant::select(db, &restaurant_id).await {
            Some(restaurantsql) => Some(restaurantsql.admin_id),
            None => None,
        }
    }

    // Given an admin_id return their associated restaurants.
    pub async fn get_admin_restaurants(
        db: &mut PoolConnectionPtr,
        admin_id: Uuid,
    ) -> Vec<Restaurant> {
        restaurant::select_by_admin_id(db, &admin_id).await
    }

    // Given a text query return  restaurant matches.
    pub async fn get_restaurants_by_word(
        db: &mut PoolConnectionPtr,
        word: String,
        offset: Option<i64>,
        to: Option<i64>,
    ) -> Vec<Restaurant> {
        restaurant::select_restaurants_by_word(db, word, offset, to).await
    }

    // Store restaurant's background image as a binary buffer in database.
    pub async fn store_background_image(
        db: &mut PoolConnectionPtr,
        restaurant_id: &Uuid,
        buffer: Vec<u8>,
    ) -> Uuid {
        let image_id = Uuid::new_v4();
        let previous_id = restaurant::select_background_image_id(db, restaurant_id).await;

        image::insert(db, &image_id, buffer).await;
        restaurant::update_background_image(db, restaurant_id, &image_id).await;

        if let Some(id) = previous_id {
            image::delete(db, &id).await;
        }

        image_id
    }

    // Store restaurant's logo image as a binary buffer in database.
    pub async fn store_logo(
        db: &mut PoolConnectionPtr,
        restaurant_id: &Uuid,
        buffer: Vec<u8>,
    ) -> Uuid {
        let image_id = Uuid::new_v4();
        let previous_id = restaurant::select_logo_id(db, restaurant_id).await;

        image::insert(db, &image_id, buffer).await;
        restaurant::update_logo(db, restaurant_id, &image_id).await;

        if let Some(id) = previous_id {
            image::delete(db, &id).await;
        }

        image_id
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PostRestaurantPayload {
    pub name: String,
    pub email: String,
    pub country: String,
    pub city: String,
    pub address: String,
    pub phone_number: String,
    pub description: String,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct PutRestaurantPayload {
    pub name: String,
    pub email: String,
    pub country: String,
    pub city: String,
    pub address: String,
    pub phone_number: String,
    pub description: String,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct RestaurantWithScore {
    pub restaurant: Restaurant,
    pub score: Option<f64>,
}
