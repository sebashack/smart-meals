use rocket::data::{Data, ToByteUnit};
use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::uuid::Uuid;
use rocket::Route;
use std::collections::HashMap;

use super::{PostRestaurantPayload, PutRestaurantPayload, Restaurant};
use crate::crud::connection::DbConnection;
use crate::utils::image::decode_into_jpeg_buffer;
use crate::utils::jwt::AdminApiAuth;

#[post("/restaurants", format = "json", data = "<payload>")]
async fn post_restaurant(
    mut db: DbConnection,
    auth: AdminApiAuth,
    payload: Json<PostRestaurantPayload>,
) -> status::Created<Json<Restaurant>> {
    let restaurant = Restaurant::from_payload(auth.user_id, payload);
    restaurant.create(&mut db).await
}

#[get("/restaurants/<restaurant_id>", format = "json")]
async fn get_restaurant(
    mut db: DbConnection,
    restaurant_id: Uuid,
) -> Result<Json<Restaurant>, (Status, String)> {
    match Restaurant::from_id(&mut db, restaurant_id).await {
        Some(restaurant) => Ok(Json(restaurant)),
        None => Err((Status::NotFound, "Restaurant not found".to_string())),
    }
}

#[get("/restaurants", format = "json")]
async fn get_admin_restaurants(mut db: DbConnection, auth: AdminApiAuth) -> Json<Vec<Restaurant>> {
    let restaurants = Restaurant::get_admin_restaurants(&mut db, auth.user_id).await;
    Json(restaurants)
}

#[put("/restaurants/<restaurant_id>", format = "json", data = "<payload>")]
async fn put_restaurant(
    mut db: DbConnection,
    auth: AdminApiAuth,
    restaurant_id: Uuid,
    payload: Json<PutRestaurantPayload>,
) -> Result<(), (Status, String)> {
    if Restaurant::belongs_to_admin(&mut db, &restaurant_id, &auth.user_id).await {
        Restaurant::update(&mut db, restaurant_id, payload).await;
        Ok(())
    } else {
        Err((Status::NotFound, "Restaurant not found".to_string()))
    }
}

#[delete("/restaurants/<restaurant_id>")]
async fn delete_restaurant(
    mut db: DbConnection,
    auth: AdminApiAuth,
    restaurant_id: Uuid,
) -> Result<(), (Status, String)> {
    if Restaurant::belongs_to_admin(&mut db, &restaurant_id, &auth.user_id).await {
        Restaurant::remove(&mut db, &restaurant_id).await;
        Ok(())
    } else {
        Err((Status::NotFound, "Restaurant not found".to_string()))
    }
}

#[get("/restaurants/<restaurant_id>/has-admin")]
async fn get_restaurant_has_admin(
    mut db: DbConnection,
    auth: AdminApiAuth,
    restaurant_id: Uuid,
) -> Result<Json<bool>, (Status, String)> {
    Ok(Json(
        Restaurant::belongs_to_admin(&mut db, &restaurant_id, &auth.user_id).await,
    ))
}

#[put("/restaurants/background-image/<restaurant_id>", data = "<raw_image>")]
async fn put_restaurant_background_image(
    mut db: DbConnection,
    auth: AdminApiAuth,
    restaurant_id: Uuid,
    raw_image: Data<'_>,
) -> Result<Json<Uuid>, (Status, String)> {
    if Restaurant::belongs_to_admin(&mut db, &restaurant_id, &auth.user_id).await {
        let stream = raw_image.open(3.megabytes());
        let mut buffer = vec![];
        stream.stream_to(&mut buffer).await.unwrap();

        if let Some(jpeg) = decode_into_jpeg_buffer(buffer) {
            let img_id = Restaurant::store_background_image(&mut db, &restaurant_id, jpeg).await;
            Ok(Json(img_id))
        } else {
            Err((
                Status::BadRequest,
                "Invalid image format. Must be one of [JPEG, PNG]".to_string(),
            ))
        }
    } else {
        Err((Status::NotFound, "Restaurant not found".to_string()))
    }
}

#[put("/restaurants/logo/<restaurant_id>", data = "<raw_image>")]
async fn put_restaurant_logo(
    mut db: DbConnection,
    auth: AdminApiAuth,
    restaurant_id: Uuid,
    raw_image: Data<'_>,
) -> Result<Json<Uuid>, (Status, String)> {
    if Restaurant::belongs_to_admin(&mut db, &restaurant_id, &auth.user_id).await {
        let stream = raw_image.open(3.megabytes());
        let mut buffer = vec![];
        stream.stream_to(&mut buffer).await.unwrap();

        if let Some(jpeg) = decode_into_jpeg_buffer(buffer) {
            let img_id = Restaurant::store_logo(&mut db, &restaurant_id, jpeg).await;
            Ok(Json(img_id))
        } else {
            Err((
                Status::BadRequest,
                "Invalid image. Must be one [JPEG, PNG] maximum 3MB size".to_string(),
            ))
        }
    } else {
        Err((Status::NotFound, "Restaurant not found".to_string()))
    }
}

#[get("/restaurants/search?<word>&<offset>&<to>", format = "json")]
async fn get_restaurants_by_word(
    mut db: DbConnection,
    word: String,
    offset: Option<i64>,
    to: Option<i64>,
) -> Json<Vec<Restaurant>> {
    let words: Vec<String> = word.split_whitespace().map(|s| s.to_string()).collect();
    let to_stem = |w: String| -> String {
        let w: String = w.chars().filter(|c| c.is_alphanumeric()).collect();
        if w.len() >= 6 {
            let (w, _) = w.split_at(4);
            w.to_lowercase()
        } else if w.len() >= 4 && w.len() <= 5 {
            let (w, _) = w.split_at(4);
            w.to_lowercase()
        } else {
            w.to_lowercase()
        }
    };
    let stems = words.into_iter().map(to_stem);
    let mut search_result: HashMap<Uuid, Restaurant> = HashMap::new();

    for s in stems {
        let restaurants = Restaurant::get_restaurants_by_word(&mut db, s, offset, to).await;

        for r in restaurants {
            search_result.insert(r.id.clone(), r);
        }
    }

    Json(search_result.into_values().collect())
}

pub fn restaurant_routes() -> Vec<Route> {
    routes![
        post_restaurant,
        get_restaurant,
        get_admin_restaurants,
        delete_restaurant,
        put_restaurant,
        put_restaurant_logo,
        put_restaurant_background_image,
        get_restaurant_has_admin,
        get_restaurants_by_word
    ]
}
