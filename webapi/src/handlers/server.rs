use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Header;
use rocket::http::{ContentType, Method, Status};
use rocket::response::status::NoContent;
use rocket::{Build, Rocket};
use rocket::{Request, Response};
use rocket_db_pools::Database;

use super::image::image_routes;
use super::menu::api::menu_routes;
use super::restaurant::api::restaurant_routes;
use super::review::api::review_routes;
use super::user::api::user_routes;
use crate::crud::connection::Db;

pub struct Cors;

#[rocket::async_trait]
impl Fairing for Cors {
    fn info(&self) -> Info {
        Info {
            name: "Cross-Origin-Resource-Sharing Fairing",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, PATCH, PUT, DELETE, HEAD, OPTIONS, GET",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new(
            "Access-Control-Expose-Headers",
            "*, Authorization, Content-Type, User",
        ));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));

        if request.method() == Method::Options {
            response.set_header(ContentType::Plain);
            response.set_sized_body(0, std::io::Cursor::new(""));
            response.set_status(Status::Ok);
        }
    }
}

#[get("/status")]
pub async fn status() -> &'static str {
    "Ok"
}

#[options("/<_..>")]
pub async fn options() -> NoContent {
    NoContent
}

#[catch(500)]
fn internal_error() -> &'static str {
    "Internal server error"
}

#[catch(400)]
fn bad_request(req: &Request) -> String {
    format!("Bad request '{}'", req.uri())
}

#[catch(404)]
fn not_found(req: &Request) -> String {
    format!("Resource not found '{}'", req.uri())
}

#[catch(403)]
fn forbidden(req: &Request) -> String {
    format!("Forbidden access '{}'", req.uri())
}

#[catch(401)]
fn unauthorized(req: &Request) -> String {
    format!("Unauthorized access '{}'", req.uri())
}

#[catch(default)]
fn default(status: Status, req: &Request) -> String {
    format!("Unknown error: {} ({})", status, req.uri())
}

pub fn build_server() -> Rocket<Build> {
    rocket::build()
        .attach(Db::init())
        .attach(Cors)
        .mount("/", routes![status, options])
        .mount("/", user_routes())
        .mount("/", restaurant_routes())
        .mount("/", menu_routes())
        .mount("/", image_routes())
        .mount("/", review_routes())
        .register(
            "/",
            catchers![
                internal_error,
                forbidden,
                unauthorized,
                not_found,
                bad_request,
                default
            ],
        )
}
