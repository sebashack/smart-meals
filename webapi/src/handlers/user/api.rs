use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::uuid::Uuid;
use rocket::Route;
use rocket_basicauth::BasicAuth;

use super::{HistoryReview, PostUserPayload, Recommendation, User, UserWithLikes};
use crate::crud::connection::DbConnection;
use crate::handlers::restaurant::RestaurantWithScore;
use crate::handlers::review::Category;
use crate::utils::jwt::{ApiAuth, EncodedJWT};
use std::collections::HashMap;
use crate::handlers::restaurant::Restaurant;

#[post("/users", format = "json", data = "<payload>")]
async fn post_user(
    mut db: DbConnection,
    payload: Json<PostUserPayload>,
) -> Result<status::Created<Json<EncodedJWT>>, (Status, String)> {
    let (user, hash) = User::from_payload(payload);
    user.create_and_generate_jwt(&mut db, hash).await
}

#[put("/users/auth")]
async fn basic_auth(mut db: DbConnection, auth: BasicAuth) -> Result<Json<User>, (Status, String)> {
    match User::from_email(&mut db, &auth.username).await {
        Some((mut user, hash)) => user.authenticate(&mut db, auth.password, hash).await,
        None => Err((Status::NotFound, "user not found".to_string())),
    }
}

#[get("/users/profile")]
async fn get_profile(mut db: DbConnection, auth: ApiAuth) -> Result<Json<User>, (Status, String)> {
    match User::from_id(&mut db, &auth.user_id).await {
        Some(user) => Ok(Json(user)),
        None => Err((Status::NotFound, "user not found".to_string())),
    }
}

#[delete("/users/auth")]
async fn delete_auth(mut db: DbConnection, auth: ApiAuth) {
    User::revoke_token(&mut db, &auth.user_id).await;
}

#[get("/users/reviews?<category>&<offset>&<to>")]
async fn get_user_reviews(
    mut db: DbConnection,
    auth: ApiAuth,
    category: Category,
    offset: Option<i64>,
    to: Option<i64>,
) -> Result<Json<Vec<HistoryReview>>, (Status, String)> {
    let reviews = User::get_reviews_by_category(&mut db, &auth.user_id, category, offset, to).await;
    Ok(Json(reviews))
}

#[get("/users/scores")]
async fn get_average_user_score_by_category(
    mut db: DbConnection,
    auth: ApiAuth,
) -> Result<Json<HashMap<String, f64>>, (Status, String)> {
    Ok(Json(User::get_average_score_by_category(&mut db, &auth.user_id).await))
}

#[get("/users/restaurants/ranking?<category>&<offset>&<to>")]
async fn get_restaurants_sorted_desc_by_category(
    mut db: DbConnection,
    auth: ApiAuth,
    category: Category,
    offset: Option<i64>,
    to: Option<i64>,
) -> Result<Json<Vec<RestaurantWithScore>>, (Status, String)> {
    let restaurants = User::get_restaurants_ordered_desc_by_category(
        &mut db,
        &auth.user_id,
        category,
        offset,
        to,
    )
    .await;
    Ok(Json(restaurants))
}

#[get("/users/recommendations?<category>&<offset>&<to>")]
async fn get_user_recommendation_by_category(
    mut db: DbConnection,
    auth: ApiAuth,
    category: Category,
    offset: Option<i64>,
    to: Option<i64>,
) -> Result<Json<Vec<Recommendation>>, (Status, String)> {
    let recommendations =
        User::get_recommendation_by_category(&mut db, &auth.user_id, category, offset, to).await;
    Ok(Json(recommendations))
}


#[get("/users/ranking?<restaurant_id>&<offset>&<to>")]
async fn get_user_ranking_by_restaurant(
    mut db: DbConnection,
    restaurant_id: Uuid,
    offset: Option<i64>,
    to: Option<i64>,
) -> Result<Json<Vec<UserWithLikes>>, (Status, String)> {
    if Restaurant::exist(&mut db, &restaurant_id).await {
        let ranking = User::get_ranking(&mut db, &restaurant_id, offset, to).await;
        Ok(Json(ranking))
    } else {
        Err((Status::NotFound, "Restaurant not found".to_string()))
    }
}

pub fn user_routes() -> Vec<Route> {
    routes![
        post_user,
        basic_auth,
        delete_auth,
        get_user_reviews,
        get_profile,
        get_user_ranking_by_restaurant,
        get_average_user_score_by_category,
        get_restaurants_sorted_desc_by_category,
        get_user_recommendation_by_category,
    ]
}
