pub mod api;
pub mod auth;

use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::uuid::Uuid;
use rocket::serde::{Deserialize, Serialize};
use sqlx;
use sqlx::FromRow;
use std::collections::HashMap;
use time::{serde, Date};

use super::user::auth::HashedPassword;
use crate::crud::connection::PoolConnectionPtr;
use crate::crud::review;
use crate::crud::user;
use crate::handlers::restaurant::RestaurantWithScore;
use crate::handlers::review::Category;
use crate::utils::jwt::{gen_jw_pair, EncodedJWT};

const TOKEN_EXP_SECS: usize = 86400 * 90; // 90 Days

// Format for JSON dates
serde::format_description!(date_format, Date, "[year]-[month]-[day]");
serde::format_description!(
    iso8601_format,
    PrimitiveDateTime,
    "[year]-[month]-[day]T[hour]:[minute]:[second]Z"
);

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct User {
    pub id: Uuid,
    pub email: String,
    pub name: String,
    pub is_admin: bool,
    pub country: String,
    pub city: String,
    #[serde(with = "date_format")]
    pub birthdate: Date,
    jwt: Option<EncodedJWT>,
}

impl User {
    // Given a JSON payload, return a User together with their hashed password.
    pub fn from_payload(p: Json<PostUserPayload>) -> (Self, HashedPassword) {
        let user = User {
            id: Uuid::new_v4(),
            email: p.email.clone(),
            name: p.name.clone(),
            is_admin: p.is_admin,
            country: p.country.clone(),
            city: p.city.clone(),
            birthdate: p.birthdate,
            jwt: None,
        };

        (user, HashedPassword::new(p.password.clone()))
    }

    // Given an email, retrieve a User together with their password from database.
    pub async fn from_email(
        db: &mut PoolConnectionPtr,
        email: &str,
    ) -> Option<(Self, HashedPassword)> {
        user::select_with_password_by_email(db, email)
            .await
            .map(|v| {
                let month = v.birthdate.month();
                let birthdate =
                    Date::from_calendar_date(v.birthdate.year(), month, v.birthdate.day()).unwrap();

                let user = User {
                    id: v.id,
                    email: v.email,
                    name: v.name,
                    is_admin: v.is_admin,
                    country: v.country,
                    city: v.city,
                    birthdate,
                    jwt: None,
                };

                (user, HashedPassword::from_bytes(v.password))
            })
    }

    // Given an id, retrieve a User from database.
    pub async fn from_id(db: &mut PoolConnectionPtr, id: &Uuid) -> Option<Self> {
        user::select_by_id(db, id).await.map(|v| User {
            id: v.id,
            email: v.email,
            name: v.name,
            is_admin: v.is_admin,
            country: v.country,
            city: v.city,
            birthdate: v.birthdate,
            jwt: None,
        })
    }

    // Given a hashshed password, validate that it is not duplicated in DB. If not duplicated,
    // insert User in database together with their hashed password and return a JWT auth token.
    // If email is duplicated, return a BadRequest error.
    pub async fn create_and_generate_jwt(
        &self,
        db: &mut PoolConnectionPtr,
        hash: HashedPassword,
    ) -> Result<status::Created<Json<EncodedJWT>>, (Status, String)> {
        if user::email_exists(db, self.email.as_str()).await {
            Err((Status::BadRequest, "duplicated email".to_string()))
        } else {
            let (jwt, jwk) = gen_jw_pair(self.email.clone(), self.id, TOKEN_EXP_SECS);
            user::insert(db, self, hash.password).await;
            user::update_jwk(db, &self.id, Some(jwk)).await;
            Ok(status::Created::new("/users").body(Json(jwt)))
        }
    }

    // Validate that raw_password matches the User's hashed password. If validation is successful,
    // return a Json representation of the user with their JWT auth token. Otherwise, return a
    // forbidden access error.
    pub async fn authenticate(
        &mut self,
        db: &mut PoolConnectionPtr,
        raw_password: String,
        hash: HashedPassword,
    ) -> Result<Json<User>, (Status, String)> {
        let is_valid_pass = hash.verify_password(raw_password);
        if is_valid_pass {
            let (jwt, jwk) = gen_jw_pair(self.email.clone(), self.id, TOKEN_EXP_SECS);
            user::update_jwk(db, &self.id, Some(jwk.clone())).await;
            self.jwt = Some(jwt);
            Ok(Json(self.clone()))
        } else {
            Err((Status::Forbidden, "forbidden access".to_string()))
        }
    }

    // Given a category return their associated reviews for this user.
    pub async fn get_reviews_by_category(
        db: &mut PoolConnectionPtr,
        user_id: &Uuid,
        category: Category,
        offset: Option<i64>,
        to: Option<i64>,
    ) -> Vec<HistoryReview> {
        review::select_user_reviews_by_category(db, user_id, category, offset, to).await
    }

    // Given a category return their average score for this user.
    pub async fn get_average_score_by_category(
        db: &mut PoolConnectionPtr,
        user_id: &Uuid,
    ) -> HashMap<String, f64> {
        let mut avgs = HashMap::new();

        let quality_avg =
            user::select_average_score_by_category(db, &user_id, Category::Quality).await;
        let healthiness_avg =
            user::select_average_score_by_category(db, &user_id, Category::Healthiness).await;
        let fairness_avg =
            user::select_average_score_by_category(db, &user_id, Category::PriceFairness).await;
        let ecology_avg =
            user::select_average_score_by_category(db, &user_id, Category::Ecology).await;

        avgs.insert("quality".to_string(), quality_avg.score);
        avgs.insert("healthiness".to_string(), healthiness_avg.score);
        avgs.insert("price_fairness".to_string(), fairness_avg.score);
        avgs.insert("ecology".to_string(), ecology_avg.score);

        avgs
    }

    // Given a category return their restaurants vector sorted descend by global score for this user.
    pub async fn get_restaurants_ordered_desc_by_category(
        db: &mut PoolConnectionPtr,
        user_id: &Uuid,
        category: Category,
        offset: Option<i64>,
        to: Option<i64>,
    ) -> Vec<RestaurantWithScore> {
        user::select_restaurants_sorted_desc_by_category(db, user_id, category, offset, to).await
    }

    // Given a category return a recommendation vector.
    pub async fn get_recommendation_by_category(
        db: &mut PoolConnectionPtr,
        user_id: &Uuid,
        category: Category,
        offset: Option<i64>,
        to: Option<i64>,
    ) -> Vec<Recommendation> {
        let score = user::select_average_score_number_by_category(db, user_id, category).await;
        let score_aprox = score.ceil();

        let score_mod = if (score_aprox - score) == 0.0 {
            score_aprox + 1.0
        } else {
            score_aprox
        };

        user::select_recommendation_by_category(db, score_mod, category, offset, to).await
    }

    // Given a user_id, Uuid, remove the authentication secret for the current JWT auth token.
    // This is equivalent to logging the user out.
    pub async fn revoke_token(db: &mut PoolConnectionPtr, uid: &sqlx::types::uuid::Uuid) {
        user::update_jwk(db, uid, None).await;
    }

    // Given a restaurant return a user ranking vector.
    pub async fn get_ranking(
        db: &mut PoolConnectionPtr,
        restaurant_id: &Uuid,
        offset: Option<i64>,
        to: Option<i64>,
    ) -> Vec<UserWithLikes> {
        user::select_users_by_likes(db, restaurant_id, offset, to).await
    }
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct PostUserPayload {
    pub email: String,
    pub name: String,
    pub is_admin: bool,
    pub country: String,
    pub city: String,
    #[serde(with = "date_format")]
    pub birthdate: Date,
    pub password: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, FromRow)]
#[serde(crate = "rocket::serde")]
pub struct HistoryReview {
    pub category: Category,
    pub score: f32,
    pub content: Option<String>,
    pub item_title: String,
    pub image_id: Option<Uuid>,
    pub price: i64,
    #[serde(with = "iso8601_format")]
    pub created_at: time::PrimitiveDateTime,
}

#[derive(Debug, Clone, Serialize, Deserialize, FromRow)]
#[serde(crate = "rocket::serde")]
pub struct Recommendation {
    pub item_id: Uuid,
    pub item_title: String,
    pub item_description: String,
    pub image_id: Option<Uuid>,
    pub price: i64,
    pub restaurant_id: Uuid,
    pub restaurant_name: String,
    pub meal_type_id: Uuid,
    pub meal_type_title: String,
    pub score: f64,
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct UserWithLikes {
    pub id: Uuid,
    pub email: String,
    pub name: String,
    pub is_admin: bool,
    pub country: String,
    pub city: String,
    pub birthdate: Date,
    pub reviews: Option<i64>,
    pub likes: Option<i64>,
}
