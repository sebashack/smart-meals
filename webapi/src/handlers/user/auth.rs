use argon2::{
    password_hash::{rand_core::OsRng, PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2,
};

pub struct HashedPassword {
    pub password: Vec<u8>,
}

impl HashedPassword {
    pub fn new(raw_pass: String) -> Self {
        let salt = SaltString::generate(&mut OsRng);
        let argon2 = Argon2::default();
        let password = argon2
            .hash_password(raw_pass.as_bytes(), &salt)
            .unwrap()
            .to_string()
            .as_bytes()
            .to_vec();

        HashedPassword { password }
    }

    pub fn from_bytes(password: Vec<u8>) -> Self {
        HashedPassword { password }
    }

    pub fn verify_password(&self, raw_pass: String) -> bool {
        let hash = String::from_utf8(self.password.clone()).unwrap();
        let parsed_hash = PasswordHash::new(hash.as_str()).unwrap();
        Argon2::default()
            .verify_password(raw_pass.as_bytes(), &parsed_hash)
            .is_ok()
    }
}
