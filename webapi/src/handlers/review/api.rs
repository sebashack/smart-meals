use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::uuid::Uuid;
use rocket::Route;

use super::{PostReviewPayload, Review};
use crate::crud::connection::DbConnection;
use crate::utils::jwt::ApiAuth;

use super::menu;
use crate::handlers::review::{Category, ReviewWithUserName};

#[post(
    "/restaurants/menus/items/<item_id>/reviews",
    format = "json",
    data = "<payload>"
)]
async fn post_review(
    mut db: DbConnection,
    auth: ApiAuth,
    item_id: Uuid,
    payload: Json<PostReviewPayload>,
) -> Result<status::Created<Json<Review>>, (Status, String)> {
    if menu::select_if_item_exists(&mut db, &item_id).await {
        let review = Review::from_payload(auth.user_id, item_id, payload);
        review.create(&mut db).await
    } else {
        Err((Status::NotFound, "Item not found".to_string()))
    }
}

#[get("/restaurants/menus/items/<item_id>/reviews?<category>&<offset>&<to>")]
async fn get_item_reviews(
    mut db: DbConnection,
    item_id: Uuid,
    category: Category,
    offset: Option<i64>,
    to: Option<i64>,
) -> Result<Json<Vec<ReviewWithUserName>>, (Status, String)> {
    if menu::select_if_item_exists(&mut db, &item_id).await {
        let reviews = Review::get_item_reviews(&mut db, &item_id, category, offset, to).await;
        Ok(Json(reviews))
    } else {
        Err((Status::NotFound, "Item not found".to_string()))
    }
}

#[delete("/restaurants/menus/items/reviews/<review_id>")]
async fn delete_review(
    mut db: DbConnection,
    auth: ApiAuth,
    review_id: Uuid,
) -> Result<(), (Status, String)> {
    if Review::belongs_to_user(&mut db, &review_id, &auth.user_id).await {
        Review::remove(&mut db, review_id).await
    } else {
        Err((Status::NotFound, "Review not found".to_string()))
    }
}

#[put("/restaurants/menus/items/reviews/<review_id>/likes")]
async fn put_like(mut db: DbConnection, auth: ApiAuth, review_id: Uuid) -> Json<bool> {
    if Review::like_exists(&mut db, &auth.user_id, &review_id).await {
        Json(Review::change_like_state(&mut db, &auth.user_id, &review_id).await)
    } else {
        Review::create_like(&mut db, &auth.user_id, &review_id).await;
        Json(true)
    }
}

#[get("/restaurants/menus/items/reviews/<review_id>/likes")]
async fn get_review_likes(mut db: DbConnection, review_id: Uuid) -> Json<i16> {
    Json(Review::get_num_likes(&mut db, &review_id).await)
}

pub fn review_routes() -> Vec<Route> {
    routes![
        post_review,
        get_item_reviews,
        delete_review,
        put_like,
        get_review_likes
    ]
}
