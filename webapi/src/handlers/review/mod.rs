pub mod api;

use rocket::http::Status;
use rocket::response::status;
use rocket::serde::json::Json;
use rocket::serde::uuid::Uuid;
use rocket::serde::{Deserialize, Serialize};
use sqlx::FromRow;
use time::{serde, PrimitiveDateTime};

use crate::crud::connection::PoolConnectionPtr;
use crate::crud::{menu, review};

serde::format_description!(
    iso8601_format,
    PrimitiveDateTime,
    "[year]-[month]-[day]T[hour]:[minute]:[second]Z"
);

#[derive(
    Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Serialize, Deserialize, FromFormField, sqlx::Type,
)]
#[repr(i16)]
#[serde(rename_all(deserialize = "lowercase", serialize = "PascalCase"))]
pub enum Category {
    Quality = 0,
    Healthiness = 1,
    PriceFairness = 2,
    Ecology = 3,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Review {
    pub id: Uuid,
    pub category: Category,
    pub user_id: Uuid,
    pub item_id: Uuid,
    pub score: f32,
    pub content: Option<String>,
}

impl Review {
    // Given a Json payload, return a new Review.
    pub fn from_payload(user_id: Uuid, item_id: Uuid, p: Json<PostReviewPayload>) -> Self {
        let content = if p.content.as_ref().map(|c| c.len()) < Some(1) {
            None
        } else {
            p.content.clone()
        };

        Review {
            id: Uuid::new_v4(),
            category: p.category,
            user_id,
            item_id,
            score: p.score,
            content,
        }
    }

    // Insert review in database.
    pub async fn create(
        &self,
        db: &mut PoolConnectionPtr,
    ) -> Result<status::Created<Json<Review>>, (Status, String)> {
        if self.score < 0.0 || self.score > 5.0 {
            Err((Status::BadRequest, "Score must be in [0, 5]".to_string()))
        } else {
            review::insert(db, self).await;
            Ok(status::Created::new("/restaurants/menus/items/reviews").body(Json(self.clone())))
        }
    }

    // Given an item_id return their associated reviews.
    pub async fn get_item_reviews(
        db: &mut PoolConnectionPtr,
        item_id: &Uuid,
        category: Category,
        offset: Option<i64>,
        to: Option<i64>,
    ) -> Vec<ReviewWithUserName> {
        review::select_item_reviews(db, item_id, category, offset, to).await
    }

    // Determine if this Review belongs to user with user_id.
    pub async fn belongs_to_user(
        db: &mut PoolConnectionPtr,
        review_id: &Uuid,
        user_id: &Uuid,
    ) -> bool {
        review::select_if_review_belongs_to_user(db, review_id, user_id).await
    }

    // Remove review from database.
    pub async fn remove(
        db: &mut PoolConnectionPtr,
        review_id: Uuid,
    ) -> Result<(), (Status, String)> {
        review::delete_review(db, &review_id).await;
        Ok(())
    }

    pub async fn create_like(db: &mut PoolConnectionPtr, user_id: &Uuid, review_id: &Uuid) {
        review::insert_like(db, Uuid::new_v4(), &user_id, &review_id).await;
    }

    pub async fn like_exists(db: &mut PoolConnectionPtr, user_id: &Uuid, review_id: &Uuid) -> bool {
        review::select_if_like_exists(db, user_id, review_id).await
    }

    pub async fn change_like_state(
        db: &mut PoolConnectionPtr,
        user_id: &Uuid,
        review_id: &Uuid,
    ) -> bool {
        review::update_state(db, user_id, review_id).await
    }

    pub async fn get_num_likes(db: &mut PoolConnectionPtr, review_id: &Uuid) -> i16 {
        match review::review_exists(db, review_id).await {
            true => review::select_likes_count(db, review_id).await,
            false => 0,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, FromRow)]
#[serde(crate = "rocket::serde")]
pub struct PostReviewPayload {
    pub category: Category,
    pub score: f32,
    pub content: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize, FromRow)]
#[serde(crate = "rocket::serde")]
pub struct ReviewWithUserName {
    pub id: Uuid,
    pub category: Category,
    pub user_id: Uuid,
    pub item_id: Uuid,
    pub score: f32,
    pub user_name: String,
    pub content: Option<String>,
    #[serde(with = "iso8601_format")]
    pub created_at: PrimitiveDateTime,
}
