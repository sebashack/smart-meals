use rocket::http::Status;
use rocket::serde::uuid::Uuid;
use rocket::Route;

use crate::crud::connection::DbConnection;
use crate::crud::image;
use crate::utils::image::{decode_jpeg, resize, to_jpeg_buffer};

#[derive(Responder)]
#[response(status = 200)]
struct Image(Vec<u8>);

// Retrieve an image from database given an image_id. The resolution for the image must be specified.
#[get("/image/<image_id>?<width>&<height>")]
async fn get_image(
    mut db: DbConnection,
    image_id: Uuid,
    width: u32,
    height: u32,
) -> Result<Image, (Status, String)> {
    if let Some(buffer) = image::select(&mut db, &image_id).await {
        let mut jpeg = decode_jpeg(buffer).unwrap();
        let resized_jpeg = resize(&mut jpeg, width, height);
        Ok(Image(to_jpeg_buffer(resized_jpeg)))
    } else {
        Err((Status::NotFound, "Image not found".to_string()))
    }
}

pub fn image_routes() -> Vec<Route> {
    routes![get_image]
}
