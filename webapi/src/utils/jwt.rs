use jsonwebtoken::{decode, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation};
use rocket::http::Status;
use rocket::outcome::Outcome;
use rocket::request::{self, FromRequest, Request};
use rocket::serde::uuid::Uuid;
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use std::time::{SystemTime, UNIX_EPOCH};

use crate::crud::connection::DbConnection;
use crate::crud::user::{is_admin, select_jwk_by_id};

pub type EncodedJWT = String;
pub type JWK = Vec<u8>;

// Authorization for regular users
pub struct ApiAuth {
    pub user_id: Uuid,
}

#[rocket::async_trait]
// Middleware implementation for user authentication. This trait implementation is logged in.
impl<'r> FromRequest<'r> for ApiAuth {
    type Error = String;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        match authorize(req).await {
            Ok(user_id) => Outcome::Success(ApiAuth { user_id }),
            Err((status, err)) => Outcome::Failure((status, err)),
        }
    }
}

// Authorization for admin users
pub struct AdminApiAuth {
    pub user_id: Uuid,
}

// Middleware implementation for admin authentication. This trait implementation verifies
// that user is logged in and is an admin.
#[rocket::async_trait]
impl<'r> FromRequest<'r> for AdminApiAuth {
    type Error = String;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        match authorize(req).await {
            Ok(user_id) => {
                let db = req.guard::<DbConnection>().await;
                match db {
                    Outcome::Success(mut db) => {
                        if is_admin(&mut db, &user_id).await {
                            Outcome::Success(AdminApiAuth { user_id })
                        } else {
                            Outcome::Failure((Status::Unauthorized, "Not an admin".to_string()))
                        }
                    }
                    _ => Outcome::Failure((
                        Status::InternalServerError,
                        "Internal server error".to_string(),
                    )),
                }
            }
            Err((status, err)) => Outcome::Failure((status, err)),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct JwtClaims {
    iss: String,
    aud: String,
    exp: usize,
    iat: usize,
    jti: Uuid,
    sub: Uuid,
}

// Generate JWT together with the secret necessary to validate its signature and
// authenticity.
pub fn gen_jw_pair(aud: String, user_id: Uuid, exp_secs: usize) -> (EncodedJWT, JWK) {
    let iat: usize = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs()
        .try_into()
        .unwrap();
    let jti = Uuid::new_v4();
    let claims = JwtClaims {
        iss: "smartmeals".to_string(),
        aud,
        exp: iat + exp_secs,
        iat,
        jti,
        sub: user_id,
    };
    let mut jwk = [0u8; 256];
    getrandom::getrandom(&mut jwk).unwrap();

    let token = encode(
        &Header::new(Algorithm::HS256),
        &claims,
        &EncodingKey::from_secret(&jwk),
    )
    .unwrap();

    (token, jwk.to_vec())
}

// Helpers
fn verify_jwt(jwk: JWK, encoded_token: String) -> Result<(), String> {
    let mut validation = Validation::new(Algorithm::HS256);
    validation.set_issuer(&["smartmeals"]);
    validation.validate_exp = true;
    let token = decode::<JwtClaims>(
        encoded_token.as_str(),
        &DecodingKey::from_secret(jwk.as_slice()),
        &validation,
    );
    match token {
        Ok(_) => Ok(()),
        Err(err) => Err(err.to_string()),
    }
}

// Given a request, determine if JWT validation is successful.
async fn authorize<'r>(req: &'r Request<'_>) -> Result<Uuid, (Status, String)> {
    let headers = req.headers();
    let user_h = headers.get_one("User");
    let auth_h = headers.get_one("Authorization");
    match (user_h, auth_h) {
        (Some(user_h), Some(auth_h)) => {
            let db = req.guard::<DbConnection>().await;
            match db {
                Outcome::Success(mut db) => match Uuid::from_str(user_h) {
                    Err(_) => Err((Status::BadRequest, "Invalid user id".to_string())),
                    Ok(uid) => match select_jwk_by_id(&mut db, &uid).await {
                        Some(jwk) => {
                            match auth_h.strip_prefix("Bearer").map(|s| s.trim().to_string()) {
                                Some(jwt) => match verify_jwt(jwk, jwt) {
                                    Ok(_) => Ok(uid),
                                    Err(_) => {
                                        Err((Status::Unauthorized, "Unverified JWT".to_string()))
                                    }
                                },
                                None => Err((Status::Unauthorized, "Invalid JWT".to_string())),
                            }
                        }
                        None => Err((Status::NotFound, "User not found".to_string())),
                    },
                },
                _ => Err((
                    Status::InternalServerError,
                    "Internal server error".to_string(),
                )),
            }
        }
        _ => Err((
            Status::Unauthorized,
            "Both 'Authorization' and 'User' headers must be set".to_string(),
        )),
    }
}
