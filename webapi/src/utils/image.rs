use image::imageops::FilterType;
use image::io::Reader;
use image::DynamicImage;
use image::ImageFormat::{Jpeg, Png};
use std::io::Cursor;

pub fn decode_into_jpeg_buffer(buffer: Vec<u8>) -> Option<Vec<u8>> {
    decode_jpeg_or_png(buffer).map(to_jpeg_buffer)
}

pub fn decode_jpeg_or_png(buffer: Vec<u8>) -> Option<DynamicImage> {
    if let Ok(image) = Reader::with_format(Cursor::new(buffer.clone()), Jpeg).decode() {
        Some(image)
    } else if let Ok(image) = Reader::with_format(Cursor::new(buffer), Png).decode() {
        Some(image)
    } else {
        None
    }
}

pub fn decode_jpeg(buffer: Vec<u8>) -> Option<DynamicImage> {
    if let Ok(image) = Reader::with_format(Cursor::new(buffer), Jpeg).decode() {
        Some(image)
    } else {
        None
    }
}

pub fn resize(image: &mut DynamicImage, w: u32, h: u32) -> DynamicImage {
    image.resize(w, h, FilterType::Gaussian)
}

pub fn to_jpeg_buffer(image: DynamicImage) -> Vec<u8> {
    let mut buffer = vec![];
    image.write_to(&mut Cursor::new(&mut buffer), Jpeg).unwrap();

    buffer
}
