#[macro_use]
extern crate rocket;

pub mod crud;
pub mod handlers;
pub mod utils;
