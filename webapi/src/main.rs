#[macro_use]
extern crate rocket;

use webapi::handlers::server::build_server;

#[launch]
fn rocket() -> _ {
    build_server()
}
