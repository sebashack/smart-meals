use sqlx::types::uuid::Uuid;

use super::connection::PoolConnectionPtr;
use crate::handlers::review::{Category, Review, ReviewWithUserName};
use crate::handlers::user::HistoryReview;

pub async fn insert(conn: &mut PoolConnectionPtr, r: &Review) {
    sqlx::query!("INSERT INTO review (id, category, user_id, item_id, score, content) VALUES ($1, $2, $3, $4, $5, $6)",
    &r.id,
    r.category as i16,
    &r.user_id,
    &r.item_id,
    &r.score,
    r.content.as_ref())
    .execute(conn)
    .await.unwrap();
}

pub async fn select_item_reviews(
    conn: &mut PoolConnectionPtr,
    item_id: &Uuid,
    category: Category,
    offset: Option<i64>,
    to: Option<i64>,
) -> Vec<ReviewWithUserName> {
    sqlx::query_as!(
        ReviewWithUserName,
        r#"SELECT rv.id, rv.category as "category: Category", rv.user_id, rv.item_id, rv.score, u.name as user_name, rv.content, rv.created_at
        FROM review rv
        INNER JOIN xuser u ON rv.user_id = u.id
        LEFT JOIN review_like rl ON rv.id = rl.review_id AND rl.is_active = true
        WHERE rv.item_id = $1 AND rv.category = $2
        GROUP BY  rv.id, user_name
        ORDER BY COUNT(rl.id) DESC
        OFFSET $3
        LIMIT $4"#,
        item_id,
        category as i16,
        offset,
        to,
    )
    .fetch_all(conn)
    .await
    .unwrap()
}

pub async fn select_user_reviews_by_category(
    conn: &mut PoolConnectionPtr,
    user_id: &Uuid,
    category: Category,
    offset: Option<i64>,
    to: Option<i64>,
) -> Vec<HistoryReview> {
    sqlx::query_as!(
        HistoryReview,
        r#"SELECT rv.category as "category: Category", rv.score, rv.content, it.title as item_title, it.image_id, it.price, rv.created_at
        FROM review rv
        INNER JOIN xuser u ON rv.user_id = u.id
        INNER JOIN item it ON rv.item_id = it.id
        WHERE rv.user_id = $1 AND rv.category = $2
        ORDER BY rv.created_at DESC
        OFFSET $3
        LIMIT $4"#,
        user_id,
        category as i16,
        offset,
        to
    )
    .fetch_all(conn)
    .await
    .unwrap()
}

pub async fn select_if_review_belongs_to_user(
    conn: &mut PoolConnectionPtr,
    review_id: &Uuid,
    user_id: &Uuid,
) -> bool {
    sqlx::query!(
        "SELECT rv.id FROM review rv WHERE rv.user_id = $1 AND rv.id = $2",
        user_id,
        review_id
    )
    .fetch_optional(conn)
    .await
    .unwrap()
    .is_some()
}

pub async fn delete_review(conn: &mut PoolConnectionPtr, id: &Uuid) {
    sqlx::query!("DELETE FROM review WHERE id = $1", id,)
        .execute(conn)
        .await
        .unwrap();
}

pub async fn insert_like(conn: &mut PoolConnectionPtr, id: Uuid, user_id: &Uuid, review_id: &Uuid) {
    sqlx::query!(
        "INSERT INTO review_like (id, user_id, review_id) VALUES ($1, $2, $3)",
        id,
        user_id,
        review_id
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn select_if_like_exists(
    conn: &mut PoolConnectionPtr,
    user_id: &Uuid,
    review_id: &Uuid,
) -> bool {
    sqlx::query!(
        "SELECT id FROM review_like WHERE user_id = $1 AND review_id = $2",
        user_id,
        review_id
    )
    .fetch_optional(conn)
    .await
    .unwrap()
    .is_some()
}

pub async fn update_state(conn: &mut PoolConnectionPtr, user_id: &Uuid, review_id: &Uuid) -> bool {
    sqlx::query!(
        r#"UPDATE review_like SET is_active = NOT is_active WHERE user_id = $1 AND review_id = $2 RETURNING is_active"#,
        user_id,
        review_id
    )
    .fetch_optional(conn)
    .await
    .unwrap()
    .map(|row| row.is_active)
    .unwrap_or(false)
}

pub async fn review_exists(conn: &mut PoolConnectionPtr, review_id: &Uuid) -> bool {
    sqlx::query!("SELECT id FROM review rv WHERE rv.id = $1", review_id)
        .fetch_optional(conn)
        .await
        .unwrap()
        .is_some()
}

pub async fn select_likes_count(conn: &mut PoolConnectionPtr, review_id: &Uuid) -> i16 {
    let result = sqlx::query!(
        "SELECT COUNT(rl.id) FROM review_like rl WHERE rl.review_id = $1 AND rl.is_active = true",
        review_id
    )
    .fetch_one(conn)
    .await;
    match result {
        Ok(row) => row.count.unwrap_or(0) as i16,
        Err(_) => 0,
    }
}
