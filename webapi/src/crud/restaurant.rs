use rocket::serde::json::Json;
use sqlx::types::uuid::Uuid;
use sqlx::{self, Row};

use super::connection::PoolConnectionPtr;
use crate::handlers::restaurant::PutRestaurantPayload;
use crate::handlers::restaurant::Restaurant;
use crate::utils::time::sql_timestamp;

pub async fn insert(conn: &mut PoolConnectionPtr, r: &Restaurant) {
    sqlx::query!(
       "INSERT INTO restaurant (id, admin_id, email, name, country, city, address, phone_number, description) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)",
       &r.id,
       &r.admin_id,
       &r.email,
       &r.name,
       &r.country,
       &r.city,
       &r.address,
       &r.phone_number,
       &r.description
    ).execute(conn).await.unwrap();
}

pub async fn select(conn: &mut PoolConnectionPtr, id: &Uuid) -> Option<Restaurant> {
    sqlx::query_as!(
        Restaurant,
        "SELECT id, admin_id, name, email, city, country, address, phone_number, description, background_image_id, logo_id FROM restaurant WHERE id = $1",
        id
    ).fetch_optional(conn)
     .await.unwrap()
}

pub async fn select_background_image_id(conn: &mut PoolConnectionPtr, id: &Uuid) -> Option<Uuid> {
    sqlx::query("SELECT background_image_id FROM restaurant WHERE id = $1")
        .bind(id)
        .fetch_one(conn)
        .await
        .and_then(|r| r.try_get(0))
        .ok()
}

pub async fn select_logo_id(conn: &mut PoolConnectionPtr, id: &Uuid) -> Option<Uuid> {
    sqlx::query("SELECT logo_id FROM restaurant WHERE id = $1")
        .bind(id)
        .fetch_one(conn)
        .await
        .and_then(|r| r.try_get(0))
        .ok()
}

pub async fn select_by_admin_id(conn: &mut PoolConnectionPtr, admin_id: &Uuid) -> Vec<Restaurant> {
    sqlx::query_as!(
        Restaurant,
        "SELECT id, admin_id, name, email, city, country, address, phone_number, description, background_image_id, logo_id FROM restaurant WHERE admin_id = $1",
        admin_id
    ).fetch_all(conn)
     .await.unwrap()
}

pub async fn select_if_belongs_to_admin(
    conn: &mut PoolConnectionPtr,
    id: &Uuid,
    admin_id: &Uuid,
) -> bool {
    sqlx::query!(
        "SELECT id FROM restaurant WHERE id = $1 AND admin_id = $2",
        id,
        admin_id
    )
    .fetch_optional(conn)
    .await
    .unwrap()
    .is_some()
}

pub async fn select_if_exists(conn: &mut PoolConnectionPtr, id: &Uuid) -> bool {
    sqlx::query!("SELECT id FROM restaurant WHERE id = $1", id,)
        .fetch_optional(conn)
        .await
        .unwrap()
        .is_some()
}

pub async fn update(
    conn: &mut PoolConnectionPtr,
    res_id: Uuid,
    payload: Json<PutRestaurantPayload>,
) {
    sqlx::query!(
       "UPDATE restaurant SET email=$1, name=$2, country=$3, city=$4, address=$5, phone_number=$6, description=$7, updated_at=$8 WHERE id = $9",
       payload.email.clone(),
       payload.name.clone(),
       payload.country.clone(),
       payload.city.clone(),
       payload.address.clone(),
       payload.phone_number.clone(),
       payload.description.clone(),
       sql_timestamp(),
       res_id,
    ).execute(conn).await.unwrap();
}

pub async fn update_background_image(conn: &mut PoolConnectionPtr, res_id: &Uuid, img_id: &Uuid) {
    sqlx::query!(
        "UPDATE restaurant SET background_image_id = $2 WHERE id = $1",
        res_id,
        img_id,
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn update_logo(conn: &mut PoolConnectionPtr, res_id: &Uuid, img_id: &Uuid) {
    sqlx::query!(
        "UPDATE restaurant SET logo_id = $2 WHERE id = $1",
        res_id,
        img_id,
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn delete(conn: &mut PoolConnectionPtr, id: &Uuid) {
    sqlx::query!("DELETE FROM restaurant WHERE id = $1", id)
        .execute(conn)
        .await
        .unwrap();
}

pub async fn select_restaurants_by_word(
    conn: &mut PoolConnectionPtr,
    word: String,
    offset: Option<i64>,
    to: Option<i64>,
) -> Vec<Restaurant> {
    sqlx::query_as!(
        Restaurant,
        "SELECT id, admin_id, name, email, city, country, address, phone_number, description, background_image_id, logo_id
         FROM restaurant
         WHERE name ILIKE $1
         OFFSET $2
         LIMIT  $3",
        format!("%{}%", word),
        offset,
        to
    ).fetch_all(conn)
     .await.unwrap()
}
