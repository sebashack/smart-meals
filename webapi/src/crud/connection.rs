use rocket_db_pools::sqlx::{self};
use rocket_db_pools::Connection;
use rocket_db_pools::Database;
use sqlx::pool::PoolConnection;
use sqlx::postgres::{PgPool, Postgres};

#[derive(Database)]
#[database("smartmeals")]
pub struct Db(PgPool);

pub type DbConnection = Connection<Db>;
pub type PoolConnectionPtr = PoolConnection<Postgres>;
