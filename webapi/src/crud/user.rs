use sqlx::{self, types::uuid::Uuid};
use time::Date;

use super::connection::PoolConnectionPtr;
use crate::handlers::restaurant::{Restaurant, RestaurantWithScore};
use crate::handlers::review::Category;
use crate::handlers::user::{Recommendation, User, UserWithLikes};
use crate::utils::time::sql_timestamp;

pub struct UserWithPasswordSql {
    pub id: Uuid,
    pub email: String,
    pub name: String,
    pub is_admin: bool,
    pub country: String,
    pub city: String,
    pub birthdate: Date,
    pub password: Vec<u8>,
}


#[derive(Debug)]
pub struct AverageSql {
    pub category: Category,
    pub score: f64,
}

pub async fn insert(conn: &mut PoolConnectionPtr, u: &User, hashed_pass: Vec<u8>) {
    sqlx::query!(
       "INSERT INTO xuser (id, email, name, city, country, birthdate, is_admin, password) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)",
       &u.id,
       &u.email,
       &u.name,
       &u.city,
       &u.country,
       &u.birthdate,
       u.is_admin,
       hashed_pass
    ).execute(conn).await.unwrap();
}

pub async fn select_with_password_by_email(
    conn: &mut PoolConnectionPtr,
    email: &str,
) -> Option<UserWithPasswordSql> {
    sqlx::query_as!(
        UserWithPasswordSql,
        "SELECT id, name, email, city, country, is_admin, birthdate, password FROM xuser WHERE email = $1",
        email
    ).fetch_optional(conn)
    .await.unwrap()
}

pub async fn select_by_id(conn: &mut PoolConnectionPtr, id: &Uuid) -> Option<UserWithPasswordSql> {
    sqlx::query_as!(
        UserWithPasswordSql,
        "SELECT id, name, email, city, country, is_admin, birthdate, password FROM xuser WHERE id = $1",
        id
    ).fetch_optional(conn)
    .await.unwrap()
}

pub async fn update_jwk(conn: &mut PoolConnectionPtr, uid: &Uuid, jwk: Option<Vec<u8>>) {
    sqlx::query!(
        "UPDATE xuser SET jwk = $1, updated_at = $2 WHERE id = $3",
        jwk,
        sql_timestamp(),
        uid
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn select_jwk_by_id(conn: &mut PoolConnectionPtr, uid: &Uuid) -> Option<Vec<u8>> {
    sqlx::query!("SELECT jwk FROM xuser WHERE id = $1", uid)
        .fetch_optional(conn)
        .await
        .unwrap()
        .and_then(|r| r.jwk)
}

pub async fn is_admin(conn: &mut PoolConnectionPtr, uid: &Uuid) -> bool {
    let v = sqlx::query!("SELECT is_admin FROM xuser WHERE id = $1", uid)
        .fetch_optional(conn)
        .await
        .unwrap()
        .map(|r| r.is_admin);

    v == Some(true)
}

pub async fn email_exists(conn: &mut PoolConnectionPtr, email: &str) -> bool {
    sqlx::query!("SELECT email FROM xuser WHERE email = $1", email)
        .fetch_optional(conn)
        .await
        .unwrap()
        .map(|r| r.email)
        .is_some()
}

pub async fn select_average_score_by_category(
    conn: &mut PoolConnectionPtr,
    user_id: &Uuid,
    category: Category,
) -> AverageSql {
    let result = sqlx::query_as!(
        AverageSql,
        r#"SELECT COALESCE(AVG(score), 0) as "score!", category as "category: Category" 
            FROM review WHERE user_id = $1 AND category = $2 GROUP BY category"#,
        user_id,
        category as i16,
    )
    .fetch_optional(conn)
    .await
    .unwrap();

    match result {
        Some(v) => v,
        None => AverageSql {
            category,
            score: 0.0,
        },
    }
}

pub async fn select_average_score_number_by_category(
    conn: &mut PoolConnectionPtr,
    user_id: &Uuid,
    category: Category,
) -> f64 {
    sqlx::query_scalar!(
            r#"SELECT COALESCE(AVG(score), 0) as "score!" FROM review WHERE user_id = $1 AND category = $2"#,
            user_id,
            category as i16,
        )
        .fetch_one(conn)
        .await
        .unwrap()
}

pub async fn select_restaurants_sorted_desc_by_category(
    conn: &mut PoolConnectionPtr,
    user_id: &Uuid,
    category: Category,
    offset: Option<i64>,
    to: Option<i64>,
) -> Vec<RestaurantWithScore> {
    let restaurants = sqlx::query!(
        "SELECT r.*, AVG(rv.score) as score
            FROM restaurant r
            JOIN menu m ON r.id = m.restaurant_id
            JOIN meal_type mt ON m.id = mt.menu_id
            JOIN item i ON mt.id = i.meal_type_id
            JOIN review rv ON i.id = rv.item_id
            WHERE rv.user_id = $1 AND rv.category = $2
            GROUP BY r.id, r.admin_id
            ORDER BY score DESC
            OFFSET $3
            LIMIT $4",
        user_id,
        category as i16,
        offset,
        to
    )
    .fetch_all(conn)
    .await
    .unwrap();

    restaurants
        .into_iter()
        .map(|r| RestaurantWithScore {
            restaurant: Restaurant {
                id: r.id,
                admin_id: r.admin_id,
                name: r.name,
                email: r.email,
                country: r.country,
                city: r.city,
                address: r.address,
                phone_number: r.phone_number,
                description: r.description,
                background_image_id: r.background_image_id,
                logo_id: r.logo_id,
            },
            score: r.score,
        })
        .collect()
}

pub async fn select_recommendation_by_category(
    conn: &mut PoolConnectionPtr,
    score: f64,
    category: Category,
    offset: Option<i64>,
    to: Option<i64>,
) -> Vec<Recommendation> {
    sqlx::query_as!(
            Recommendation,
            r#"SELECT i.id as item_id, i.title as item_title, i.description as item_description, i.image_id, i.price, mn.restaurant_id, rt.name as restaurant_name, i.meal_type_id, mt.title as meal_type_title, COALESCE(AVG(rv.score), 0) as "score!"
            FROM review rv
            INNER JOIN item i ON rv.item_id = i.id
            INNER JOIN meal_type mt ON i.meal_type_id = mt.id
            INNER JOIN menu mn ON mt.menu_id = mn.id
            INNER JOIN restaurant rt ON rt.id = mn.restaurant_id
            WHERE rv.category = $1
            GROUP BY mn.restaurant_id, i.id, rt.name, mt.title
            HAVING AVG(rv.score) >= $2
            ORDER BY AVG(rv.score) DESC
            OFFSET $3
            LIMIT  $4"#,
            category as i16,
            score,
            offset,
            to
        )
        .fetch_all(conn)
        .await
        .unwrap()
}

pub async fn select_users_by_likes(
    conn: &mut PoolConnectionPtr,
    restaurant_id: &Uuid,
    offset: Option<i64>,
    limit: Option<i64>,
) -> Vec<UserWithLikes> {
    sqlx::query_as!(
        UserWithLikes,
        r#"
        SELECT u.id, u.name, u.email, u.city, u.country, u.is_admin, u.birthdate,
        COUNT(rv.id) as reviews,
        COUNT(CASE WHEN rl.is_active = true THEN 1 ELSE NULL END) as likes
        FROM xuser u
        INNER JOIN review rv ON rv.user_id = u.id
        LEFT JOIN review_like rl ON rv.id = rl.review_id
        INNER JOIN item i ON rv.item_id = i.id
        INNER JOIN meal_type mt ON i.meal_type_id = mt.id
        INNER JOIN menu m ON mt.menu_id = m.id
        INNER JOIN restaurant r ON m.restaurant_id = r.id
        WHERE r.id = $1
        GROUP BY u.id, u.name, u.email, u.city, u.country, u.is_admin, u.birthdate
        ORDER BY COUNT(CASE WHEN rl.is_active = true THEN 1 ELSE NULL END) DESC
        OFFSET $2
        LIMIT $3
        "#,
        restaurant_id,
        offset.unwrap_or(0),
        limit.unwrap_or(100),
    )
    .fetch_all(conn)
    .await
    .unwrap()
}

