use super::connection::PoolConnectionPtr;
use rocket::serde::json::Json;
use sqlx::types::uuid::Uuid;
use sqlx::{self, Row};

use crate::handlers::menu::{Item, ItemWithScore, MealType, Menu, PutItemPayload};
use crate::handlers::review::Category;
use crate::utils::time::sql_timestamp;

#[derive(Debug)]
pub struct MenuSql {
    pub id: Uuid,
    pub restaurant_id: Uuid,
    pub title: String,
}

#[derive(Debug)]
pub struct AverageSql {
    pub category: Category,
    pub score: f64,
}

pub async fn select_menu_by_restaurant_id(
    conn: &mut PoolConnectionPtr,
    restaurant_id: &Uuid,
) -> Option<MenuSql> {
    sqlx::query_as!(
        MenuSql,
        "SELECT id, restaurant_id, title FROM menu WHERE restaurant_id = $1",
        restaurant_id
    )
    .fetch_optional(conn)
    .await
    .unwrap()
}

pub async fn select_meal_types_by_menu_id(
    conn: &mut PoolConnectionPtr,
    menu_id: &Uuid,
) -> Vec<MealType> {
    sqlx::query_as!(
        MealType,
        "SELECT id, menu_id, title FROM meal_type WHERE menu_id = $1",
        menu_id
    )
    .fetch_all(conn)
    .await
    .unwrap()
}

pub async fn select_items_with_score_by_meal_type_id(
    conn: &mut PoolConnectionPtr,
    meal_type_id: &Uuid,
) -> Vec<ItemWithScore> {
    sqlx::query_as!(
        ItemWithScore,
        r#"SELECT i.id           :: uuid,
               i.meal_type_id,
               i.image_id,
               i.title,
               i.description,
               i.price,
               COALESCE(AVG(r.score), 0) as "global_score!"
        FROM item i
        LEFT JOIN review r ON i.id = r.item_id
        WHERE i.meal_type_id = $1
        GROUP BY i.id"#,
        meal_type_id
    )
    .fetch_all(conn)
    .await
    .unwrap()
}

pub async fn select_items_by_meal_type_id(
    conn: &mut PoolConnectionPtr,
    meal_type_id: &Uuid,
) -> Vec<Item> {
    sqlx::query_as!(
        Item,
        "SELECT id, meal_type_id, title, description, price, image_id FROM item WHERE meal_type_id = $1",
        meal_type_id
    )
    .fetch_all(conn)
    .await
    .unwrap()
}

pub async fn insert_menu(conn: &mut PoolConnectionPtr, m: &Menu) {
    sqlx::query!(
        "INSERT INTO menu (id, restaurant_id, title) VALUES ($1, $2, $3)",
        m.id,
        m.restaurant_id,
        m.title,
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn insert_meal_type(conn: &mut PoolConnectionPtr, mt: &MealType) {
    sqlx::query!(
        "INSERT INTO meal_type (id, menu_id, title) VALUES ($1, $2, $3)",
        mt.id,
        mt.menu_id,
        mt.title
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn insert_item(conn: &mut PoolConnectionPtr, i: &Item) {
    sqlx::query!(
        "INSERT INTO item (id, meal_type_id, title, description, price) VALUES ($1, $2, $3, $4, $5)",
        &i.id,
        &i.meal_type_id,
        &i.title,
        &i.description,
        i.price,
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn select_if_mealtype_belongs_to_admin(
    conn: &mut PoolConnectionPtr,
    meal_type_id: &Uuid,
    admin_id: &Uuid,
) -> bool {
    sqlx::query!(
        "SELECT mt.id
            FROM xuser u
            JOIN restaurant r ON u.id = r.admin_id
            JOIN menu m ON r.id = m.restaurant_id
            JOIN meal_type mt ON m.id = mt.menu_id
            WHERE u.id = $1
            AND mt.id = $2
        ",
        admin_id,
        meal_type_id
    )
    .fetch_optional(conn)
    .await
    .unwrap()
    .is_some()
}

pub async fn select_if_menu_belongs_to_admin(
    conn: &mut PoolConnectionPtr,
    menu_id: &Uuid,
    admin_id: &Uuid,
) -> bool {
    sqlx::query!(
        "SELECT m.id
            FROM xuser u
            JOIN restaurant r ON u.id = r.admin_id
            JOIN menu m ON r.id = m.restaurant_id
            WHERE u.id = $1
            AND m.id = $2",
        admin_id,
        menu_id,
    )
    .fetch_optional(conn)
    .await
    .unwrap()
    .is_some()
}

pub async fn select_if_item_belongs_to_admin(
    conn: &mut PoolConnectionPtr,
    item_id: &Uuid,
    admin_id: &Uuid,
) -> bool {
    sqlx::query!(
        "SELECT i.id
            FROM xuser u
            JOIN restaurant r ON u.id = r.admin_id
            JOIN menu m ON r.id = m.restaurant_id
            JOIN meal_type mt ON m.id = mt.menu_id
            JOIN item i ON mt.id = i.meal_type_id
            WHERE u.id = $1
            AND i.id = $2",
        admin_id,
        item_id,
    )
    .fetch_optional(conn)
    .await
    .unwrap()
    .is_some()
}

pub async fn select_if_item_exists(conn: &mut PoolConnectionPtr, item_id: &Uuid) -> bool {
    sqlx::query!("SELECT id FROM item WHERE id = $1", item_id)
        .fetch_optional(conn)
        .await
        .unwrap()
        .is_some()
}

pub async fn select_if_meal_type_exists(conn: &mut PoolConnectionPtr, meal_type_id: &Uuid) -> bool {
    sqlx::query!("SELECT id FROM meal_type WHERE id = $1", meal_type_id)
        .fetch_optional(conn)
        .await
        .unwrap()
        .is_some()
}

pub async fn delete_menu(conn: &mut PoolConnectionPtr, id: &Uuid) {
    sqlx::query!("DELETE FROM menu WHERE id = $1", id,)
        .execute(conn)
        .await
        .unwrap();
}

pub async fn delete_meal_type(conn: &mut PoolConnectionPtr, id: &Uuid) {
    sqlx::query!("DELETE FROM meal_type WHERE id = $1", id,)
        .execute(conn)
        .await
        .unwrap();
}

pub async fn delete_item(conn: &mut PoolConnectionPtr, id: &Uuid) {
    sqlx::query!("DELETE FROM item WHERE id = $1", id,)
        .execute(conn)
        .await
        .unwrap();
}

pub async fn update_meal_type(conn: &mut PoolConnectionPtr, meal_type_id: &Uuid, title: &str) {
    sqlx::query!(
        "UPDATE meal_type SET title=$1, updated_at=$2 WHERE id = $3",
        title,
        sql_timestamp(),
        meal_type_id,
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn update_item(
    conn: &mut PoolConnectionPtr,
    meal_type_id: &Uuid,
    item_id: &Uuid,
    p: Json<PutItemPayload>,
) {
    sqlx::query!(
        "UPDATE item SET title = $1, description = $2, price = $3, meal_type_id = $4, updated_at = $5 WHERE id = $6",
        p.title,
        p.description,
        p.price,
        meal_type_id,
        sql_timestamp(),
        item_id,
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn update_item_image(conn: &mut PoolConnectionPtr, item_id: &Uuid, img_id: &Uuid) {
    sqlx::query!(
        "UPDATE item SET image_id = $2, updated_at = $3 WHERE id = $1",
        item_id,
        img_id,
        sql_timestamp(),
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn update_menu_title(conn: &mut PoolConnectionPtr, menu_id: &Uuid, title: &str) {
    sqlx::query!(
        "UPDATE menu SET title=$1, updated_at=$2 WHERE id = $3",
        title,
        sql_timestamp(),
        menu_id,
    )
    .execute(conn)
    .await
    .unwrap();
}

pub async fn select_item_image_id(conn: &mut PoolConnectionPtr, id: &Uuid) -> Option<Uuid> {
    sqlx::query("SELECT image_id FROM item WHERE id = $1")
        .bind(id)
        .fetch_one(conn)
        .await
        .and_then(|r| r.try_get(0))
        .ok()
}

pub async fn select_items_by_global_score(
    conn: &mut PoolConnectionPtr,
    meal_type_id: &Uuid,
    min_score: f64,
    up_bound: f64,
) -> Vec<ItemWithScore> {
    sqlx::query_as!(
        ItemWithScore,
        r#"SELECT i.id, i.meal_type_id, i.image_id, i.title, i.description, i.price, COALESCE(AVG(r.score), 0) as "global_score!"
        FROM item i
        LEFT JOIN review r ON i.id = r.item_id
        WHERE i.meal_type_id = $1
        GROUP BY i.id
        HAVING COALESCE(AVG(r.score), 0) >= $2 AND COALESCE(AVG(r.score), 0) < $3"#,
        meal_type_id,
        min_score,
        up_bound,
    )
    .fetch_all(conn)
    .await
    .unwrap()
}

pub async fn select_item_averages(
    conn: &mut PoolConnectionPtr,
    item_id: &Uuid,
    categories: &[Category],
) -> Vec<AverageSql> {
    let mut averages = Vec::new();
    for cat in categories.iter() {
        averages.push(select_item_category_average(conn, item_id, *cat).await);
    }

    averages
}

pub async fn select_item_category_average(
    conn: &mut PoolConnectionPtr,
    item_id: &Uuid,
    category: Category,
) -> AverageSql {
    let result = sqlx::query_as!(
        AverageSql,
        r#"SELECT COALESCE(AVG(score), 0) as "score!", category as "category: Category" FROM review WHERE item_id = $1 AND category = $2 GROUP BY category"#,
        item_id,
        category as i16,
    )
    .fetch_optional(conn)
    .await
    .unwrap();

    match result {
        Some(v) => v,
        None => AverageSql {
            category,
            score: 0.0,
        },
    }
}

pub async fn select_meal_type_category_average(
    conn: &mut PoolConnectionPtr,
    meal_type_id: &Uuid,
    category: Category,
) -> AverageSql {
    let result = sqlx::query_as!(
        AverageSql,
        r#"SELECT COALESCE(AVG(score), 0) as "score!", category as "category: Category" 
        FROM meal_type mt
        JOIN item i ON mt.id = i.meal_type_id
        JOIN review rv ON i.id = rv.item_id
        WHERE mt.id = $1 AND rv.category = $2
        GROUP BY rv.category"#,
        meal_type_id,
        category as i16,
    )
    .fetch_optional(conn)
    .await
    .unwrap();

    match result {
        Some(v) => v,
        None => AverageSql {
            category,
            score: 0.0,
        },
    }
}

pub async fn select_if_menu_exists(
    conn: &mut PoolConnectionPtr,
    menu_id: &Uuid,
) -> bool {
    sqlx::query!(
        "SELECT id FROM menu m WHERE m.id = $1",
        menu_id,
    )
    .fetch_optional(conn)
    .await
    .unwrap()
    .is_some()
}

pub async fn select_category_average(
    conn: &mut PoolConnectionPtr,
    menu_id: &Uuid,
    category: Category,
) -> AverageSql {
    let result = sqlx::query_as!(
        AverageSql,
        r#"SELECT COALESCE(AVG(score), 0) as "score!",
        category as "category: Category"
        FROM review
        WHERE item_id IN (
            SELECT id FROM item
            WHERE meal_type_id IN (
                SELECT id FROM meal_type
                WHERE menu_id = $1
            )
        )
        AND category = $2
        GROUP BY category"#,
        menu_id,
        category as i16,
    )
    .fetch_optional(conn)
    .await
    .unwrap();

    match result {
        Some(v) => v,
        None => AverageSql {
            category,
            score: 0.0,
        },
    }
}
