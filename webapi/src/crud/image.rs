use rocket_db_pools::sqlx::{self, Row};
use sqlx::types::uuid::Uuid;

use super::connection::PoolConnectionPtr;

pub async fn insert(conn: &mut PoolConnectionPtr, id: &Uuid, image: Vec<u8>) {
    sqlx::query!("INSERT INTO image (id, content) VALUES ($1, $2)", id, image)
        .execute(conn)
        .await
        .unwrap();
}

pub async fn select(conn: &mut PoolConnectionPtr, id: &Uuid) -> Option<Vec<u8>> {
    sqlx::query("SELECT content FROM image WHERE id = $1")
        .bind(id)
        .fetch_one(conn)
        .await
        .and_then(|r| r.try_get(0))
        .ok()
}

pub async fn delete(conn: &mut PoolConnectionPtr, id: &Uuid) {
    sqlx::query!("DELETE FROM image WHERE id = $1", id)
        .execute(conn)
        .await
        .unwrap();
}
