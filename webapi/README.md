# Run development DB

To run postgresql DB:

```
cd ../docker
./service up
```

To stop postgresql DB:

```
./service down
```

# Add and run migrations

To Add a migration:

```
cd migrations
./add.sh <migration_name>
````


To run migrations:

```
cd migrations
./run.sh
```

# Run static typechecking for CRUD

```
./build-sql.sh
```

# Build server in development mode

```
cargo build
```

# Execute server in development mode

```
cargo run
```

# Fix style

```
cargo fmt
```

# Run linter

```
cargo clippy
```
