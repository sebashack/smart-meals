BUILD_DIR:=${CURDIR}/_build
DIST_DIR:=${BUILD_DIR}/dist
PACKAGE_DIR:=${BUILD_DIR}/packages

COMMIT_SHORT_SHA:=$(if $(CI_COMMIT_SHORT_SHA),$(CI_COMMIT_SHORT_SHA),$(shell git rev-parse --short HEAD))
COMPRESION_LEVEL:=$(if $(CI),9,1)
UBUNTU_CODE_NAME:=$(shell cat /etc/os-release | grep UBUNTU_CODENAME | cut -d = -f 2 )
BRANCH:=$(if $(CI_COMMIT_BRANCH),$(CI_COMMIT_BRANCH),$(shell git rev-parse --abbrev-ref HEAD))
BUILD_NUMBER:=$(if $(CI_PIPELINE_IID),$(CI_PIPELINE_IID),$(shell git rev-list --count HEAD))

MAJOR_VERSION:=1
MINOR_VERSION:=0
PACKAGE_NAME:=smart-menu-${MAJOR_VERSION}.${MINOR_VERSION}.${BUILD_NUMBER}-${COMMIT_SHORT_SHA}--ubuntu-${UBUNTU_CODE_NAME}

.PHONY: package
package: build-dist
	mkdir -p ${PACKAGE_DIR}
	$(eval FINGERPRINT:=$(shell python3 fingerprint.py ${DIST_DIR}))
	rm -rf ${PACKAGE_DIR}/dist && mv ${DIST_DIR} ${PACKAGE_DIR}
	cd ${PACKAGE_DIR} && tar chvf ${PACKAGE_NAME}--${FINGERPRINT}.tar ./dist
	lzip -f -${COMPRESION_LEVEL} ${PACKAGE_DIR}/${PACKAGE_NAME}--${FINGERPRINT}.tar
	rm -rf ${PACKAGE_DIR}/dist

.PHONY: build-dist
build-dist: prod-webapi
	rm -rf ${DIST_DIR}
	mkdir -p ${DIST_DIR}
	touch ${DIST_DIR}/version
	mv webapi/target/release/webapi ${DIST_DIR}
	echo "repository: smart-meals" >> ${DIST_DIR}/version
	echo "branch: ${BRANCH}" >> ${DIST_DIR}/version
	echo "commit: ${COMMIT_SHORT_SHA}" >> ${DIST_DIR}/version

.PHONY: prod-webapi
prod-webapi:
	cd webapi && cargo build --release

.PHONY: dev-webapi
dev-webapi:
	cd webapi && cargo build

.PHONY: prod-webapp
prod-webapp:
	cd webapp && npm install && npm run build

.PHONY: install-webapp
install-webapp:
	cd webapp && npm install

.PHONY: check-style
check-style:
	cd webapi && cargo fmt --check
	cd webapp && npm run check-style
	cd webapp && npm run lint

.PHONY: clean
clean:
	cd webapi && cargo clean
	rm -rf ${BUILD_DIR}
	cd webapp && rm -rf node_modules
