import { styled } from '@mui/material/styles'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import ButtonBase from '@mui/material/ButtonBase'
import Rating from '@mui/material/Rating'
import PropTypes from 'prop-types'
import { priceFormatter } from '@/utils/helpers'

const styles = {
    paper: {
        p: 2,
        position: 'relative',
        margin: 'auto',
        marginBottom: '35px',
        maxWidth: 800,
        backgroundColor: (theme) => (theme.palette.mode === 'dark' ? '#1A2027' : '#fff')
    },
    gridContainer: {
        placeItems: 'center',
        cursor: 'pointer'
    },
    itemImg: {
        width: '100%',
        objectFit: 'cover',
        '@media only screen and  (min-width: 660px)': {
            maxWidth: 300
        }
    },
    rating: {
        '&.MuiRating-root span': {
            fontSize: '40px'
        },
        marginBottom: '10px'
    },
    boldText: {
        fontWeight: 'bold'
    },
    infoIcon: {
        position: 'absolute',
        top: '20px',
        right: '20px',
        fontSize: '25px'
    }
}

const Img = styled('img')({
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%'
})

export function ItemRecommendationCard({
    imageUrl,
    name,
    description,
    rating,
    price,
    onClick,
    restaurantName,
    mealTypeTitle
}) {
    return (
        <>
            <Paper sx={styles.paper}>
                <Grid container sx={styles.gridContainer} spacing={2} onClick={onClick}>
                    <Grid item>
                        <ButtonBase sx={styles.itemImg}>
                            <Img alt="Meal image" src={imageUrl} />
                        </ButtonBase>
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                                <Typography gutterBottom variant="h4" sx={styles.boldText}>
                                    {name}
                                </Typography>
                                <Rating name="rating" value={rating} precision={0.5} sx={styles.rating} readOnly />
                                <Typography variant="body2" gutterBottom>
                                    {description}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography sx={styles.boldText} variant="h4">
                                    {priceFormatter(price)}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Typography variant="h6">Categoria: {mealTypeTitle}</Typography>
                        <Typography variant="h6">Restaurante: {restaurantName}</Typography>
                    </Grid>
                </Grid>
            </Paper>
        </>
    )
}

ItemRecommendationCard.propTypes = {
    imageUrl: PropTypes.string,
    rating: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    price: PropTypes.number,
    onClick: PropTypes.func,
    restaurantName: PropTypes.string,
    mealTypeTitle: PropTypes.string
}
