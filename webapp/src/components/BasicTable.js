import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material'
import PropTypes from 'prop-types'

const style = {
    tableRow: {
        '&:last-child td, &:last-child th': { border: 0 }
    },
    tableCell: {
        textAlign: 'center'
    }
}
export default function BasicTable({ fields, rows }) {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {fields.map((field, index) => {
                            return (
                                <TableCell key={index} sx={style.tableCell}>
                                    {field.text}
                                </TableCell>
                            )
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, index) => (
                        <TableRow key={index} sx={style.tableRow}>
                            {fields.map((field, index) => {
                                return (
                                    <TableCell sx={style.tableCell} key={index}>
                                        {row[`${field.value}`]}
                                    </TableCell>
                                )
                            })}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

BasicTable.propTypes = {
    fields: PropTypes.array.isRequired,
    rows: PropTypes.array.isRequired
}
