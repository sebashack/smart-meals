import { useRouter } from 'next/router'
import { Container, Stack, CardMedia, Typography, Card, Divider, Button } from '@mui/material'
import PropTypes from 'prop-types'

const style = {
    mainContainer: {
        width: '90%',
        height: { md: '500px' },
        borderRadius: '10px',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        padding: {
            xs: '10px',
            sm: '50px'
        },
        mt: 1
    },
    mainStack: {
        justifyContent: 'center'
    },
    cardInfo: {
        padding: '10px',
        width: { xs: '100%' },
        borderRadius: '20px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    stackInfo: {
        textAlign: 'center',
        mt: 1
    },
    menuButton: {
        width: { xs: '100%', sm: '75%', md: '50%' },
        background: 'black',
        m: 1,
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        }
    },
    cardImage: {
        borderRadius: '50%'
    }
}

export function RestaurantCard({ restaurantInfo }) {
    const { id, name, address, phone_number, description, email, logo, background, hasMenu, isAdmin } = restaurantInfo
    const router = useRouter()

    const goToRoute = (url) => {
        router.push(url)
    }

    const actions = hasMenu ? (
        <Button variant="contained" sx={style.menuButton} onClick={() => goToRoute(`/restaurant/${id}/menu`)}>
            Menú
        </Button>
    ) : isAdmin ? (
        <Button
            variant="contained"
            sx={style.menuButton}
            onClick={() => goToRoute(`/admin/restaurants/${id}/menu/register`)}
        >
            Añadir menú
        </Button>
    ) : null

    const adminActions =
        isAdmin && hasMenu ? (
            <Button
                variant="contained"
                sx={style.menuButton}
                onClick={() => goToRoute(`/admin/restaurants/${id}/menu/items`)}
            >
                Items
            </Button>
        ) : null

    return (
        <Container sx={{ ...style.mainContainer, backgroundImage: `url('${background}')` }}>
            <Stack
                direction={{ xs: 'column', sm: 'column', md: 'row' }}
                spacing={{ xs: 1, sm: 8, md: 16 }}
                sx={style.mainStack}
            >
                <Container>
                    <CardMedia component="img" alt="Logo restaurante" image={logo} sx={style.cardImage} />
                </Container>
                <Card sx={style.cardInfo}>
                    <Stack direction="column" sx={style.stackInfo} spacing={1}>
                        <Typography variant="h4">Bienvenido a {name}</Typography>
                        <Divider />
                        <Typography variant="p">{description}</Typography>
                        <Divider />
                        <Typography variant="p">
                            <b>Dirección: </b> {address}
                            <br />
                            <b>Número: </b> {phone_number}
                            <br />
                            <b>Correo: </b> {email}
                            <br />
                        </Typography>
                        <Container>
                            {actions}
                            {adminActions}
                        </Container>
                    </Stack>
                </Card>
            </Stack>
        </Container>
    )
}

RestaurantCard.propTypes = {
    restaurantInfo: PropTypes.object.isRequired
}
