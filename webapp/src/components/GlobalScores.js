import { Rating, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'

const styles = {
    boldText: {
        fontWeight: 'bold'
    }
}

export default function GlobalScores({ getGlobalScores }) {
    const [state, setState] = useState({
        scores: null
    })

    const categories = [
        { name: 'healthiness', title: 'Salud' },
        { name: 'quality', title: 'Calidad' },
        { name: 'price_fairness', title: 'Economía' },
        { name: 'ecology', title: 'Ecología' }
    ]

    useEffect(() => {
        const fetchData = async () => {
            const fetchScores = await getGlobalScores()

            if (fetchScores.isSuccess) {
                setState((prevSt) => ({ ...prevSt, scores: fetchScores.data }))
            } else {
                console.log(fetchScores.error)
            }
        }

        fetchData()
    }, [getGlobalScores])

    const globalScores = state.scores
        ? categories.map((category) => (
              <div key={category.name}>
                  <Typography gutterBottom variant="h6" sx={styles.boldText}>
                      {category.title}
                  </Typography>
                  <Rating name="rating" defaultValue={state.scores[category.name]} precision={0.5} readOnly />
              </div>
          ))
        : null

    return globalScores
}
