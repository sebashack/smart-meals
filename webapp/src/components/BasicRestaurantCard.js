import { useRouter } from 'next/router'
import { Container, CardMedia, Typography, Card, Divider, Grid } from '@mui/material'
import PropTypes from 'prop-types'

const styles = {
    cardContainer: {
        height: '100%',
        boxShadow: '1px 1px 2px 2px rgba(0, 0, 0, 0.2)',
        borderRadius: '20px',
        margin: 'auto'
    },
    cardContent: {
        borderRadius: '20px 20px 0 0',
        padding: '5px'
    },
    cardInfo: {
        padding: '10px',
        width: { xs: '100%' },
        borderRadius: '0 0 20px 20px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    cardImage: {
        maxWidth: '155px',
        borderRadius: '50%'
    },
    restaurantName: {
        fontSize: '1.5em',
        fontWeight: 'bold',
        overflowWrap: 'break-word'
    },
    restaurantInfo: {
        fontSize: '1rem',
        wordBreak: 'break-word'
    }
}

export function BasicRestaurantCard({ restaurantInfo }) {
    const { id, name, address, phone_number, logo, background } = restaurantInfo
    const router = useRouter()

    const goToRoute = (url) => {
        router.push(url)
    }

    return (
        <>
            <Grid
                container
                onClick={() => goToRoute(`/restaurant/${id}/`)}
                justifyContent="center"
                sx={styles.cardContainer}
            >
                <Grid
                    item
                    container
                    justifyContent="center"
                    sx={{ ...styles.cardContent, backgroundImage: `url('${background}')` }}
                >
                    <Grid item>
                        <CardMedia component="img" alt="Logo restaurante" image={logo} sx={styles.cardImage} />
                    </Grid>
                </Grid>
                <Card sx={styles.cardInfo}>
                    <Grid item container justifyContent="center">
                        <Grid item xs={12} md={12} sm={12}>
                            <Typography paragraph align="center" sx={styles.restaurantName}>
                                {name}
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} mb={2}>
                            <Divider />
                        </Grid>
                        <Grid item xs={12} md={12} sm={12}>
                            <Typography paragraph align="center" sx={styles.restaurantInfo}>
                                <b>Dirección:</b>
                                <br />
                                <span>{address}</span>
                                <br />
                                <b>Número:</b>
                                <br />
                                <span>{phone_number}</span>
                                <br />
                            </Typography>
                        </Grid>
                        <Container></Container>
                    </Grid>
                </Card>
            </Grid>
        </>
    )
}

BasicRestaurantCard.propTypes = {
    restaurantInfo: PropTypes.object.isRequired
}
