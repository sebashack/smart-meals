import { Container, Typography } from '@mui/material'
import PropTypes from 'prop-types'

export function TextFieldError({ message }) {
    return (
        <Container component="div" maxWidth="xs">
            <Typography component="p" color="error" variant="subtitle1" align="center">
                {' '}
                {message}{' '}
            </Typography>
        </Container>
    )
}

TextFieldError.propTypes = {
    message: PropTypes.string.isRequired
}
