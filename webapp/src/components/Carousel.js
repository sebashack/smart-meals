import React from 'react'
import PropTypes from 'prop-types'
import { ArrowBackIosNew, ArrowForwardIos } from '@mui/icons-material'
import { Container, IconButton } from '@mui/material'

const styles = {
    relativeContainer: {
        position: 'relative'
    },
    carouselContainer: {
        overflowX: 'scroll',
        scrollBehavior: 'smooth',
        '::-webkit-scrollbar': {
            display: 'none'
        },
        margin: '20px 0 100px 0',
        padding: '20px 0 20px 0'
    },
    carousel: {
        display: 'flex',
        flexWrap: 'nowrap'
    },
    iconButton: {
        backgroundColor: '#000000',
        color: '#ffffff',
        ':Hover': {
            backgroundColor: '#000000',
            color: '#ffffff'
        }
    },
    leftArrowButton: {
        position: 'absolute',
        top: '50%',
        left: '0'
    },
    rightArrowButton: {
        position: 'absolute',
        top: '50%',
        right: '0'
    }
}

export default function Carousel({ children, contentLength }) {
    function scrollLeft() {
        document.getElementById('carouselContainer').scrollLeft -= 400
    }

    function scrollRight() {
        document.getElementById('carouselContainer').scrollLeft += 400
    }

    const actionButtons =
        contentLength && contentLength > 2 ? (
            <>
                <IconButton onClick={() => scrollLeft()} sx={{ ...styles.iconButton, ...styles.leftArrowButton }}>
                    <ArrowBackIosNew />
                </IconButton>
                <IconButton onClick={() => scrollRight()} sx={{ ...styles.iconButton, ...styles.rightArrowButton }}>
                    <ArrowForwardIos />
                </IconButton>
            </>
        ) : null

    return (
        <div style={styles.relativeContainer}>
            <Container id="carouselContainer" sx={styles.carouselContainer}>
                {actionButtons}
                <div
                    style={
                        contentLength && contentLength > 1
                            ? { ...styles.carousel }
                            : { ...styles.carousel, justifyContent: 'center' }
                    }
                >
                    {children}
                </div>
            </Container>
        </div>
    )
}

Carousel.propTypes = {
    children: PropTypes.node,
    contentLength: PropTypes.number
}
