import React from 'react'
import PropTypes from 'prop-types'
import { Button, Dialog, DialogActions, DialogTitle } from '@mui/material'

const styles = {
    closeButton: {
        borderRadius: '25px',
        margin: 0,
        '&.MuiButtonBase-root': {
            textTransform: 'lowercase',
            minHeight: '0px',
            margin: 0,
            padding: '10px',
            width: 'auto',
            fontWeight: 'bold',
            minWidth: '50px',
            lineHeight: 1,
            boxShadow: 'none'
        }
    },
    dialogActions: {
        '&.MuiDialogActions-root': {
            margin: 0,
            padding: '8px 8px 0 8px'
        }
    },
    dialogTitle: {
        fontWeight: 'bold'
    }
}

export default function Modal({
    children,
    open,
    scroll = 'paper',
    ariaLabelledBy = 'scroll-dialog-title',
    ariaDescribedBy = 'scroll-dialog-description',
    fullWidth = true,
    maxWidth = 'lg',
    handleCloseModal,
    title
}) {
    const dialogTitle = title ? (
        <DialogTitle sx={styles.dialogTitle} id="scroll-dialog-title">
            {title}
        </DialogTitle>
    ) : null

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleCloseModal}
                scroll={scroll}
                aria-labelledby={ariaLabelledBy}
                aria-describedby={ariaDescribedBy}
                fullWidth={fullWidth}
                maxWidth={maxWidth}
            >
                <DialogActions sx={styles.dialogActions}>
                    <Button sx={styles.closeButton} variant="contained" color="error" onClick={handleCloseModal}>
                        x
                    </Button>
                </DialogActions>
                {dialogTitle}
                {children}
            </Dialog>
        </div>
    )
}

Modal.propTypes = {
    children: PropTypes.node,
    open: PropTypes.bool,
    scroll: PropTypes.string,
    ariaLabelledBy: PropTypes.string,
    ariaDescribedBy: PropTypes.string,
    fullWidth: PropTypes.bool,
    maxWidth: PropTypes.string,
    handleCloseModal: PropTypes.func,
    title: PropTypes.string
}
