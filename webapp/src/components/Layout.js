import React, { useState, useEffect } from 'react'
import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Toolbar from '@mui/material/Toolbar'
import Typography from '@mui/material/Typography'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import IconButton from '@mui/material/IconButton'
import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import MenuItem from '@mui/material/MenuItem'
import Menu from '@mui/material/Menu'
import Link from '@mui/material/Link'
import PropTypes from 'prop-types'
import { Container } from '@mui/material'
import { deleteToken } from '../services/user'
import { clearUserSession, getUserSession } from '../utils/session'
import { useRouter } from 'next/router'
import Image from 'next/image'
// import MenuIcon from '@mui/icons-material/Menu'

const style = {
    appBar: {
        backgroundColor: '#FFFFFF',
        boxShadow: 'none',
        border: 2,
        borderColor: '#e6e7e8',
        borderRadius: 2
    },
    logInButton: {
        mt: 1,
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        }
    },
    containerTextLogo: {
        flexGrow: 1
    },
    textLogo: {
        color: '#252656',
        textDecoration: 'none'
    },
    iconButton: {
        color: '#000000'
    },
    boxDropdownMenu: {
        flexGrow: 1,
        display: { xs: 'flex', md: 'none' }
    },
    menuDropdown: {
        display: { xs: 'block', md: 'none' }
    },
    listPagesMenu: {
        flexGrow: 24,
        display: { xs: 'none', md: 'flex' }
    },
    listPagesMenuButton: {
        my: 2,
        color: 'black',
        display: 'block'
    },
    containerChildren: {
        minHeight: '700px'
    },
    footer: {
        backgroundColor: '#efefef',
        color: '#000000',
        padding: '40px'
    },
    col: {
        display: 'flex',
        flexDirection: 'column',
        marginBottom: '20px'
    },
    colTitle: {
        fontWeight: 'bold',
        marginBottom: '10px'
    },
    link: {
        color: '#000000',
        textDecoration: 'none',
        marginBottom: '5px'
    },
    resourcesLink: {
        color: '#000000',
        textDecoration: 'underline',
        marginBottom: '5px'
    },
    isotype: {
        paddingLeft: '40px'
    },
    copy: {
        backgroundColor: '#FFFFFF',
        color: '#000000',
        padding: '16px',
        textAlign: 'center',
        width: '100%'
    },
    copyright: {
        m: 3
    }
}

export function Layout({ children }) {
    const router = useRouter()
    const [state, setState] = useState({
        hasSession: false,
        anchor: null,
        open: false,
        pages: [],
        user: {}
    })

    useEffect(() => {
        const userSession = getUserSession()
        if (userSession) {
            setState({ hasSession: true, user: userSession, pages: GetRoutes(userSession.isAdmin) })
        }
    }, [])

    const handleMenu = (event) => {
        setState({ ...state, anchor: event.currentTarget })
    }

    const handleClose = () => {
        setState({ ...state, anchor: null })
    }

    const handleProfile = () => {
        handleClose()
        if (state.user.isAdmin) router.push('/admin/profile')
        else router.push('/user/profile')
    }

    const handleCloseSesion = async () => {
        setState({ ...state, anchor: null })

        try {
            const { uid, jwt } = getUserSession()
            clearUserSession()
            await deleteToken(jwt, uid)
            router.push('/signin')
        } catch (error) {
            console.log(error)
        }
    }

    // const [anchorElNav, setAnchorElNav] = React.useState(null)

    // const handleOpenNavMenu = (event) => {
    //     setAnchorElNav(event.currentTarget)
    // }
    // const handleCloseNavMenu = () => {
    //     setAnchorElNav(null)
    // }

    const handlePageOptions = (url) => {
        //setAnchorElNav(null)
        router.push(url)
    }

    return (
        <div>
            <AppBar position="static" sx={style.appBar}>
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        {/* {state.hasSession ? (
                            <Box sx={style.boxDropdownMenu}>
                                <IconButton
                                    size="large"
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleOpenNavMenu}
                                    color="black"
                                >
                                    <MenuIcon />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorElNav}
                                    anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'left'
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'left'
                                    }}
                                    open={Boolean(anchorElNav)}
                                    onClose={handleCloseNavMenu}
                                    sx={style.menuDropdown}
                                >
                                    {state.pages.map((page) => (
                                        <MenuItem key={page.name} onClick={() => handlePageOptions(page.url)}>
                                            <Typography textAlign="center">{page.name}</Typography>
                                        </MenuItem>
                                    ))}
                                </Menu>
                            </Box>
                        ) : null} */}
                        <Typography variant="h6" component="div" sx={style.containerTextLogo}>
                            <Link href="/" sx={style.textLogo}>
                                SmartMeals
                            </Link>
                        </Typography>
                        {/* {state.hasSession ? (
                            <Box sx={style.listPagesMenu}>
                                {state.pages.map((page) => (
                                    <Button
                                        key={page.name}
                                        onClick={() => handlePageOptions(page.url)}
                                        sx={style.listPagesMenuButton}
                                    >
                                        {page.name}
                                    </Button>
                                ))}
                            </Box>
                        ) : null} */}

                        {state.hasSession ? (
                            <div>
                                <IconButton
                                    size="large"
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleMenu}
                                    sx={style.iconButton}
                                >
                                    <AccountCircleIcon />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={state.anchor}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right'
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right'
                                    }}
                                    open={Boolean(state.anchor)}
                                    onClose={handleClose}
                                >
                                    <MenuItem onClick={handleProfile}>Perfil</MenuItem>
                                    {state.hasSession
                                        ? state.pages.map((page) => (
                                              <MenuItem key={page.name} onClick={() => handlePageOptions(page.url)}>
                                                  {page.name}
                                              </MenuItem>
                                          ))
                                        : null}

                                    <MenuItem onClick={handleCloseSesion}>Log Out</MenuItem>
                                </Menu>
                            </div>
                        ) : (
                            <LogInButton />
                        )}
                    </Toolbar>
                </Container>
            </AppBar>
            <Container sx={style.containerChildren}>{children}</Container>
            <Footer />
        </div>
    )
}

function GetRoutes(isAdmin) {
    const adminPages = [
        {
            name: 'Mis restaurantes',
            url: '/admin/restaurants'
        }
    ]

    const userPages = []

    return isAdmin ? adminPages : userPages
}

const LogInButton = () => {
    return (
        <div>
            <Button href="/signin" fullWidth variant="contained" sx={style.logInButton}>
                Iniciar Sesión
            </Button>
        </div>
    )
}

function Footer() {
    return (
        <Box component="footer" style={style.footer}>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6} md={3}>
                    <Link href="/" style={style.link}>
                        <Image src="/isotipo.svg" alt="logo" width={120} height={120} style={style.isotype} />
                    </Link>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <div style={style.col}>
                        <div style={style.colTitle}>Compañía</div>
                        <Link href="#" style={style.link}>
                            Sobre nosotros
                        </Link>
                        <Link href="#" style={style.link}>
                            Contáctanos
                        </Link>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <div style={style.col}>
                        <div style={style.colTitle}>Políticas</div>
                        <Link href="#" style={style.link}>
                            Términos y condiciones
                        </Link>
                        <Link href="#" style={style.link}>
                            Privacidad
                        </Link>
                    </div>
                </Grid>
                <Grid item xs={12} sm={6} md={3}>
                    <div style={style.col}>
                        <div style={style.colTitle}>Recursos</div>
                        <Link href="#" style={style.link}>
                            Comunidad
                        </Link>
                        <Link href="#" style={style.link}>
                            Atención al usuario
                        </Link>
                        <Link href="#" style={style.link}>
                            Preguntas frecuentes
                        </Link>
                    </div>
                </Grid>
            </Grid>
            <br />
            <br />
            <br />
            <Copyright />
        </Box>
    )
}

function Copyright() {
    return (
        <Typography variant="body2" align="center">
            {'© '}
            <Link href="#" style={{ color: 'inherit' }}>
                Smart Meals
            </Link>{' '}
            {new Date().getFullYear()}
        </Typography>
    )
}

Layout.propTypes = {
    children: PropTypes.node,
    hasSession: PropTypes.bool
}
