import React from 'react'
import { styled } from '@mui/material/styles'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import ButtonBase from '@mui/material/ButtonBase'
import Rating from '@mui/material/Rating'
import PropTypes from 'prop-types'

const styles = {
    paper: {
        p: 2,
        margin: 'auto',
        marginBottom: '35px',
        maxWidth: 800,
        backgroundColor: (theme) => (theme.palette.mode === 'dark' ? '#1A2027' : '#fff')
    },
    gridContainer: {
        placeItems: 'center',
        cursor: 'pointer'
    },
    itemImg: {
        width: '100%',
        '@media only screen and  (min-width: 660px)': {
            maxWidth: 300
        }
    },
    rating: {
        '&.MuiRating-root span': {
            fontSize: '40px'
        },
        marginBottom: '10px'
    },
    boldText: {
        fontWeight: 'bold'
    }
}

const Img = styled('img')({
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%'
})

export function UserReviewCard({ imageUrl, content, itemTitle, rating }) {
    return (
        <Paper sx={styles.paper}>
            <Grid container sx={styles.gridContainer} justifyContent="center" spacing={2}>
                <Grid item>
                    <ButtonBase sx={styles.itemImg}>
                        <Img alt="Meal image" src={imageUrl} />
                    </ButtonBase>
                </Grid>
                <Grid item xs={12} sm container>
                    <Grid item container direction="row" spacing={2}>
                        <Grid item xs>
                            <Typography gutterBottom variant="h4" sx={styles.boldText}>
                                Reseña | {itemTitle}
                            </Typography>
                            <Rating name="rating" defaultValue={rating} precision={0.5} sx={styles.rating} readOnly />
                            <Typography
                                variant="body2"
                                sx={{
                                    overflowWrap: 'anywhere'
                                }}
                            >
                                {content}
                            </Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    )
}

UserReviewCard.propTypes = {
    imageUrl: PropTypes.string,
    rating: PropTypes.number,
    itemTitle: PropTypes.string,
    content: PropTypes.string
}
