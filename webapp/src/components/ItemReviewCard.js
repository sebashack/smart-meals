import { Stack, Typography, Paper, Rating, Button } from '@mui/material'
import ThumbUpIcon from '@mui/icons-material/ThumbUp'
import PropTypes from 'prop-types'

export default function ItemReviewCard({ id, user, rating, description, likes, onClickLike }) {
    const style = {
        paper: {
            my: 2,
            p: 2,
            backgroundColor: (theme) => (theme.palette.mode === 'dark' ? '#1A2027' : '#fff')
        },
        rating: {
            '&.MuiRating-root span': {
                fontSize: '40px'
            },
            marginBottom: '10px',
            m: 2
        },
        typographyUser: {
            m: 2
        },
        typographyDescription: {
            m: 2
        },
        likeButton: {
            width: '20px',
            backgroundColor: 'black',
            borderRadius: 2
        }
    }

    return (
        <Paper sx={style.paper}>
            <Stack>
                <Typography variant="h5" sx={style.typographyUser}>
                    {user}
                </Typography>
                <Rating name="rating" defaultValue={rating} precision={0.5} sx={style.rating} readOnly />
                <Typography variant="p" sx={style.typographyDescription}>
                    {description}
                </Typography>
                <Button
                    variant="contained"
                    sx={style.likeButton}
                    onClick={() => {
                        onClickLike(id)
                    }}
                >
                    <Stack direction="row" spacing={1}>
                        <ThumbUpIcon />
                        <p>{likes}</p>
                    </Stack>
                </Button>
            </Stack>
        </Paper>
    )
}

ItemReviewCard.propTypes = {
    id: PropTypes.string,
    user: PropTypes.string,
    rating: PropTypes.number,
    description: PropTypes.string,
    likes: PropTypes.number,
    onClickLike: PropTypes.func
}
