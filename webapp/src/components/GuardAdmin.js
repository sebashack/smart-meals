import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { getUserSession } from '@/utils/session'
import PropTypes from 'prop-types'

export function GuardAdmin({ children }) {
    const [state, setState] = useState({
        loading: true
    })
    const router = useRouter()
    useEffect(() => {
        const userSession = getUserSession()
        if (!(userSession && userSession.isAdmin)) {
            router.push('/signin')
        } else {
            setState({ loading: false })
        }
    }, [router])

    return <>{state.loading ? <h1>Cargando...</h1> : children}</>
}

GuardAdmin.propTypes = {
    children: PropTypes.node
}
