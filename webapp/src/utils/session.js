import jwt_decode from 'jwt-decode'

export function decodeJWT(jwt) {
    if (jwt) {
        return jwt_decode(jwt)
    } else {
        return null
    }
}

export function mkSession(uid, email, jwt, isAdmin) {
    return { uid, email, jwt, isAdmin }
}

export function storeUserSession({ uid, email, jwt, isAdmin }) {
    if (typeof window !== 'undefined') {
        localStorage.setItem('sm_uid', uid)
        localStorage.setItem('sm_user_email', email)
        localStorage.setItem('sm_jwt', jwt)
        localStorage.setItem('sm_is_admin', isAdmin)
    }
}

export function getUserSession() {
    if (typeof window !== 'undefined') {
        const jwt = localStorage.getItem('sm_jwt')

        if (jwt) {
            const uid = localStorage.getItem('sm_uid')
            const email = localStorage.getItem('sm_user_email')
            const isAdmin = localStorage.getItem('sm_is_admin') == 'true' ? true : false

            return { uid, email, jwt, isAdmin }
        } else {
            return null
        }
    } else {
        return null
    }
}

export function clearUserSession() {
    if (typeof window !== 'undefined') {
        localStorage.removeItem('sm_uid')
        localStorage.removeItem('sm_user_email')
        localStorage.removeItem('sm_jwt')
        localStorage.removeItem('sm_is_admin')
    }
}
