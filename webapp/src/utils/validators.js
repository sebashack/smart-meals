import { default as passwordValidator } from 'password-validator'
import validator from 'validator'

export function signupValidator({ name, email, password, confirmPassword, country, city }) {
    const nameValidation = validateTextField(name)
    const emailValidation = validateEmail(email)
    const passwordValidation = validatePassword(password, confirmPassword)
    const countryValidation = validateTextField(country)
    const cityValidation = validateTextField(city)
    const allValid =
        nameValidation.isValid &&
        emailValidation.isValid &&
        passwordValidation.isValid &&
        countryValidation.isValid &&
        cityValidation.isValid

    return {
        nameValidation,
        emailValidation,
        passwordValidation,
        countryValidation,
        cityValidation,
        allValid
    }
}

export function signinValidator({ email, password }) {
    const emailValidation = nullStringValidator(email)
    const passwordValidation = nullStringValidator(password)
    const allValid = emailValidation.isValid && passwordValidation.isValid

    return {
        emailValidation,
        passwordValidation,
        allValid
    }
}

export function restaurantValidator({
    name,
    email,
    city,
    country,
    address,
    phone_number,
    description,
    logoImage,
    backgroundImage
}) {
    const nameValidation = validateTextField(name)
    const emailValidation = validateEmail(email)
    const countryValidation = validateTextField(country)
    const cityValidation = validateTextField(city)
    const addressValidation = nullStringValidator(address)
    const phoneNumberValidation = phoneNumberValidator(phone_number)
    const descriptionValidation = nullStringValidator(description)
    const logoFileValidation = imageValidator(logoImage)
    const backgroundFileValidation = imageValidator(backgroundImage)
    const allValid =
        nameValidation.isValid &&
        emailValidation.isValid &&
        countryValidation.isValid &&
        cityValidation.isValid &&
        addressValidation.isValid &&
        phoneNumberValidation.isValid &&
        descriptionValidation.isValid &&
        logoFileValidation.isValid &&
        backgroundFileValidation.isValid

    return {
        nameValidation,
        emailValidation,
        countryValidation,
        cityValidation,
        addressValidation,
        phoneNumberValidation,
        descriptionValidation,
        logoFileValidation,
        backgroundFileValidation,
        allValid
    }
}

export function updateRestaurantValidator({
    name,
    email,
    city,
    country,
    address,
    phone_number,
    description,
    logoImage,
    backgroundImage
}) {
    const nameValidation = validateTextField(name)
    const emailValidation = validateEmail(email)
    const countryValidation = validateTextField(country)
    const cityValidation = validateTextField(city)
    const addressValidation = nullStringValidator(address)
    const phoneNumberValidation = phoneNumberValidator(phone_number)
    const descriptionValidation = nullStringValidator(description)
    const logoFileValidation = updateImageValidator(logoImage)
    const backgroundFileValidation = updateImageValidator(backgroundImage)
    const allValid =
        nameValidation.isValid &&
        emailValidation.isValid &&
        countryValidation.isValid &&
        cityValidation.isValid &&
        addressValidation.isValid &&
        phoneNumberValidation.isValid &&
        descriptionValidation.isValid &&
        logoFileValidation.isValid &&
        backgroundFileValidation.isValid

    return {
        nameValidation,
        emailValidation,
        countryValidation,
        cityValidation,
        addressValidation,
        phoneNumberValidation,
        descriptionValidation,
        logoFileValidation,
        backgroundFileValidation,
        allValid
    }
}

export function itemValidator({ title, description, price, meal_type_id, itemImage }) {
    const titleValidation = validateTextField(title)
    const descriptionValidation = nullStringValidator(description)
    const priceValidation = intValidator(price)
    const mealTypeIdValidation = nullStringValidator(meal_type_id)
    const itemImageValidation = imageValidator(itemImage)
    const allValid =
        titleValidation.isValid &&
        descriptionValidation.isValid &&
        priceValidation.isValid &&
        mealTypeIdValidation.isValid &&
        itemImageValidation.isValid

    return {
        titleValidation,
        descriptionValidation,
        priceValidation,
        mealTypeIdValidation,
        itemImageValidation,
        allValid
    }
}

export function updateItemValidator({ title, description, price, meal_type_id, itemImage }) {
    const titleValidation = validateTextField(title)
    const descriptionValidation = nullStringValidator(description)
    const priceValidation = intValidator(price)
    const mealTypeIdValidation = nullStringValidator(meal_type_id)
    const itemImageValidation = updateImageValidator(itemImage)
    const allValid =
        titleValidation.isValid &&
        descriptionValidation.isValid &&
        priceValidation.isValid &&
        mealTypeIdValidation.isValid &&
        itemImageValidation.isValid

    return {
        titleValidation,
        descriptionValidation,
        priceValidation,
        mealTypeIdValidation,
        itemImageValidation,
        allValid
    }
}

export function mealTypeValidator({ title }) {
    const titleValidation = validateTextField(title)

    const allValid = titleValidation.isValid

    return {
        titleValidation,
        allValid
    }
}

export function reviewValidator({ content }) {
    const contentValidation = validateReviewContent(content)

    const allValid = contentValidation.isValid

    return {
        contentValidation,
        allValid
    }
}

// Helpers
function phoneNumberValidator(txt) {
    const reg = /^\(?\d{3}\)?((-)?|(\s|-)?)\d{3}(\s|-)?\d{2}(\s|-)?\d{2}$/

    if (!reg.test(txt)) {
        return { isValid: false, message: 'formato inválido.' }
    }

    return { isValid: true, message: null }
}

function validateReviewContent(text) {
    if (text.length > 500) {
        return { isValid: false, message: 'La reseña debe tener menos de 500 caracteres.' }
    }

    return { isValid: true, message: null }
}

function imageValidator(file) {
    if (!file) {
        return { isValid: false, message: 'el archivo es requerido.' }
    }

    const { type, size } = file
    const maxImgSize = 3000000 // 3MB

    if (type != 'image/jpeg' && type != 'image/png') {
        return { isValid: false, message: 'formato inválido, se aceptan [JPEG, PNG].' }
    }
    if (size > maxImgSize) {
        return { isValid: false, message: 'el archivo es muy grande, se aceptan menores o igual a 3MB.' }
    }

    return { isValid: true, message: null }
}

function updateImageValidator(image) {
    if (!image) {
        return { isValid: true, message: null }
    }
    const { type, size } = image
    const maxImgSize = 3000000 // 3MB

    if (type != 'image/jpeg' && type != 'image/png') {
        return { isValid: false, message: 'formato inválido, se aceptan [JPEG, PNG].' }
    }
    if (size > maxImgSize) {
        return { isValid: false, message: 'el archivo es muy grande, se aceptan menores o igual a 3MB.' }
    }

    return { isValid: true, message: null }
}

function nullStringValidator(string) {
    return string ? { isValid: true, message: '' } : { isValid: false, message: 'no puede ser Vacío' }
}

function intValidator(number) {
    const reg = /^\d+$/
    if (!reg.test(number)) {
        return { isValid: false, message: 'debe ser un entero positivo' }
    }
    return { isValid: true, message: null }
}

function validatePassword(password, confirmPassword) {
    const schema = new passwordValidator()
    const passSchema = schema
        .is()
        .min(8)
        .is()
        .max(30)
        .has()
        .uppercase()
        .has()
        .lowercase()
        .has()
        .digits(1)
        .has()
        .symbols()
        .has()
        .not()
        .spaces()
    const errors = passSchema.validate(password, { list: true })

    if (errors.includes('min') || errors.includes('max')) {
        return { isValid: false, message: 'debe tener entre 8 y 30 caracteres.' }
    }

    if (errors.includes('uppercase')) {
        return { isValid: false, message: 'debe tener un caracter en mayúscula' }
    }

    if (errors.includes('lowercase')) {
        return { isValid: false, message: 'debe tener un caracter en minúscula' }
    }

    if (errors.includes('digits')) {
        return { isValid: false, message: 'debe tener almenos un dígito' }
    }

    if (errors.includes('symbols')) {
        return { isValid: false, message: 'debe tener alemnos un caracter especial' }
    }

    if (errors.includes('spaces')) {
        return { isValid: false, message: 'no puede tener espacios' }
    }

    if (password != confirmPassword) {
        return { isValid: false, message: 'no coincide con la confimación' }
    }

    return { isValid: true, message: null }
}

export function validateEmail(email) {
    if (email.length < 1) {
        return { isValid: false, message: 'no puede ser Vacío' }
    }

    if (!validator.isEmail(email)) {
        return { isValid: false, message: 'formato inválido' }
    }

    return { isValid: true, message: null }
}

export function validateTextField(name) {
    if (name.length < 2 || name.length > 45) {
        return { isValid: false, message: 'debe estar entre 2 y 45 caracteres' }
    }

    return { isValid: true, message: null }
}
