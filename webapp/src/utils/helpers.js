export function priceFormatter(price, currency = 'USD') {
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        minimumFractionDigits: 1,
        currency
    })

    return formatter.format(price)
}
