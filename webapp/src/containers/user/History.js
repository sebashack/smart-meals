import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Button, Grid } from '@mui/material'

import { getUserSession } from '@/utils/session'
import { getUserReviews } from '@/services/user'
import { UserReviewCard } from '@/components/UserReviewCard'
import { getImgUrl } from '@/services/image'

const styles = {
    primaryButton: {
        mt: 1,
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            color: '#ffffff',
            transform: 'scale(1.1)'
        },
        '&.MuiButtonBase-root': {
            height: 'min-content',
            margin: 0
        },
        '&.MuiButtonBase-root p': {
            margin: 0
        }
    }
}

export function History() {
    const [state, setState] = useState({
        fetchData: null,
        currentSelected: 'Quality'
    })

    useEffect(() => {
        const session = getUserSession()

        const fetchData = async () => {
            const fetchReviews = await getUserReviews(session.jwt, session.uid, state.currentSelected, 0, 10)

            if (fetchReviews.isSuccess) {
                setState((prevSt) => ({ ...prevSt, fetchData: fetchReviews.data }))
            } else {
                console.log(fetchReviews.error)
            }
        }

        fetchData()
    }, [state.currentSelected])

    const onClickCat = (category) => {
        return async (evt) => {
            evt.preventDefault()
            setState((prevSt) => ({ ...prevSt, currentSelected: category }))
        }
    }

    const categories = [
        { onClick: onClickCat('Quality'), title: 'Calidad', disabled: state.currentSelected === 'Quality' },
        { onClick: onClickCat('Healthiness'), title: 'Salud', disabled: state.currentSelected === 'Healthiness' },
        {
            onClick: onClickCat('PriceFairness'),
            title: 'Economía',
            disabled: state.currentSelected === 'PriceFairness'
        },
        { onClick: onClickCat('Ecology'), title: 'Ecología', disabled: state.currentSelected === 'Ecology' }
    ]

    return (
        <Grid container direction="row">
            <Grid item xs={12} sm={12} md={12} container direction="row" spacing={2} mt={4}>
                <ReviewFilters categories={categories} />
            </Grid>
            <Grid item container direction="row" spacing={4} mt={2}>
                <UserReviews reviews={state.fetchData} />
            </Grid>
        </Grid>
    )
}

function ReviewFilters({ categories }) {
    return categories.map((categorie) => (
        <Grid item key={categorie.title}>
            <Button
                onClick={categorie.onClick}
                disabled={categorie.disabled}
                variant="contained"
                sx={styles.primaryButton}
            >
                <p>{categorie.title}</p>
            </Button>
        </Grid>
    ))
}

function UserReviews({ reviews }) {
    const userReviews = reviews
        ? reviews.map((review) => (
              <Grid key={review.created_at} item xs={12} md={6} sm={6}>
                  <UserReviewCard
                      imageUrl={getImgUrl(review.image_id, 500, 500)}
                      content={review.content}
                      itemTitle={review.item_title}
                      rating={review.score}
                      price={review.price}
                  />
              </Grid>
          ))
        : null

    return <>{userReviews}</>
}

ReviewFilters.propTypes = {
    categories: PropTypes.array
}

UserReviews.propTypes = {
    reviews: PropTypes.array
}
