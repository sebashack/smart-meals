import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { Grid, Button } from '@mui/material'
import { ItemRecommendationCard } from '@/components/ItemRecommendationCard'
import { getImgUrl } from '@/services/image'
import { getUserRecommendationByCategory } from '@/services/user'
import { getUserSession } from '@/utils/session'

const styles = {
    primaryButton: {
        mt: 1,
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            color: '#ffffff',
            transform: 'scale(1.1)'
        },
        '&.MuiButtonBase-root': {
            height: 'min-content',
            margin: 0
        },
        '&.MuiButtonBase-root p': {
            margin: 0
        }
    }
}

export function Recommendations() {
    const [state, setState] = useState({
        currentSelected: 'Quality'
    })
    const router = useRouter()

    useEffect(() => {
        const session = getUserSession()

        const fetchData = async () => {
            const fetchReviews = await getUserRecommendationByCategory(
                session.jwt,
                session.uid,
                state.currentSelected,
                0,
                10
            )

            if (fetchReviews.isSuccess) {
                setState((prevSt) => ({ ...prevSt, fetchData: fetchReviews.data }))
            } else {
                console.log(fetchReviews.error)
            }
        }

        fetchData()
    }, [state.currentSelected])

    const onClickCat = (category) => {
        return async (evt) => {
            evt.preventDefault()
            setState((prevSt) => ({ ...prevSt, currentSelected: category }))
        }
    }

    const categories = [
        { onClick: onClickCat('Quality'), title: 'Calidad', disabled: state.currentSelected === 'Quality' },
        { onClick: onClickCat('Healthiness'), title: 'Salud', disabled: state.currentSelected === 'Healthiness' },
        {
            onClick: onClickCat('PriceFairness'),
            title: 'Economía',
            disabled: state.currentSelected === 'PriceFairness'
        },
        { onClick: onClickCat('Ecology'), title: 'Ecología', disabled: state.currentSelected === 'Ecology' }
    ]

    const onClickItem = (restaurantId, mealTypeId, itemName) => {
        router.push(`/restaurant/${restaurantId}/menu?defaultMealTypeId=${mealTypeId}&defaultItemName=${itemName}`)
    }

    return (
        <>
            <Grid container direction="row">
                <Grid item xs={12} sm={12} md={12} container direction="row" spacing={2} mt={4}>
                    <RecommendationFilters categories={categories} />
                </Grid>
                <Grid item container direction="row" spacing={4} mt={2}>
                    <UserRecommendations recommendations={state.fetchData} onClickItem={onClickItem} />
                </Grid>
            </Grid>
        </>
    )
}

function RecommendationFilters({ categories }) {
    return categories.map((categorie, index) => (
        <Grid item key={index}>
            <Button
                onClick={categorie.onClick}
                disabled={categorie.disabled}
                variant="contained"
                sx={styles.primaryButton}
            >
                <p>{categorie.title}</p>
            </Button>
        </Grid>
    ))
}

function UserRecommendations({ recommendations, onClickItem }) {
    const userRecommendations = recommendations
        ? recommendations.map((recommendation, index) => (
              <Grid key={index} item xs={12} md={6} sm={6}>
                  <>
                      <ItemRecommendationCard
                          imageUrl={getImgUrl(recommendation.image_id, 500, 500)}
                          name={recommendation.item_title}
                          description={recommendation.description}
                          rating={recommendation.score}
                          price={recommendation.price}
                          restaurantName={recommendation.restaurant_name}
                          mealTypeTitle={recommendation.meal_type_title}
                          onClick={() =>
                              onClickItem(
                                  recommendation.restaurant_id,
                                  recommendation.meal_type_id,
                                  recommendation.item_title
                              )
                          }
                      />
                  </>
              </Grid>
          ))
        : null

    return <>{userRecommendations}</>
}

RecommendationFilters.propTypes = {
    categories: PropTypes.array
}

UserRecommendations.propTypes = {
    recommendations: PropTypes.array,
    onClickItem: PropTypes.func
}
