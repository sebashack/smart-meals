import React, { useState } from 'react'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import FormControlLabel from '@mui/material/FormControlLabel'
import TextField from '@mui/material/TextField'
import Link from '@mui/material/Link'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import Checkbox from '@mui/material/Checkbox'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Typography from '@mui/material/Typography'
import Container from '@mui/material/Container'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { mkPostUserPayload, postUser } from '@/services/user'
import { decodeJWT, mkSession, storeUserSession } from '@/utils/session'
import { signupValidator } from '@/utils/validators'
import { TextFieldError } from '@/components/TextFieldError'

const style = {
    boxContainer: {
        marginTop: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    iconForm: {
        m: 1,
        bgcolor: 'secondary.main'
    },
    boxContainerForm: {
        mt: 3
    },
    signUpButtom: {
        mt: 3,
        mb: 2
    }
}

export default function SignUp() {
    const router = useRouter()
    const [state, setState] = useState({
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
        confirmationCode: '',
        country: '',
        city: '',
        isAdmin: false,
        birthdate: '2000-01-01',
        validations: null,
        submitDisabled: true,
        isDuplicatedEmail: false
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value, isDuplicatedEmail: false }
                newState.validations = signupValidator(newState)

                if (newState.validations.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }

            setState(updateState)
        }
    }

    const handleCheckboxChange = async (evt) => {
        evt.preventDefault()
        setState((prevSt) => ({ ...prevSt, isAdmin: !prevSt.isAdmin }))
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        if (state.validations && state.validations.allValid) {
            const userData = mkPostUserPayload(
                state.email,
                state.name,
                state.password,
                state.city,
                state.country,
                state.birthdate,
                state.isAdmin
            )

            const result = await postUser(userData)

            if (result.isSuccess) {
                const jwt = result.data.jwt
                const payload = decodeJWT(jwt)
                const session = mkSession(payload.sub, payload.aud, jwt, state.isAdmin)

                storeUserSession(session)

                router.push('/')
            } else {
                if (result.error.isDuplicatedEmail) {
                    setState((prevSt) => ({ ...prevSt, isDuplicatedEmail: true }))
                } else {
                    console.error(result.error.description)
                }
            }
        } else {
            console.log('Sign up attempt with invalid fields')
        }
    }

    const duplicatedEmailError = state.isDuplicatedEmail ? <TextFieldError message="Correo electrónico en uso" /> : null

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box sx={style.boxContainer}>
                <Avatar sx={style.iconForm}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Registrarse
                </Typography>
                <Box component="form" noValidate onSubmit={handleSubmit} sx={style.boxContainerForm}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                autoComplete="given-name"
                                name="name"
                                required
                                fullWidth
                                id="name"
                                label="Nombre completo"
                                autoFocus
                                onChange={handleInputChange('name')}
                            />
                            <FieldError validations={state.validations} validationType="nameValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                id="email"
                                label="Correo electrónico"
                                name="email"
                                autoComplete="email"
                                onChange={handleInputChange('email')}
                            />
                            <FieldError validations={state.validations} validationType="emailValidation" />
                            {duplicatedEmailError}
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="date"
                                label="Fecha de nacimiento"
                                type="date"
                                defaultValue="2000-01-01"
                                InputLabelProps={{ shrink: true }}
                                onChange={handleInputChange('birthdate')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="country"
                                variant="outlined"
                                required
                                fullWidth
                                id="country"
                                label="País"
                                onChange={handleInputChange('country')}
                            />
                            <FieldError validations={state.validations} validationType="countryValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="city"
                                variant="outlined"
                                required
                                fullWidth
                                id="city"
                                label="Ciudad"
                                onChange={handleInputChange('city')}
                            />
                            <FieldError validations={state.validations} validationType="cityValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                name="password"
                                label="Contraseña"
                                type="password"
                                id="password"
                                autoComplete="new-password"
                                onChange={handleInputChange('password')}
                            />
                            <FieldError validations={state.validations} validationType="passwordValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="confirmPassword"
                                label="Confirmar Contraseña"
                                type="password"
                                id="confirmPassword"
                                onChange={handleInputChange('confirmPassword')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControlLabel
                                control={<Checkbox onChange={handleCheckboxChange} value="isAdmin" color="primary" />}
                                label="Soy administrador de restaurante"
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={style.signUpButtom}
                        disabled={state.submitDisabled}
                    >
                        Registrarse
                    </Button>
                    <Grid container justifyContent="flex-end">
                        <Grid item>
                            <Link href="/signin" variant="body2">
                                ¿Ya tiene cuenta? Inicia sesión
                            </Link>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
        </Container>
    )
}

function FieldError({ validations, validationType }) {
    const fieldError =
        !validations || validations[validationType].isValid ? null : (
            <TextFieldError message={validations[validationType].message} />
        )
    return fieldError
}

FieldError.propTypes = {
    validationType: PropTypes.string.isRequired,
    validations: PropTypes.object
}
