import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { RestaurantCard } from '@/components/RestaurantCard'
import { getImgUrl } from '@/services/image'
import { getMenusAndItemsByRestaurantId } from '@/services/menu'
import { getRestaurantById } from '@/services/restaurant'
import { getUserSession } from '@/utils/session'

export default function Restaurant({ restaurantId }) {
    const [restaurantInfo, setRestaurantInfo] = useState({
        id: null,
        name: '',
        address: '',
        phone_number: '',
        description: '',
        email: '',
        logo: '',
        background: '',
        hasMenu: false
    })

    useEffect(() => {
        async function fetchData() {
            const { id, name, address, phone_number, description, email, background_image_id, logo_id, admin_id } =
                await getRestaurantById(restaurantId)
            const backgroundUrl = getImgUrl(background_image_id, 1000, 700)
            const logoUrl = getImgUrl(logo_id, 600, 600)
            const menu = await getMenusAndItemsByRestaurantId(restaurantId)
            const userSession = getUserSession()
            const isAdmin = userSession && userSession.isAdmin && userSession.uid === admin_id
            setRestaurantInfo((prevSt) => ({
                ...prevSt,
                id,
                name,
                address,
                phone_number,
                description,
                email,
                logo: logoUrl,
                background: backgroundUrl,
                hasMenu: menu ? true : false,
                isAdmin
            }))
        }
        fetchData()
    }, [restaurantId])

    return (
        <div>
            <RestaurantCard restaurantInfo={restaurantInfo} />
        </div>
    )
}

Restaurant.propTypes = {
    restaurantId: PropTypes.string.isRequired
}
