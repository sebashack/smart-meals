import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { getUserSession } from '@/utils/session'
import { deleteMealType } from '@/services/mealType'

export default function DeleteMealType({ mealTypeId }) {
    const router = useRouter()
    useEffect(() => {
        async function fetchPromise() {
            const { uid, jwt, isAdmin } = getUserSession()

            if (isAdmin) {
                await deleteMealType(uid, jwt, mealTypeId)
            }

            router.back()
        }

        fetchPromise()
    }, [mealTypeId, router])

    return (
        <>
            <h1>Deleting...</h1>
        </>
    )
}

DeleteMealType.propTypes = {
    mealTypeId: PropTypes.string.isRequired
}
