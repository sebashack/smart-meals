import { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Container, Stack, Typography, Avatar, FormControl, TextField, Button, Alert } from '@mui/material'
import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu'
import { mkPostMealType, postMealType } from '@/services/mealType'
import { getUserSession } from '@/utils/session'
import { mealTypeValidator } from '@/utils/validators'
import { getMenusAndItemsByRestaurantId } from '@/services/menu'

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center'
    },
    stack: {
        width: { xs: '100%', sm: '300px', md: '400px' },
        display: 'flex',
        alignItems: 'center'
    },
    avatar: {
        m: 1,
        bgcolor: '#FF6161'
    },
    inputLabelSelect: {
        mt: 1
    },
    submitButton: {
        mt: 2,
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00BA77',
            transform: 'scale(1.1)'
        }
    }
}

export default function AddMealType({ restaurantId }) {
    const [state, setState] = useState({
        title: '',
        menuId: null,
        submitDisabled: true,
        showSucessAlert: false
    })

    useEffect(() => {
        async function fetchData() {
            const { menu } = await getMenusAndItemsByRestaurantId(restaurantId)
            const menuTitle = menu.title
            setState((prevSt) => ({ ...prevSt, menuId: menu.id, menuTitle }))
        }
        fetchData()
    }, [restaurantId])

    const handleSubmit = async () => {
        event.preventDefault()

        if (state.validator && state.validator.allValid) {
            const mealTypeData = mkPostMealType(state.title)
            const { uid, jwt } = getUserSession()
            const result = await postMealType(uid, jwt, mealTypeData, state.menuId)
            if (result.isSuccess) {
                setState((prevSt) => ({ ...prevSt, showSucessAlert: true }))
            } else {
                setState((prevSt) => ({ ...prevSt, showSucessAlert: false }))
            }
        } else {
            console.log('Register attempt with invalid fields')
        }
    }

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }
                newState.validator = mealTypeValidator(newState)
                if (newState.validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }
            setState(updateState)
        }
    }

    const handleOnCloseAlert = (attr) => {
        return () => {
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: false }
                return newState
            }
            setState(updateState)
        }
    }

    return (
        <Container sx={styles.container}>
            <Stack direction="column" sx={styles.stack} spacing={1}>
                <Typography variant="h5">Añadir tipo de comida del menu {state.menuTitle}</Typography>
                <Avatar sx={styles.avatar}>
                    <RestaurantMenuIcon />
                </Avatar>
                {state.showSucessAlert ? <SucessAlert onClose={handleOnCloseAlert} /> : null}
                <form onSubmit={handleSubmit} id="create-item-form">
                    <FormControl fullWidth>
                        <TextField
                            required
                            fullWidth
                            id="title"
                            label="Titulo"
                            name="title"
                            onChange={handleInputChange('title')}
                            value={state.title}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            disabled={state.submitDisabled}
                            sx={styles.submitButton}
                        >
                            Añadir
                        </Button>
                    </FormControl>
                </form>
            </Stack>
        </Container>
    )
}

const SucessAlert = ({ onClose }) => (
    <Alert severity="success" onClose={onClose('showSucessAlert')}>
        El tipo de comida se creó correctamente!
    </Alert>
)

SucessAlert.propTypes = {
    onClose: PropTypes.func.isRequired
}

AddMealType.propTypes = {
    restaurantId: PropTypes.string.isRequired
}
