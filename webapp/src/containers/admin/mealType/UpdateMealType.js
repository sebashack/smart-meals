import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Container, Stack, Typography, Avatar, FormControl, TextField, Button, Alert } from '@mui/material'
import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu'
import { mealTypeValidator } from '@/utils/validators'
import { mkPutMealType, putMealType } from '@/services/mealType'
import { getMenusAndItemsByRestaurantId } from '@/services/menu'
import { getUserSession } from '@/utils/session'
import { useRouter } from 'next/router'

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center'
    },
    stack: {
        width: { xs: '100%', sm: '300px', md: '400px' },
        display: 'flex',
        alignItems: 'center'
    },
    avatar: {
        m: 1,
        bgcolor: '#FF6161'
    },
    inputLabelSelect: {
        mt: 1
    },
    submitButton: {
        mt: 2,
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00BA77',
            transform: 'scale(1.1)'
        }
    }
}

export default function UpdateMealType({ restaurantId, mealTypeId }) {
    const router = useRouter()
    const [state, setState] = useState({
        title: '',
        submitDisabled: true,
        showSucessAlert: false
    })

    useEffect(() => {
        const fetchData = async () => {
            const { menu } = await getMenusAndItemsByRestaurantId(restaurantId)
            const menuTitle = menu.title
            const title = menu.meal_types.filter((mealType) => mealType.id === mealTypeId)[0].title
            setState((prvst) => ({ ...prvst, menuTitle, title }))
        }
        fetchData()
    }, [restaurantId, mealTypeId])

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }
                newState.validator = mealTypeValidator(newState)
                if (newState.validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }
            setState(updateState)
        }
    }

    const handleOnCloseAlert = (attr) => {
        return () => {
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: false }
                return newState
            }
            setState(updateState)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        if (state.validator && state.validator.allValid) {
            const mealTypeData = mkPutMealType(state.title)

            const { uid, jwt, isAdmin } = getUserSession()

            if (!isAdmin) {
                router.push('/profile')
            }

            const result = await putMealType(jwt, mealTypeData, uid, mealTypeId)
            if (result.isSuccess) {
                console.log('Sucess')
                setState((prevSt) => ({ ...prevSt, showSucessAlert: true }))
            } else {
                setState((prevSt) => ({ ...prevSt, showSucessAlert: false }))
                console.error(result.error.description)
            }
        } else {
            console.log('Register attempt with invalid fields')
        }
    }

    return (
        <Container sx={styles.container}>
            <Stack direction="column" sx={styles.stack} spacing={1}>
                <Typography variant="h5">Editar tipo de comida del menu {state.menuTitle}</Typography>
                <Avatar sx={styles.avatar}>
                    <RestaurantMenuIcon />
                </Avatar>
                {state.showSucessAlert ? <SucessAlert onClose={handleOnCloseAlert} /> : null}
                <form onSubmit={handleSubmit} id="create-item-form">
                    <FormControl fullWidth>
                        <TextField
                            required
                            fullWidth
                            id="title"
                            label="Titulo"
                            name="title"
                            onChange={handleInputChange('title')}
                            value={state.title}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            disabled={state.submitDisabled}
                            sx={styles.submitButton}
                        >
                            Editar
                        </Button>
                    </FormControl>
                </form>
            </Stack>
        </Container>
    )
}

const SucessAlert = ({ onClose }) => (
    <Alert severity="success" onClose={onClose('showSucessAlert')}>
        El tipo de comida se actualizó correctamente!
    </Alert>
)

SucessAlert.propTypes = {
    onClose: PropTypes.func.isRequired
}

UpdateMealType.propTypes = {
    mealTypeId: PropTypes.string.isRequired,
    restaurantId: PropTypes.string.isRequired
}

UpdateMealType.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId, mealTypeId } = query
    return { restaurantId, mealTypeId }
}
