import { useEffect, useState } from 'react'
import { Button, Divider, Grid, Typography } from '@mui/material'
import Stack from '@mui/material/Stack'
import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import BottomNavigation from '@mui/material/BottomNavigation'
import BottomNavigationAction from '@mui/material/BottomNavigationAction'
import InfoIcon from '@mui/icons-material/Info'
import ArticleIcon from '@mui/icons-material/Article'
import FavoriteIcon from '@mui/icons-material/Favorite'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'

import { clearUserSession, getUserSession } from '@/utils/session'
import { getUserInfo, deleteToken, getUserGlobalScores } from '@/services/user'
import { History } from '../user/History'
import ListRestaurants from './restaurant/ListRestaurants'
import GlobalScores from '@/components/GlobalScores'
import { Recommendations } from '../user/Recommendations'

const styles = {
    profileHeaderContainer: {
        flexWrap: 'wrap'
    },
    userInfoContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        gap: '1vw',
        flexWrap: 'wrap'
    },
    accountIcon: {
        fontSize: '80px'
    },
    userEmail: { fontStyle: 'italic' },
    logoutButton: {
        backgroundColor: 'black',
        ':hover': {
            fontWeight: 'bold',
            backgroundColor: '#E00000',
            transform: 'scale(1.1)'
        },
        '&.MuiButtonBase-root': {
            height: 'min-content',
            margin: 0,
            borderRadius: '10px'
        },
        '&.MuiButtonBase-root p': {
            margin: 0
        }
    },
    profileNavigationActionsContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'flex-start',
        flexWrap: 'wrap',
        flexDirection: { xs: 'row', sm: 'column', md: 'column' },
        marginTop: '20px'
    },
    profileNavigationButton: {
        color: 'black',
        '&.Mui-selected': {
            color: '#FF0000'
        }
    },
    boldText: {
        fontWeight: 'bold'
    }
}

export default function AdminProfile() {
    const router = useRouter()
    const [state, setState] = useState({
        name: '',
        email: '',
        country: '',
        city: '',
        birthDate: ''
    })
    const [currentOption, setCurrentOption] = useState({
        label: 'information'
    })

    const handleOptionChange = (event, newValue) => {
        event.preventDefault()
        setCurrentOption((prevSt) => ({ ...prevSt, label: newValue }))
    }

    useEffect(() => {
        async function fetchData() {
            const session = getUserSession()
            const { jwt, uid } = session
            const { data, isSuccess } = await getUserInfo(jwt, uid)

            if (data && isSuccess) {
                const { name, email, country, city, birthDate } = data
                setState((prevSt) => ({ ...prevSt, name, email, country, city, birthDate }))
            } else {
                console.log('Error fetching user')
            }
        }
        fetchData()
    }, [])

    const handleCloseSesion = async () => {
        try {
            const { uid, jwt } = getUserSession()
            clearUserSession()
            await deleteToken(jwt, uid)
            router.push('/signin')
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <>
            <div>
                <Stack direction="row" justifyContent="space-between" mt={2} mb={2} sx={styles.profileHeaderContainer}>
                    <div style={styles.userInfoContainer}>
                        <AccountCircleIcon sx={styles.accountIcon} />
                        <div>
                            <Typography variant="h4">{state.name} | Admin</Typography>
                            <Typography sx={styles.userEmail}>{state.email}</Typography>
                        </div>
                    </div>
                    <Button variant="contained" onClick={handleCloseSesion} sx={styles.logoutButton}>
                        Log out
                    </Button>
                </Stack>
                <Divider />
                <BottomNavigation
                    sx={styles.profileNavigationActionsContainer}
                    value={currentOption.label}
                    onChange={handleOptionChange}
                >
                    <BottomNavigationAction
                        direction="row"
                        label="Informacion"
                        value="information"
                        sx={styles.profileNavigationButton}
                        icon={<InfoIcon />}
                    />
                    <BottomNavigationAction
                        label="Historial"
                        sx={styles.profileNavigationButton}
                        value="history"
                        icon={<ArticleIcon />}
                    />
                    <BottomNavigationAction
                        label="Recomendaciones"
                        value="recommendations"
                        sx={styles.profileNavigationButton}
                        icon={<FavoriteIcon />}
                    />
                    <BottomNavigationAction
                        label="Mis restaurantes"
                        value="myRestaurants"
                        sx={styles.profileNavigationButton}
                        icon={<MenuBookIcon />}
                    />
                </BottomNavigation>
                <CurrentComponent currentOption={currentOption.label} />
            </div>
        </>
    )
}

function CurrentComponent({ currentOption }) {
    useEffect(() => {}, [currentOption])

    switch (currentOption) {
        case 'information': {
            const session = getUserSession()
            return (
                <Grid container direction="row" justifyContent="flex-start">
                    <div>
                        <Typography mb={5} mt={5} variant="h5" sx={styles.boldText}>
                            Rendimiento
                        </Typography>
                        <GlobalScores getGlobalScores={() => getUserGlobalScores(session.jwt, session.uid)} />
                    </div>
                </Grid>
            )
        }
        case 'history': {
            return (
                <Grid container direction="row" justifyContent="center">
                    <History />
                </Grid>
            )
        }
        case 'recommendations': {
            return (
                <Grid container direction="row" justifyContent="center">
                    <Recommendations />
                </Grid>
            )
        }
        case 'myRestaurants': {
            return (
                <Grid container direction="row" justifyContent="center">
                    <ListRestaurants />
                </Grid>
            )
        }
        default: {
            return (
                <Grid container direction="row" justifyContent="center">
                    <div>
                        <h1>Coming soon...</h1>
                    </div>
                </Grid>
            )
        }
    }
}

CurrentComponent.propTypes = {
    currentOption: PropTypes.string
}
