import { useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { Tabs, Tab, Box, Container, Typography, Button, DialogContent, Popover } from '@mui/material'
import { InfoOutlined } from '@mui/icons-material'
import BasicTable from '@/components/BasicTable'
import AddIcon from '@mui/icons-material/Add'
import { getMenusAndItemsByRestaurantId } from '@/services/menu'
import Modal from '@/components/Modal'
import GlobalScores from '@/components/GlobalScores'
import { getMealtypeGlobalScores } from '@/services/mealType'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import { getImgUrl } from '@/services/image'
import { styled } from '@mui/material/styles'
import VisibilityIcon from '@mui/icons-material/Visibility'

const style = {
    title: {
        textAlign: 'center'
    },
    box: {
        flexGrow: 1,
        display: 'flex',
        p: 5,
        borderColor: '#d8d8d8'
    },
    tabs: {
        borderRight: 1,
        borderColor: 'divider'
    },
    boxItem: {},
    button: {
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    buttonAdd: {
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00BA77',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    buttonEdit: {
        ':hover': {
            fontWeight: 'bold',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    buttonDelete: {
        bgcolor: '#D63535',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    buttonPreview: {
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#000000',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    divItem: {
        width: '100%',
        marginLeft: '10px'
    },
    stackOptions: {
        display: 'flex',
        justifyContent: 'center',
        my: 2
    },
    containerTabs: {
        display: 'flex',
        flexDirection: 'column',
        width: 'auto'
    },
    containerTabPanel: {
        display: 'flex',
        flexDirection: 'row-reverse'
    },
    pointerCursor: {
        ':hover': {
            cursor: 'pointer'
        }
    }
}

const Img = styled('img')({
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%'
})

export default function ListItems({ restaurantId }) {
    const [state, setState] = useState({
        mealTypes: [],
        items: {},
        menuTitle: '',
        value: 0,
        openModal: false,
        anchorEl: null,
        open: false
    })

    const route = useRouter()

    useEffect(() => {
        const fetchData = async () => {
            const { menu, items } = await getMenusAndItemsByRestaurantId(restaurantId)
            const menuTitle = menu.title
            const mealTypes = menu.meal_types
            setState((prvst) => ({ ...prvst, mealTypes, items, menuTitle }))
        }
        fetchData()
    }, [restaurantId])

    const handleChange = (event, newValue) => {
        setState((prvst) => ({ ...prvst, value: newValue }))
    }

    function handleShowScoresInfo(evt) {
        evt.preventDefault()
        setState((prevSt) => ({ ...prevSt, openModal: true }))
    }

    function handleCloseScoresInfo(evt) {
        evt.preventDefault()
        setState((prvst) => ({ ...prvst, openModal: false }))
    }

    const currentMealtypeTitle = state.mealTypes.length > 0 ? state.mealTypes[state.value].title : null

    const handleClick = (event) => {
        setState((prvst) => ({ ...prvst, anchorEl: event.currentTarget }))
    }

    const handleClose = () => {
        setState((prvst) => ({ ...prvst, anchorEl: null }))
    }

    const goToRoute = (url) => {
        route.push(url)
    }

    return (
        <>
            <Container fixed>
                <Typography variant="h4" sx={style.title}>
                    Lista de items del menú {state.menuTitle}
                </Typography>
                <Box sx={style.box} borderRadius={5} border={2}>
                    <Container sx={style.containerTabs}>
                        <Button
                            sx={style.buttonAdd}
                            variant="contained"
                            onClick={() => goToRoute(`/admin/restaurants/${restaurantId}/menu/addMealType`)}
                        >
                            <AddIcon />
                        </Button>
                        <Tabs
                            orientation="vertical"
                            variant="scrollable"
                            value={state.value}
                            onChange={handleChange}
                            aria-label="Vertical tabs example"
                            sx={style.tabs}
                            indicatorColor="primary"
                        >
                            {state.mealTypes.map((mealType, index) => {
                                return <Tab key={index} label={mealType.title} {...a11yProps(index)} />
                            })}
                        </Tabs>
                    </Container>
                    {state.mealTypes.map((mealType, index) => {
                        const { fields, data } = InfoTable(state.items[`${mealType.id}`], restaurantId, goToRoute)
                        return (
                            <TabPanel key={index} value={state.value} index={index}>
                                <Container sx={style.containerTabPanel}>
                                    <Button
                                        aria-describedby="simple-popover"
                                        sx={style.button}
                                        variant="contained"
                                        onClick={handleClick}
                                    >
                                        <MoreVertIcon />
                                    </Button>
                                </Container>
                                <Popover
                                    id="simple-popover"
                                    open={Boolean(state.anchorEl)}
                                    anchorEl={state.anchorEl}
                                    onClose={handleClose}
                                    anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'center'
                                    }}
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right'
                                    }}
                                >
                                    <Button
                                        sx={style.buttonAdd}
                                        variant="contained"
                                        onClick={() => goToRoute(`/admin/restaurants/${restaurantId}/menu/addItem`)}
                                    >
                                        <AddIcon /> Item
                                    </Button>
                                    <Button
                                        sx={style.buttonEdit}
                                        variant="contained"
                                        onClick={() =>
                                            goToRoute(`/admin/restaurants/${restaurantId}/menu/${mealType.id}/update`)
                                        }
                                    >
                                        <EditIcon /> {mealType.title}
                                    </Button>
                                    <Button
                                        sx={style.buttonDelete}
                                        variant="contained"
                                        onClick={() =>
                                            goToRoute(`/admin/restaurants/${restaurantId}/menu/${mealType.id}/delete`)
                                        }
                                    >
                                        <DeleteIcon /> {mealType.title}
                                    </Button>
                                </Popover>
                                {data.length > 0 ? (
                                    <BasicTable fields={fields} rows={data} />
                                ) : (
                                    <Typography>No hay items asociados</Typography>
                                )}
                            </TabPanel>
                        )
                    })}
                    <InfoOutlined sx={style.pointerCursor} onClick={handleShowScoresInfo} />
                </Box>
            </Container>
            <Modal
                open={state.openModal}
                handleCloseModal={handleCloseScoresInfo}
                title={`${currentMealtypeTitle} | Scores`}
                maxWidth="sm"
            >
                <DialogContent dividers={true}>
                    <GlobalScores getGlobalScores={() => getMealtypeGlobalScores(state.mealTypes[state.value].id)} />
                </DialogContent>
            </Modal>
        </>
    )
}

function InfoTable(itemsInfo, restaurantId, goToRoute) {
    const data = itemsInfo.map((item) => {
        const { title, price, image_id, meal_type_id } = item
        const imageUrl = getImgUrl(image_id, 100, 100)
        return {
            title,
            price,
            image: (
                <>
                    <Img alt="Meal image" src={imageUrl} />
                </>
            ),
            actions: (
                <>
                    <Button
                        sx={style.buttonEdit}
                        variant="contained"
                        onClick={() =>
                            goToRoute(`/admin/restaurants/${restaurantId}/menu/${item.meal_type_id}/${item.id}/update`)
                        }
                    >
                        {' '}
                        <EditIcon />{' '}
                    </Button>
                    <Button
                        sx={style.buttonDelete}
                        variant="contained"
                        onClick={() =>
                            goToRoute(`/admin/restaurants/${restaurantId}/menu/${item.meal_type_id}/${item.id}/delete`)
                        }
                    >
                        {' '}
                        <DeleteIcon />{' '}
                    </Button>
                    <Button
                        sx={style.buttonPreview}
                        variant="contained"
                        onClick={() =>
                            goToRoute(
                                `/restaurant/${restaurantId}/menu?defaultMealTypeId=${meal_type_id}&defaultItemName=${item.title}`
                            )
                        }
                    >
                        {' '}
                        <VisibilityIcon />{' '}
                    </Button>
                </>
            )
        }
    })
    const fields = [
        {
            text: 'Imágen',
            value: 'image'
        },
        {
            text: 'Título',
            value: 'title'
        },
        {
            text: 'Precio',
            value: 'price'
        },
        {
            text: 'Acción',
            value: 'actions'
        }
    ]
    return { fields, data }
}

function TabPanel(props) {
    const { children, value, index, ...other } = props
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            style={style.divItem}
            {...other}
        >
            {value === index && <Box sx={style.boxItem}>{children}</Box>}
        </div>
    )
}

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`
    }
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired
}

ListItems.propTypes = {
    restaurantId: PropTypes.string.isRequired
}
