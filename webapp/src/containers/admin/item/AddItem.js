import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
    Container,
    Stack,
    Avatar,
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Typography,
    IconButton,
    TextField,
    Alert
} from '@mui/material'
import AddPhotoAlternateIcon from '@mui/icons-material/AddAPhoto'
import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu'
import { TextFieldError } from '@/components/TextFieldError'
import { itemValidator } from '@/utils/validators'
import { mkPostItemPayload, postItem, postItemImage } from '@/services/item'
import { getMenusAndItemsByRestaurantId } from '@/services/menu'
import { getUserSession } from '@/utils/session'
import { useRouter } from 'next/router'

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center'
    },
    stack: {
        width: { xs: '100%', sm: '300px', md: '400px' },
        display: 'flex',
        alignItems: 'center'
    },
    avatar: {
        m: 1,
        bgcolor: '#FF6161'
    },
    inputLabelSelect: {
        mt: 1
    },
    menuButton: {
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    submitButton: {
        my: 2,
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00BA77',
            transform: 'scale(1.1)'
        }
    }
}

export default function AddItem({ restaurantId }) {
    const router = useRouter()
    const [state, setState] = useState({
        menuTitle: '',
        mealTypes: [],
        meal_type_id: '',
        title: '',
        description: '',
        price: 0,
        validator: null,
        itemImage: null,
        submitDisabled: true,
        showSucessAlert: false
    })
    useEffect(() => {
        const fetchData = async () => {
            const { menu } = await getMenusAndItemsByRestaurantId(restaurantId)
            const menuTitle = menu.title
            const mealTypes = menu.meal_types
            setState((prvst) => ({ ...prvst, mealTypes, menuTitle }))
        }

        fetchData()
    }, [restaurantId])

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }
                newState.validator = itemValidator(newState)
                if (newState.validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }
            setState(updateState)
        }
    }

    const handleOnCloseAlert = (attr) => {
        return () => {
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: false }
                return newState
            }
            setState(updateState)
        }
    }

    const handleUploadImage = (attr) => {
        return async (evt) => {
            const updateState = (prevSt) => {
                let file = evt.target.files[0]
                if (!file) {
                    return prevSt
                }
                let newState = { ...prevSt, [attr]: evt.target.files[0] }
                newState.validator = itemValidator(newState)
                if (newState.validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }
            setState(updateState)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        if (state.validator && state.validator.allValid) {
            const itemData = mkPostItemPayload(state.title, state.description, state.price)

            const { uid, jwt, isAdmin } = getUserSession()

            if (!isAdmin) {
                router.push('/profile')
            }

            const result = await postItem(jwt, itemData, uid, state.meal_type_id)
            const item = result.data.jwt

            await postItemImage(jwt, uid, item.id, state.itemImage)

            if (result.isSuccess) {
                console.log('Sucess')
                const resetFields = {
                    meal_type_id: '',
                    title: '',
                    description: '',
                    price: 0,
                    itemImage: null,
                    submitDisabled: true
                }
                document.getElementById('create-item-form').reset()
                setState((prevSt) => ({ ...prevSt, ...resetFields, showSucessAlert: true }))
            } else {
                setState((prevSt) => ({ ...prevSt, showSucessAlert: false }))
                console.error(result.error.description)
            }
        } else {
            console.log('Register attempt with invalid fields')
        }
    }

    const onClickMenu = () => {
        router.push(`/restaurant/${restaurantId}/menu`)
    }

    return (
        <Container sx={styles.container}>
            <form onSubmit={handleSubmit} id="create-item-form">
                <Stack direction="column" sx={styles.stack} spacing={1}>
                    <Button sx={styles.menuButton} variant="contained" onClick={onClickMenu}>
                        Ver menú
                    </Button>
                    <Typography variant="h5">
                        Agregar item al menú <b>{state.menuTitle}</b>
                    </Typography>
                    <Avatar sx={styles.avatar}>
                        <RestaurantMenuIcon />
                    </Avatar>
                    {state.showSucessAlert ? <SucessAlert onClose={handleOnCloseAlert} /> : null}
                    <FormControl fullWidth>
                        <InputLabel id="mealTypeLabel">Tipo de comida *</InputLabel>
                        <Select
                            labelId="mealTypeLabel"
                            id="meal_type_id"
                            label="meal_type_id"
                            value={state.meal_type_id}
                            onChange={handleInputChange('meal_type_id')}
                        >
                            {state.mealTypes.map((item) => (
                                <MenuItem key={item.id} value={item.id}>
                                    {item.title}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FieldError validations={state.validator} validationType="mealTypeIdValidation" />
                    <TextField
                        required
                        fullWidth
                        id="title"
                        label="Titulo"
                        name="title"
                        onChange={handleInputChange('title')}
                    />
                    <FieldError validations={state.validator} validationType="titleValidation" />
                    <TextField
                        required
                        fullWidth
                        id="price"
                        label="Price"
                        type="number"
                        name="price"
                        value={state.price}
                        onChange={handleInputChange('price')}
                    />
                    <FieldError validations={state.validator} validationType="priceValidation" />
                    <TextField
                        required
                        fullWidth
                        id="description"
                        label="Description"
                        name="description"
                        multiline
                        onChange={handleInputChange('description')}
                    />
                    <FieldError validations={state.validator} validationType="descriptionValidation" />
                    <Typography>Insertar la imagen del item</Typography>
                    <IconButton color="primary" aria-label="upload picture" component="label">
                        <input hidden accept="image/*" type="file" onChange={handleUploadImage('itemImage')} />
                        <AddPhotoAlternateIcon />
                    </IconButton>
                    {state.itemImage ? state.itemImage.name : null}
                    <FieldError validations={state.validator} validationType="itemImageValidation" />
                </Stack>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={state.submitDisabled}
                    sx={styles.submitButton}
                >
                    Añadir Item
                </Button>
            </form>
        </Container>
    )
}

const SucessAlert = ({ onClose }) => (
    <Alert severity="success" onClose={onClose('showSucessAlert')}>
        El item se añadió correctamente!
    </Alert>
)

function FieldError({ validations, validationType }) {
    const fieldError =
        !validations || validations[validationType].isValid ? null : (
            <TextFieldError message={validations[validationType].message} />
        )
    return fieldError
}

SucessAlert.propTypes = {
    onClose: PropTypes.func.isRequired
}

AddItem.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

FieldError.propTypes = {
    validationType: PropTypes.string.isRequired,
    validations: PropTypes.object
}
