import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
    Container,
    Stack,
    Avatar,
    Button,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Typography,
    IconButton,
    TextField,
    Alert
} from '@mui/material'
import AddPhotoAlternateIcon from '@mui/icons-material/AddAPhoto'
import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu'
import { TextFieldError } from '@/components/TextFieldError'
import { updateItemValidator } from '@/utils/validators'
import { getMenusAndItemsByRestaurantId } from '@/services/menu'
import { mkPutItemPayload, putItem, postItemImage } from '@/services/item'
import { getUserSession } from '@/utils/session'
import { useRouter } from 'next/router'

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center'
    },
    stack: {
        width: { xs: '100%', sm: '300px', md: '400px' },
        display: 'flex',
        alignItems: 'center'
    },
    avatar: {
        m: 1,
        bgcolor: '#FF6161'
    },
    inputLabelSelect: {
        mt: 1
    },
    menuButton: {
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    submitButton: {
        my: 2,
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00BA77',
            transform: 'scale(1.1)'
        }
    }
}

export default function UpdateItem({ restaurantId, mealTypeId, itemId }) {
    const router = useRouter()
    const [state, setState] = useState({
        menuTitle: '',
        mealTypes: [],
        meal_type_id: '',
        title: '',
        description: '',
        price: '',
        validator: null,
        itemImage: null,
        submitDisabled: false,
        showSucessAlert: false
    })

    useEffect(() => {
        const fetchData = async () => {
            const { menu, items } = await getMenusAndItemsByRestaurantId(restaurantId)
            const menuTitle = menu.title
            const mealTypes = menu.meal_types
            const { title, description, price, meal_type_id } = items[`${mealTypeId}`].filter(
                (item) => item.id === itemId
            )[0]
            setState((prvst) => ({
                ...prvst,
                mealTypes,
                menuTitle,
                title,
                description,
                price,
                meal_type_id
            }))
        }
        fetchData()
    }, [restaurantId, mealTypeId, itemId])

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }
                newState.validator = updateItemValidator(newState)
                if (newState.validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }
            setState(updateState)
        }
    }

    const handleOnCloseAlert = (attr) => {
        return () => {
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: false }
                return newState
            }
            setState(updateState)
        }
    }

    const handleUploadImage = (attr) => {
        return async (evt) => {
            const updateState = (prevSt) => {
                let file = evt.target.files[0]
                if (!file) {
                    return prevSt
                }
                let newState = { ...prevSt, [attr]: evt.target.files[0] }
                newState.validator = updateItemValidator(newState)
                if (newState.validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }
                return newState
            }
            setState(updateState)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        if (state.validator && state.validator.allValid) {
            const itemData = mkPutItemPayload(state.title, state.description, state.price)

            const { uid, jwt, isAdmin } = getUserSession()

            if (!isAdmin) {
                router.push('/profile')
            }

            const result = await putItem(jwt, itemData, uid, itemId, state.meal_type_id)

            if (state.itemImage) {
                await postItemImage(jwt, uid, itemId, state.itemImage)
            }

            if (result.isSuccess) {
                console.log('Sucess')
                setState((prevSt) => ({ ...prevSt, showSucessAlert: true, itemImage: null }))
            } else {
                setState((prevSt) => ({ ...prevSt, showSucessAlert: false, itemImage: null }))
                console.error(result.error.description)
            }
        } else {
            console.log('Register attempt with invalid fields')
        }
    }

    const onClickMenu = () => {
        router.push(`/restaurant/${restaurantId}/menu?defaultMealTypeId=${mealTypeId}&defaultItemName=${state.title}`)
    }

    return (
        <Container sx={styles.container}>
            <form onSubmit={handleSubmit} id="create-item-form">
                <Stack direction="column" sx={styles.stack} spacing={1}>
                    <Button sx={styles.menuButton} variant="contained" onClick={onClickMenu}>
                        Ver en menú
                    </Button>
                    <Typography variant="h5">
                        Editar item del menú <b>{state.menuTitle}</b>
                    </Typography>
                    <Avatar sx={styles.avatar}>
                        <RestaurantMenuIcon />
                    </Avatar>
                    {state.showSucessAlert ? <SucessAlert onClose={handleOnCloseAlert} /> : null}
                    <FormControl fullWidth>
                        <InputLabel id="mealTypeLabel">Tipo de comida *</InputLabel>
                        <Select
                            labelId="mealTypeLabel"
                            id="meal_type_id"
                            label="meal_type_id"
                            value={state.meal_type_id}
                            onChange={handleInputChange('meal_type_id')}
                        >
                            {state.mealTypes.map((item) => (
                                <MenuItem key={item.id} value={item.id}>
                                    {item.title}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FieldError validations={state.validator} validationType="mealTypeIdValidation" />
                    <TextField
                        required
                        fullWidth
                        id="title"
                        label="Titulo"
                        name="title"
                        onChange={handleInputChange('title')}
                        value={state.title}
                    />
                    <FieldError validations={state.validator} validationType="titleValidation" />
                    <TextField
                        required
                        fullWidth
                        id="price"
                        label="Price"
                        type="number"
                        name="price"
                        value={state.price}
                        onChange={handleInputChange('price')}
                    />
                    <FieldError validations={state.validator} validationType="priceValidation" />
                    <TextField
                        required
                        fullWidth
                        id="description"
                        label="Description"
                        name="description"
                        multiline
                        value={state.description}
                        onChange={handleInputChange('description')}
                    />
                    <FieldError validations={state.validator} validationType="descriptionValidation" />
                    <Typography>Actualizar la imagen del item</Typography>
                    <IconButton color="primary" aria-label="upload picture" component="label">
                        <input hidden accept="image/*" type="file" onChange={handleUploadImage('itemImage')} />
                        <AddPhotoAlternateIcon />
                    </IconButton>
                    {state.itemImage ? state.itemImage.name : null}
                    <FieldError validations={state.validator} validationType="itemImageValidation" />
                </Stack>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={state.submitDisabled}
                    sx={styles.submitButton}
                >
                    Editar Item
                </Button>
            </form>
        </Container>
    )
}

const SucessAlert = ({ onClose }) => (
    <Alert severity="success" onClose={onClose('showSucessAlert')}>
        El item se editó correctamente!
    </Alert>
)

UpdateItem.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId, itemId, mealTypeId } = query

    return { restaurantId, itemId, mealTypeId }
}

function FieldError({ validations, validationType }) {
    const fieldError =
        !validations || validations[validationType].isValid ? null : (
            <TextFieldError message={validations[validationType].message} />
        )
    return fieldError
}

SucessAlert.propTypes = {
    onClose: PropTypes.func.isRequired
}

UpdateItem.propTypes = {
    restaurantId: PropTypes.string.isRequired,
    mealTypeId: PropTypes.string.isRequired,
    itemId: PropTypes.string.isRequired
}

FieldError.propTypes = {
    validationType: PropTypes.string.isRequired,
    validations: PropTypes.object
}
