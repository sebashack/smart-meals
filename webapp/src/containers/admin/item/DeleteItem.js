import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { getUserSession } from '@/utils/session'
import { deleteItem } from '@/services/item'

export default function DeleteItem({ itemId }) {
    const router = useRouter()
    useEffect(() => {
        async function fetchPromise() {
            const { uid, jwt, isAdmin } = getUserSession()

            if (isAdmin) {
                await deleteItem(uid, jwt, itemId)
            }

            router.back()
        }

        fetchPromise()
    }, [itemId, router])

    return (
        <>
            <h1>Deleting...</h1>
        </>
    )
}

DeleteItem.propTypes = {
    itemId: PropTypes.string.isRequired
}
