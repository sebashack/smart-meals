import { useState } from 'react'
import { useRouter } from 'next/router'
import { Container, Stack, Avatar, TextField, FormControl, InputLabel, Select, Divider, Button } from '@mui/material'
import PropTypes from 'prop-types'
import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu'
import { mkMenuData, postMenu } from '@/services/menu'
import { getUserSession } from '@/utils/session'

const styles = {
    container: {
        display: 'flex',
        justifyContent: 'center'
    },
    stack: {
        width: { xs: '100%', sm: '300px', md: '400px' },
        display: 'flex',
        alignItems: 'center'
    },
    avatar: {
        m: 1,
        bgcolor: '#FF6161'
    },
    inputLabelSelect: {
        mt: 1
    }
}

export default function RegisterMenu({ restaurantId }) {
    const router = useRouter()
    const [state, setState] = useState({
        id: '',
        title: '',
        mealTypes: {},
        numMealTypes: 1,
        submitDisabled: true
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()

            const v = evt.target.value
            const value = attr === 'numMealTypes' ? parseInt(v) : v
            const submitDisabled = typeof value === 'number'

            setState((prevSt) => ({ ...prevSt, submitDisabled, [attr]: value }))
        }
    }

    const handleMealFieldChange = (fieldId) => {
        return (evt) => {
            evt.preventDefault()
            const value = evt.target.value
            const updateState = (prevSt) => {
                let newMealTypes = {}
                const prevMealTypes = prevSt.mealTypes

                if (prevSt.mealTypes && value === '') {
                    const keys = Object.keys(prevMealTypes)
                    keys.map((k) => {
                        if (k != fieldId) {
                            newMealTypes[k] = prevMealTypes[k]
                        }
                    })
                } else {
                    newMealTypes = { ...prevMealTypes, [fieldId]: value }
                }

                const submitDisabled = !(prevSt.numMealTypes === Object.keys(newMealTypes).length)

                return { ...prevSt, submitDisabled, mealTypes: newMealTypes }
            }

            setState(updateState)
        }
    }

    const handleSubmit = async (evt) => {
        evt.preventDefault()
        const mealTypes = Object.values(state.mealTypes)
        const { jwt, uid } = getUserSession()
        const { title } = state
        const menuData = mkMenuData(title, mealTypes)

        const result = await postMenu(jwt, uid, restaurantId, menuData)

        if (result.isSuccess) {
            console.log('Sucess')

            router.push(`/admin/restaurants/${restaurantId}/menu/items`)
        } else {
            console.error(result.error.description)
        }
    }

    const fieldIds = Array.from(Array(state.numMealTypes).keys())
    const mealTypeFields = fieldIds.map((i) => (
        <MealTypeField key={i + 1} fieldId={i + 1} onChange={handleMealFieldChange} />
    ))
    return (
        <Container sx={styles.container}>
            <form onSubmit={handleSubmit}>
                <Stack direction="column" sx={styles.stack} spacing={1}>
                    <Avatar sx={styles.avatar}>
                        <RestaurantMenuIcon />
                    </Avatar>
                    <TextField
                        required
                        fullWidth
                        id="title"
                        label="Titulo"
                        name="title"
                        onChange={handleInputChange('title')}
                    />
                    <FormControl fullWidth>
                        <InputLabel htmlFor="age-native-simple" sx={styles.inputLabelSelect}>
                            Numero de categorias
                        </InputLabel>
                        <Select native value={state.numMealTypes} onChange={handleInputChange('numMealTypes')}>
                            <MealOptions numOpts={10} />
                        </Select>
                    </FormControl>
                    <Divider />
                    {mealTypeFields}
                    <Button type="submit" fullWidth variant="contained" color="primary" disabled={state.submitDisabled}>
                        Añadir Menu
                    </Button>
                </Stack>
            </form>
        </Container>
    )
}

function MealTypeField({ fieldId, onChange }) {
    return (
        <TextField
            name={'mealType-' + fieldId}
            variant="outlined"
            required
            fullWidth
            id={fieldId.toString()}
            label={`Tipo de comida #${fieldId}`}
            onChange={onChange(fieldId)}
        />
    )
}

function MealOptions({ numOpts }) {
    const optsIds = Array.from(Array(numOpts).keys())
    return optsIds.map((i) => (
        <option key={i + 1} value={i + 1}>
            {i + 1}
        </option>
    ))
}

RegisterMenu.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query

    return { restaurantId }
}

RegisterMenu.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

MealTypeField.propTypes = {
    fieldId: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
}

MealOptions.propTypes = {
    restaurants: PropTypes.arrayOf(PropTypes.number)
}
