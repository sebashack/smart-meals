import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { getUserSession } from '@/utils/session'
import { deleteRestaurant } from '@/services/restaurant'

export default function DeleteRestaurant({ restaurantId }) {
    const router = useRouter()
    useEffect(() => {
        async function fetchPromise() {
            const { uid, jwt, isAdmin } = getUserSession()

            if (isAdmin) {
                await deleteRestaurant(uid, jwt, restaurantId)
            }

            router.back()
        }

        fetchPromise()
    }, [restaurantId, router])

    return (
        <>
            <h1>Deleting...</h1>
        </>
    )
}

DeleteRestaurant.propTypes = {
    restaurantId: PropTypes.string.isRequired
}
