import React, { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { Stack, Button } from '@mui/material'
import { getUserSession } from '@/utils/session'
import { getRestaurants } from '@/services/restaurant'
import BasicTable from '@/components/BasicTable'
import AddIcon from '@mui/icons-material/Add'
import EditIcon from '@mui/icons-material/Edit'
import DeleteIcon from '@mui/icons-material/Delete'
import VisibilityIcon from '@mui/icons-material/Visibility'

const style = {
    stackOptions: {
        justifyContent: 'center',
        m: 3
    },
    buttonAdd: {
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00BA77',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    buttonEdit: {
        ':hover': {
            fontWeight: 'bold',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    buttonDelete: {
        bgcolor: '#D63535',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        },
        m: 2
    },
    buttonPreview: {
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#000000',
            transform: 'scale(1.1)'
        },
        m: 2
    }
}

export default function ListRestaurants() {
    const [state, setState] = useState({
        restaurants: []
    })

    const route = useRouter()

    useEffect(() => {
        async function fetchData() {
            const { isAdmin, jwt, uid } = getUserSession()
            if (isAdmin) {
                const restaurants = await getRestaurants(jwt, uid)
                setState({ restaurants })
            }
        }
        fetchData()
    }, [])

    const goToRoute = (url) => {
        route.push(url)
    }

    const { fields, data } = InfoTable(state.restaurants, goToRoute)

    return (
        <>
            <Button variant="contained" sx={style.buttonAdd} onClick={() => goToRoute(`/admin/restaurants/register`)}>
                <AddIcon /> restaurante
            </Button>
            {<BasicTable fields={fields} rows={data} />}
        </>
    )
}

function InfoTable(restaurants, goToRoute) {
    const fields = [
        {
            text: 'Nombre',
            value: 'name'
        },
        {
            text: 'Email',
            value: 'email'
        },
        {
            text: 'Dirección',
            value: 'address'
        },
        {
            text: 'País',
            value: 'country'
        },
        {
            text: 'Ciudad',
            value: 'city'
        },
        {
            text: 'Número telefónico',
            value: 'phone_number'
        },
        {
            text: 'Acciones',
            value: 'actions'
        }
    ]
    const data = restaurants.map((restaurant) => {
        const { id, name, email, address, country, city, phone_number } = restaurant
        return {
            name,
            email,
            address,
            country,
            city,
            phone_number,
            actions: (
                <Stack direction="row">
                    <Button variant="contained" sx={style.buttonPreview} onClick={() => goToRoute(`/restaurant/${id}`)}>
                        <VisibilityIcon />{' '}
                    </Button>
                    <Button
                        variant="contained"
                        sx={style.buttonEdit}
                        onClick={() => goToRoute(`/admin/restaurants/${id}/update`)}
                    >
                        <EditIcon />
                    </Button>
                    <Button
                        variant="contained"
                        sx={style.buttonDelete}
                        onClick={() => goToRoute(`/admin/restaurants/${id}/delete`)}
                    >
                        <DeleteIcon />
                    </Button>
                </Stack>
            )
        }
    })
    return {
        fields,
        data
    }
}
