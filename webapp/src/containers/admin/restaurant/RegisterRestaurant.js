import React, { useState } from 'react'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import TextField from '@mui/material/TextField'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import MenuBookIcon from '@mui/icons-material/MenuBook'
import Typography from '@mui/material/Typography'
import Container from '@mui/material/Container'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import IconButton from '@mui/material/IconButton'
import AddPhotoAlternateIcon from '@mui/icons-material/AddAPhoto'
import {
    postRestaurant,
    mkPostRestaurantPayload,
    postRestaurantBackgroundImage,
    postRestaurantLogoImage
} from '@/services/restaurant'
import { getUserSession } from '@/utils/session'
import { restaurantValidator } from '@/utils/validators'
import { TextFieldError } from '@/components/TextFieldError'

const style = {
    boxContainer: {
        marginTop: 2,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    iconForm: {
        m: 1,
        bgcolor: '#FF6161'
    },
    boxContainerForm: {
        mt: 3
    },
    submitButton: {
        mt: 3,
        mb: 2,
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00BA77',
            transform: 'scale(1.1)'
        }
    }
}

export default function RegisterRestaurant() {
    const router = useRouter()
    const [state, setState] = useState({
        name: '',
        email: '',
        city: '',
        country: '',
        address: '',
        phone_number: '',
        description: '',
        validator: null,
        backgroundImage: null,
        logoImage: null,
        submitDisabled: true
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }
                newState.validator = restaurantValidator(newState)

                if (newState.validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }
            setState(updateState)
        }
    }

    const handleUploadImage = (attr) => {
        return async (evt) => {
            const updateState = (prevSt) => {
                let file = evt.target.files[0]

                if (!file) {
                    return prevSt
                }

                let newState = { ...prevSt, [attr]: evt.target.files[0] }
                newState.validator = restaurantValidator(newState)
                if (newState.validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }

                return newState
            }
            setState(updateState)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        if (state.validator && state.validator.allValid) {
            const restaurantData = mkPostRestaurantPayload(
                state.name,
                state.email,
                state.city,
                state.country,
                state.address,
                state.phone_number,
                state.description
            )

            const { uid, jwt, isAdmin } = getUserSession()

            if (!isAdmin) {
                router.push('/profile')
            }

            const result = await postRestaurant(jwt, restaurantData, uid)
            const restaurant = result.data.jwt

            await postRestaurantBackgroundImage(jwt, uid, restaurant.id, state.backgroundImage)
            await postRestaurantLogoImage(jwt, uid, restaurant.id, state.logoImage)

            if (result.isSuccess) {
                console.log('Sucess')
                router.push('/admin/restaurants')
            } else {
                if (result.error.isDuplicatedEmail) {
                    setState((prevSt) => ({ ...prevSt }))
                } else {
                    console.error(result.error.description)
                }
            }
        } else {
            console.log('Register attempt with invalid fields')
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box sx={style.boxContainer}>
                <Avatar sx={style.iconForm}>
                    <MenuBookIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Registrar restaurante
                </Typography>
                <Box component="form" noValidate onSubmit={handleSubmit} sx={style.boxContainerForm}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                autoComplete="given-name"
                                name="name"
                                required
                                fullWidth
                                id="name"
                                label="Nombre restaurante"
                                autoFocus
                                onChange={handleInputChange('name')}
                            />
                            <FieldError validations={state.validator} validationType="nameValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                id="email"
                                label="Correo electrónico"
                                name="email"
                                autoComplete="email"
                                onChange={handleInputChange('email')}
                            />
                            <FieldError validations={state.validator} validationType="emailValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="country"
                                variant="outlined"
                                required
                                fullWidth
                                id="country"
                                label="País"
                                onChange={handleInputChange('country')}
                            />
                            <FieldError validations={state.validator} validationType="countryValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="city"
                                variant="outlined"
                                required
                                fullWidth
                                id="city"
                                label="Ciudad"
                                onChange={handleInputChange('city')}
                            />
                            <FieldError validations={state.validator} validationType="cityValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="address"
                                variant="outlined"
                                required
                                fullWidth
                                id="address"
                                label="Dirección"
                                onChange={handleInputChange('address')}
                            />
                            <FieldError validations={state.validator} validationType="addressValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="phone_number"
                                variant="outlined"
                                required
                                fullWidth
                                id="phone_number"
                                label="Número teléfonico"
                                onChange={handleInputChange('phone_number')}
                            />
                            <FieldError validations={state.validator} validationType="phoneNumberValidation" />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="description"
                                variant="outlined"
                                required
                                fullWidth
                                id="description"
                                label="Descripcion"
                                multiline
                                onChange={handleInputChange('description')}
                            />
                            <FieldError validations={state.validator} validationType="descriptionValidation" />
                        </Grid>
                        <Grid item xs={12} textAlign="center">
                            <Typography>Insertar el logo del restaurante</Typography>
                            <IconButton color="primary" aria-label="upload picture" component="label">
                                <input hidden accept="image/*" type="file" onChange={handleUploadImage('logoImage')} />
                                <AddPhotoAlternateIcon />
                            </IconButton>
                            {state.logoImage ? state.logoImage.name : null}
                            <FieldError validations={state.validator} validationType="logoFileValidation" />
                        </Grid>
                        <Grid item xs={12} textAlign="center">
                            <Typography>Insertar el banner del restaurante</Typography>
                            <IconButton color="primary" aria-label="upload picture" component="label">
                                <input
                                    hidden
                                    accept="image/*"
                                    type="file"
                                    onChange={handleUploadImage('backgroundImage')}
                                />
                                <AddPhotoAlternateIcon />
                            </IconButton>
                            {state.backgroundImage ? state.backgroundImage.name : null}
                            <FieldError validations={state.validator} validationType="backgroundFileValidation" />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={style.submitButton}
                        disabled={state.submitDisabled}
                    >
                        Crear Restaurante
                    </Button>
                </Box>
            </Box>
        </Container>
    )
}

function FieldError({ validations, validationType }) {
    const fieldError =
        !validations || validations[validationType].isValid ? null : (
            <TextFieldError message={validations[validationType].message} />
        )
    return fieldError
}

FieldError.propTypes = {
    validationType: PropTypes.string.isRequired,
    validations: PropTypes.object
}
