import React, { useState, useEffect } from 'react'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import Rating from '@mui/material/Rating'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import PropTypes from 'prop-types'
import Stack from '@mui/material/Stack'
import InfoIcon from '@mui/icons-material/Info'
import Alert from '@mui/material/Alert'
import { getUserSession } from '@/utils/session'
import { mkPostReviewPayload, postReview } from '@/services/review'
import { reviewValidator } from '@/utils/validators'

const styles = {
    primaryButton: {
        mt: 1,
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            color: '#ffffff',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        },
        '&.MuiButtonBase-root': {
            height: 'min-content',
            margin: 0
        },
        '&.MuiButtonBase-root p': {
            margin: 0
        }
    },
    makeReviewButton: {
        mt: 1,
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00EA94',
            color: '#FFFFFF',
            transform: 'scale(1.1)'
        },
        '&.MuiButtonBase-root': {
            height: 'min-content',
            margin: 0
        },
        '&.MuiButtonBase-root p': {
            margin: 0
        }
    },
    formPostReview: {
        width: '100%'
    },
    buttonDelete: {
        m: 1,
        bgcolor: '#D63535',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            transform: 'scale(1.1)'
        }
    },
    buttonPublish: {
        m: 1,
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00BA77',
            transform: 'scale(1.1)'
        }
    }
}

export default function MakeReview({ selectedItem, handleRollBack }) {
    const [state, setState] = useState({
        category: '',
        score: 0,
        currentSelected: '',
        content: '',
        session: {},
        showSuccess: false,
        showFail: false,
        error: null
    })

    useEffect(() => {
        const session = getUserSession()
        setState((prevSt) => ({ ...prevSt, session }))
    }, [])

    const onClickCat = (category) => {
        return (evt) => {
            evt.preventDefault()
            setState((prevSt) => ({ ...prevSt, category, currentSelected: category }))
        }
    }

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()

            setState((prevSt) => ({ ...prevSt, [attr]: evt.target.value }))
        }
    }

    const handleOnCloseAlert = (attr) => {
        return () => {
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: false }
                return newState
            }
            setState(updateState)
        }
    }

    const handleSubmit = async (evt) => {
        evt.preventDefault()
        let session = state.session
        const cnt = state.content.length < 1 ? null : state.content
        let reviewData = mkPostReviewPayload(session.uid, selectedItem.id, state.category, parseFloat(state.score), cnt)
        const validator = reviewValidator(state)

        if (state.currentSelected.length < 1) {
            setState((prevSt) => ({
                ...prevSt,
                error: 'Debes seleccionar una categoría',
                showFail: true,
                showSuccess: false
            }))
            return
        }

        if (!validator.allValid) {
            const { contentValidation } = validator
            setState((prevSt) => ({ ...prevSt, error: contentValidation.message, showFail: true, showSuccess: false }))
            return
        }

        try {
            const response = await postReview(session.jwt, session.uid, selectedItem.id, reviewData)
            console.log('Response :' + response)
            setState((prevSt) => ({ ...prevSt, content: '', score: 0, showSuccess: true, showFail: false }))
        } catch (error) {
            console.log('Error :' + error)
            if (error === 'NotAllowedToReview') {
                setState((prevSt) => ({ ...prevSt, error: 'Has publicado en menos de 24h', showFail: true }))
            }
        }
    }

    const categories = [
        { onClick: onClickCat('Quality'), title: 'Calidad', disabled: state.currentSelected === 'Quality' },
        { onClick: onClickCat('Healthiness'), title: 'Salud', disabled: state.currentSelected === 'Healthiness' },
        {
            onClick: onClickCat('PriceFairness'),
            title: 'Economía',
            disabled: state.currentSelected === 'PriceFairness'
        },
        { onClick: onClickCat('Ecology'), title: 'Ecología', disabled: state.currentSelected === 'Ecology' }
    ]

    return (
        <>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography variant="h6">{state.session.email}</Typography>
                    <Stack direction="row" spacing={1} alignItems="center">
                        <Typography variant="body2">Se mostrará publicamente</Typography>
                        <InfoIcon />
                    </Stack>
                </Grid>
                <Grid item container spacing={2} xs={12} md={12} sm={12}>
                    <ReviewFilters categories={categories} />
                </Grid>
                <Grid item xs={12}>
                    <Rating
                        size="large"
                        name="score"
                        defaultValue={2.5}
                        precision={0.5}
                        value={parseFloat(state.score)}
                        onChange={handleInputChange('score')}
                    />
                </Grid>
                <Grid item container xs={12} md={12} sm={12}>
                    <Stack sx={styles.formPostReview}>
                        <form onSubmit={handleSubmit}>
                            <Grid item xs={12} md={12} sm={12}>
                                <TextField
                                    id="content"
                                    label="Reseña"
                                    multiline
                                    rows={4}
                                    fullWidth
                                    placeholder="Inserte aquí su opinión"
                                    variant="outlined"
                                    value={state.content}
                                    onChange={handleInputChange('content')}
                                />
                            </Grid>
                            {state.showSuccess ? <SucessAlert onClose={handleOnCloseAlert} /> : null}
                            {state.showFail ? <FailAlert onClose={handleOnCloseAlert} message={state.error} /> : null}
                            <br />
                            <Grid item align="right" xs={12}>
                                <Button
                                    onClick={handleRollBack}
                                    size="small"
                                    variant="contained"
                                    sx={styles.buttonDelete}
                                >
                                    Cancelar
                                </Button>
                                <Button
                                    type="submit"
                                    size="small"
                                    variant="contained"
                                    color="primary"
                                    sx={styles.buttonPublish}
                                >
                                    Publicar
                                </Button>
                            </Grid>
                        </form>
                    </Stack>
                </Grid>
            </Grid>
        </>
    )
}

function ReviewFilters({ categories }) {
    return categories.map((categorie) => (
        <Grid item key={categorie.title}>
            <Button
                onClick={categorie.onClick}
                disabled={categorie.disabled}
                variant="contained"
                sx={styles.primaryButton}
            >
                <p>{categorie.title}</p>
            </Button>
        </Grid>
    ))
}

const SucessAlert = ({ onClose }) => (
    <Alert severity="success" onClose={onClose('showSuccess')}>
        Se publicó correctamente!
    </Alert>
)

const FailAlert = ({ onClose, message }) => (
    <Alert severity="error" onClose={onClose('showFail')}>
        {message}
    </Alert>
)

SucessAlert.propTypes = {
    onClose: PropTypes.func
}

FailAlert.propTypes = {
    onClose: PropTypes.func,
    message: PropTypes.string
}

ReviewFilters.propTypes = {
    categories: PropTypes.array
}

MakeReview.propTypes = {
    selectedItem: PropTypes.object,
    handleRollBack: PropTypes.func
}
