import React, { useEffect, useState } from 'react'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box'
import Stack from '@mui/material/Stack'
import { PostAdd } from '@mui/icons-material'
import ItemReviewCard from '@/components/ItemReviewCard'
import { getItemReviews } from '@/services/item'
import PropTypes from 'prop-types'
import { getUserSession } from '@/utils/session'
import { Typography } from '@mui/material'
import { getLikes, putLike } from '@/services/review'

const styles = {
    makeReviewButton: {
        mt: 1,
        bgcolor: '#00BA77',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#00EA94',
            color: '#FFFFFF',
            transform: 'scale(1.1)'
        },
        '&.MuiButtonBase-root': {
            height: 'min-content',
            margin: 0
        },
        '&.MuiButtonBase-root p': {
            margin: 0
        }
    },
    primaryButton: {
        mt: 1,
        bgcolor: '#000000',
        ':hover': {
            fontWeight: 'bold',
            bgcolor: '#D63535',
            color: '#ffffff',
            transform: 'scale(1.1)'
        },
        '&.MuiButtonBase-root': {
            height: 'min-content',
            margin: 0
        },
        '&.MuiButtonBase-root p': {
            margin: 0
        }
    }
}

export default function ListReviews({ selectedItem, handleMakeReview }) {
    const [state, setState] = useState({
        currentSelected: 'Quality',
        session: {},
        currentReviews: []
    })

    useEffect(() => {
        const session = getUserSession()
        setState((prevSt) => ({ ...prevSt, session }))
    }, [])

    useEffect(() => {
        async function fetchData() {
            const { isSuccess, data } = await getItemReviews(
                selectedItem.id,
                state.currentSelected.toLowerCase(),
                0,
                30
            )
            if (isSuccess) {
                const auxReviews = []
                for (const review of data) {
                    const { res, isSuccess } = await getLikes(review.id)
                    auxReviews.push({
                        ...review,
                        likes: isSuccess ? res : 0
                    })
                }
                setState((prvst) => ({ ...prvst, currentReviews: auxReviews }))
            } else {
                console.log('ERROR')
            }
        }
        fetchData()
    }, [selectedItem.id, state.currentSelected])

    const onClickCat = (category) => {
        return async (evt) => {
            evt.preventDefault()
            const { isSuccess, data } = await getItemReviews(selectedItem.id, category.toLowerCase(), 0, 30)
            if (isSuccess) {
                setState((prevSt) => ({ ...prevSt, category, currentSelected: category, currentReviews: data }))
            } else {
                console.log('ERROR')
            }
        }
    }

    const categories = [
        { onClick: onClickCat('Quality'), title: 'Calidad', disabled: state.currentSelected === 'Quality' },
        { onClick: onClickCat('Healthiness'), title: 'Salud', disabled: state.currentSelected === 'Healthiness' },
        {
            onClick: onClickCat('PriceFairness'),
            title: 'Economía',
            disabled: state.currentSelected === 'PriceFairness'
        },
        { onClick: onClickCat('Ecology'), title: 'Ecología', disabled: state.currentSelected === 'Ecology' }
    ]

    const onClickLike = async (reviewId) => {
        const { jwt, uid } = state.session
        const { res, isSuccess } = await putLike(jwt, uid, reviewId)
        if (isSuccess) {
            const auxReviews = state.currentReviews
            auxReviews.forEach((review, index) => {
                if (review.id === reviewId) {
                    auxReviews[index] = {
                        ...auxReviews[index],
                        likes: res ? auxReviews[index].likes + 1 : auxReviews[index].likes - 1
                    }
                }
            })
            setState((prevSt) => ({ ...prevSt, currentReviews: auxReviews }))
        } else {
            console.log('ERROR')
        }
    }

    return (
        <>
            <Stack direction="row" alignItems="center" justifyContent="flext-start" spacing={1}>
                <Grid direction="row" container gap={2}>
                    <Grid item container xs={12} sm={12} md="auto">
                        <Grid direction="row" item container gap={2} alignItems="center">
                            <ReviewFilters categories={categories} />
                        </Grid>
                    </Grid>
                    <Grid item container justifyContent="flex-end" xs={12} sm={12} md>
                        <Grid item>
                            {state.session ? (
                                <Button
                                    onClick={handleMakeReview}
                                    variant="contained"
                                    sx={styles.makeReviewButton}
                                    startIcon={<PostAdd />}
                                >
                                    <p>Make review</p>
                                </Button>
                            ) : (
                                <Typography variant="caption">
                                    Para apoyarnos con una <b>Review</b>, debes <a href={'/signin'}>iniciar sesión</a>
                                </Typography>
                            )}
                        </Grid>
                    </Grid>
                </Grid>
            </Stack>
            <Box>
                {state.currentReviews.map((itemReview) => {
                    const { user_name, score, content, likes } = itemReview
                    return (
                        <ItemReviewCard
                            key={itemReview.id}
                            id={itemReview.id}
                            user={user_name}
                            rating={score}
                            description={content}
                            likes={likes}
                            onClickLike={onClickLike}
                        />
                    )
                })}
            </Box>
        </>
    )
}

function ReviewFilters({ categories }) {
    return categories.map((categorie) => (
        <Grid item key={categorie.title}>
            <Button
                onClick={categorie.onClick}
                disabled={categorie.disabled}
                variant="contained"
                sx={styles.primaryButton}
            >
                <p>{categorie.title}</p>
            </Button>
        </Grid>
    ))
}

ListReviews.propTypes = {
    selectedItem: PropTypes.object,
    handleMakeReview: PropTypes.func
}
