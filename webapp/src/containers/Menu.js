import React, { useState, useEffect } from 'react'
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import Rating from '@mui/material/Rating'
import ButtonBase from '@mui/material/ButtonBase'
import InputAdornment from '@mui/material/InputAdornment'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import SearchIcon from '@mui/icons-material/Search'
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import Fab from '@mui/material/Fab'
import UpIcon from '@mui/icons-material/KeyboardArrowUp'
import MilitaryTechIcon from '@mui/icons-material/MilitaryTech'
import Image from 'next/image'

import PropTypes from 'prop-types'

import ListReviews from '@/containers/ListReviews'
import MakeReview from '@/containers/MakeReview'
import { ItemCard } from '@/components/ItemCard'
import {
    getItemsByCategoriesScore,
    getItemsByGlobalScore,
    getMenuGlobalScores,
    getMenusAndItemsByRestaurantId
} from '@/services/menu'
import { getImgUrl } from '@/services/image'
import { Refresh } from '@mui/icons-material'
import { getRestaurantById } from '@/services/restaurant'
import Modal from '@/components/Modal'
import GlobalScores from '@/components/GlobalScores'
import RankingPage from '@/containers/RankingPage'
import { IconButton } from '@mui/material'

const styles = {
    paper: {
        margin: 'auto',
        marginTop: '50px',
        marginBottom: '100px',
        padding: '0',
        width: '90vw',
        maxWidth: 1350,
        boxShadow: 'none',
        backgroundColor: (theme) => (theme.palette.mode === 'dark' ? '#1A2027' : '#fff')
    },
    menuName: {
        textAlign: 'center',
        padding: '20px'
    },
    boldText: {
        fontWeight: 'bold'
    },
    actionTitle: {
        width: '100%',
        textAlign: 'left',
        fontWeight: 'none',
        cursor: 'pointer'
    },
    actionOption: {
        width: '100%',
        padding: '10px'
    },
    generalRating: {
        '&.MuiRating-root span': {
            fontSize: '40px'
        },
        marginBottom: '10px'
    },
    filterRating: {
        '&.MuiRating-root span': {
            fontSize: '30px'
        },
        marginBottom: '10px'
    },
    showFiltersButton: {
        width: '100%',
        color: 'black',
        fontSize: '15px'
    },
    dialogTitle: {
        fontWeight: 'bold'
    },
    searchBar: {
        display: 'flex',
        direction: 'row',
        justifyContent: 'space-between',
        gap: '10px'
    },
    restaurantLogo: {
        borderRadius: '50%',
        width: '100%',
        height: 'auto',
        boxShadow: '2px 2px 2px 3px rgba(0, 0, 0, 0.2)',
        cursor: 'pointer'
    },
    floatingButton: {
        position: 'fixed',
        bottom: 200,
        right: 40
    },
    relativeContainer: {
        position: 'relative'
    },
    rankingButton: {
        position: 'absolute',
        top: 0,
        right: 0
    },
    rankingIcon: {
        color: '#000000',
        fontSize: '1.5em'
    }
}

export default function Menu({ restaurantId, defaultMealTypeId, defaultItemName }) {
    const [state, setState] = useState({
        id: '',
        mealTypes: [],
        restaurant: null,
        title: '',
        items: {},
        showAdvancedFilters: false,
        selectedItem: {},
        currentItems: [],
        searchInput: '',
        open: false,
        openModal: false,
        openRanking: false
    })

    const [filters, setFilters] = useState({
        General: { name: 'General', score: 0, type: 'general' },
        Quality: { name: 'Calidad', score: 0, type: 'quality' },
        Healthiness: { name: 'Salud', score: 0, type: 'healthiness' },
        PriceFairness: { name: 'Economia', score: 0, type: 'pricefairness' },
        Ecology: { name: 'Ecologia', score: 0, type: 'ecology' }
    })
    const [currentMealType, setCurrentMealType] = useState({
        id: undefined,
        title: ''
    })

    const filterTypes = ['General', 'Quality', 'Healthiness', 'PriceFairness', 'Ecology']

    useEffect(() => {
        const fetchData = async () => {
            const restaurant = await getRestaurantById(restaurantId)
            //TODO!
            //El getMenusAndItemsByRestaurantId, deberia traer el id del logo del restaurant
            const { menu, items } = await getMenusAndItemsByRestaurantId(restaurantId)
            const title = menu.title
            const mealTypes = menu.meal_types
            const defaultMealtype = defaultMealTypeId
                ? { id: defaultMealTypeId }
                : mealTypes.length > 0
                ? mealTypes[0]
                : undefined
            const defaultItems = defaultItemName
                ? items[defaultMealtype.id].filter((item) => item.title.toLowerCase() === defaultItemName.toLowerCase())
                : items[defaultMealtype.id]

            setCurrentMealType((prvst) => ({ ...prvst, ...defaultMealtype }))
            setState((prvst) => ({
                ...prvst,
                id: menu.id,
                mealTypes,
                title,
                items,
                restaurant,
                currentItems: defaultItems
            }))
        }

        fetchData()
    }, [restaurantId, defaultItemName, defaultMealTypeId])

    const onClick = (id) => {
        return () => {
            setCurrentMealType({ ...currentMealType, id })
            setState({ ...state, currentItems: state.items[id] })
            setFilters((prevSt) => {
                let updateFilters = { ...prevSt }
                filterTypes.forEach((filter) => {
                    updateFilters[filter].score = 0
                })

                return updateFilters
            })
        }
    }

    function showMoreFilters(event) {
        event.preventDefault()
        setState({ ...state, showAdvancedFilters: !state.showAdvancedFilters })
    }

    const handleOpen = (item) => {
        setState((prvst) => ({ ...prvst, selectedItem: item, open: true }))
    }
    const handleClose = () => {
        setState((prvst) => ({ ...prvst, open: false, makeReview: false }))
    }

    function onChangeFilter(filter) {
        return async (evt) => {
            evt.preventDefault()
            const value = Number(evt.target.value)
            let selectedFilters = ''
            let query = undefined

            setFilters((prevSt) => {
                let updateFilters = { ...prevSt }

                if (filter === 'General') {
                    updateFilters[filter].score = value

                    filterTypes.forEach((type) => {
                        if (type !== 'General') updateFilters[type].score = 0
                    })

                    query = async (mealTypeId) => await getItemsByGlobalScore(mealTypeId, value)
                } else {
                    updateFilters[filter].score = value
                    updateFilters['General'].score = 0

                    filterTypes.forEach((type) => {
                        const score = updateFilters[type].score
                        const filterType = updateFilters[type].type

                        if (updateFilters[type].score > 0) {
                            selectedFilters += `scores=${filterType}-${score}&`
                        }
                    })

                    query = async (mealTypeId) => await getItemsByCategoriesScore(mealTypeId, selectedFilters)
                }

                return { ...updateFilters }
            })

            const response = await query(currentMealType.id)
            console.log(response)
            if (response.isSuccess) {
                const currentItems = response.res
                setState((prevSt) => ({ ...prevSt, currentItems }))
            }
        }
    }

    async function handleRefresh(evt) {
        evt.preventDefault()

        setFilters((prevSt) => {
            let updateFilters = { ...prevSt }

            filterTypes.forEach((filter) => {
                updateFilters[filter].score = 0
            })

            return updateFilters
        })
        //TODO!
        //Deberia haber solo un getItems
        const { items } = await getMenusAndItemsByRestaurantId(restaurantId)

        setState((prevSt) => ({
            ...prevSt,
            currentItems: items[currentMealType.id],
            items,
            searchInput: ''
        }))
    }

    function onChangeSearchBar(evt) {
        evt.preventDefault()
        const value = evt.target.value.toLowerCase()
        let currentItems = state.items[currentMealType.id]
        if (value && value.length > 0) {
            currentItems = state.currentItems.filter((item) => item.title.toLowerCase().includes(value))
        }

        setState((prevSt) => ({ ...prevSt, currentItems, searchInput: value }))
    }

    function handleShowScoresInfo(evt) {
        evt.preventDefault()
        setState((prevSt) => ({ ...prevSt, openModal: true }))
    }

    function handleCloseScoresInfo(evt) {
        evt.preventDefault()
        setState((prvst) => ({ ...prvst, openModal: false }))
    }

    function handleShowRanking(evt) {
        evt.preventDefault()
        setState((prevSt) => ({ ...prevSt, openRanking: true }))
    }

    function handleCloseRanking(evt) {
        evt.preventDefault()
        setState((prvst) => ({ ...prvst, openRanking: false }))
    }

    const restaurantLogo = state.restaurant ? (
        <Image
            style={styles.restaurantLogo}
            alt="restaurant-logo"
            onClick={handleShowScoresInfo}
            src={getImgUrl(state.restaurant.logo_id, 500, 500)}
            width={500}
            height={500}
        />
    ) : null

    return (
        <Paper sx={styles.paper}>
            <Grid container direction="row" spacing={2} columns={{ xs: 1, sm: 2, md: 2 }}>
                <Grid
                    name="menuActions"
                    justifyContent="flex-start"
                    item
                    xs={12}
                    sm={12}
                    md={3}
                    container
                    direction="column"
                    gap={3}
                    sx={styles.relativeContainer}
                >
                    <Grid item container direction="column" justifyContent="center" gap={2}>
                        <Grid item container justifyContent="center" alignContent="center" direction="row">
                            <Grid item xs={12} sm={12} md={12}>
                                {restaurantLogo}
                                <IconButton sx={styles.rankingButton} onClick={handleShowRanking}>
                                    <MilitaryTechIcon sx={styles.rankingIcon} />
                                </IconButton>
                            </Grid>
                            <Grid item>
                                <Typography gutterBottom sx={styles.menuName} variant="h4">
                                    {state.title}
                                </Typography>
                                <hr />
                            </Grid>
                        </Grid>
                        <Categories
                            mealtypes={state.mealTypes}
                            currentMealTypeId={currentMealType.id}
                            onClick={onClick}
                        />
                    </Grid>
                    <Grid name="menuFilters" item container direction="column" spacing={2}>
                        <Grid item xs={1}>
                            <Typography gutterBottom sx={styles.boldText} variant="h6">
                                Filtros
                            </Typography>
                        </Grid>
                        <Grid item container variant="row" xs={1}>
                            <SearchBar
                                onChangeSearchBar={onChangeSearchBar}
                                searchInput={state.searchInput}
                                handleRefresh={handleRefresh}
                            />
                        </Grid>
                        <Grid item xs={1}>
                            <Typography gutterBottom variant="h6">
                                General
                            </Typography>
                            <Rating
                                name="rating"
                                onChange={onChangeFilter('General')}
                                defaultValue={0}
                                value={filters.General.score}
                                precision={0.5}
                                sx={styles.generalRating}
                            />
                        </Grid>
                        <Grid item xs={1}>
                            <Button
                                sx={styles.showFiltersButton}
                                endIcon={state.showAdvancedFilters ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
                                onClick={showMoreFilters}
                            >
                                Filtros avanzados
                            </Button>
                        </Grid>
                        <AdvancedFilters
                            onChangeFilter={onChangeFilter}
                            filters={filters}
                            showAdvancedFilters={state.showAdvancedFilters}
                        />
                    </Grid>
                </Grid>
                <Grid name="menuItems" item xs={12} sm={12} md={9} container>
                    <Grid item container direction="column">
                        <CurrentItems
                            items={state.currentItems}
                            currentMealTypeId={currentMealType.id}
                            handleOpen={handleOpen}
                        />
                    </Grid>
                </Grid>
            </Grid>
            <Fab sx={styles.floatingButton} onClick={() => window.scrollTo(0, 0)} variant="extended" color="primary">
                <UpIcon />
            </Fab>
            <ModalReview handleClose={handleClose} open={state.open} selectedItem={state.selectedItem} />
            <Modal
                open={state.openModal}
                handleCloseModal={handleCloseScoresInfo}
                title={`${state.title} | Scores`}
                maxWidth="sm"
            >
                <DialogContent dividers={true}>
                    <GlobalScores getGlobalScores={() => getMenuGlobalScores(state.id)} />
                </DialogContent>
            </Modal>
            <Modal open={state.openRanking} handleCloseModal={handleCloseRanking} sx={styles.modal}>
                <RankingPage restaurantId={restaurantId} />
            </Modal>
        </Paper>
    )
}

function Categories({ mealtypes, onClick, currentMealTypeId }) {
    return (
        <Grid name="menuCategories" item container justifyContent="left" direction="column">
            <Grid item xs={1}>
                <Typography gutterBottom sx={styles.boldText} variant="h6">
                    Categorias
                </Typography>
            </Grid>
            {mealtypes.map((mealtype) => (
                <Mealtype
                    key={mealtype.id}
                    id={mealtype.id}
                    name={mealtype.title}
                    currentMealTypeId={currentMealTypeId}
                    onClick={onClick}
                />
            ))}
        </Grid>
    )
}

function Mealtype({ name, onClick, id, currentMealTypeId }) {
    const selected = currentMealTypeId == id
    return (
        <Grid item xs={1}>
            <ButtonBase sx={styles.actionOption} onClick={onClick(id)} disabled={selected}>
                <Typography
                    gutterBottom
                    sx={selected ? { ...styles.actionTitle, fontStyle: 'italic' } : styles.actionTitle}
                    variant="body1"
                >
                    {name}
                </Typography>
            </ButtonBase>
        </Grid>
    )
}

function CurrentItems({ items, handleOpen }) {
    return items
        ? items.map((item) => (
              <Grid key={item.id} item xs={1}>
                  <ItemCard
                      id={item.id}
                      onClick={() => handleOpen(item)}
                      description={item.description}
                      name={item.title}
                      price={item.price}
                      rating={item.global_score}
                      imageUrl={getImgUrl(item.image_id, 500, 500)}
                  />
              </Grid>
          ))
        : null
}

function SearchBar({ handleRefresh, onChangeSearchBar, searchInput }) {
    return (
        <>
            <div style={styles.searchBar}>
                <TextField
                    label="Search"
                    variant="outlined"
                    onChange={onChangeSearchBar}
                    value={searchInput}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <SearchIcon />
                            </InputAdornment>
                        )
                    }}
                />
                <Button onClick={handleRefresh} variant="outlined">
                    <Refresh />
                </Button>
            </div>
        </>
    )
}

const AdvancedFilters = ({ showAdvancedFilters, onChangeFilter, filters }) => {
    if (showAdvancedFilters) {
        return (
            <Grid name="advancedFilters" item container direction="column" spacing={1}>
                <Grid item xs={1}>
                    <Typography gutterBottom sx={styles.actionTitle} variant="body1">
                        Calidad
                    </Typography>
                    <Rating
                        name="rating"
                        onChange={onChangeFilter('Quality')}
                        defaultValue={0}
                        value={filters.Quality.score}
                        precision={0.5}
                        sx={styles.filterRating}
                    />
                </Grid>
                <Grid item xs={1}>
                    <Typography gutterBottom sx={styles.actionTitle} variant="body1">
                        Salud
                    </Typography>
                    <Rating
                        name="rating"
                        onChange={onChangeFilter('Healthiness')}
                        defaultValue={0}
                        value={filters.Healthiness.score}
                        precision={0.5}
                        sx={styles.filterRating}
                    />
                </Grid>
                <Grid item xs={1}>
                    <Typography gutterBottom sx={styles.actionTitle} variant="body1">
                        Economia
                    </Typography>
                    <Rating
                        name="rating"
                        onChange={onChangeFilter('PriceFairness')}
                        defaultValue={0}
                        value={filters.PriceFairness.score}
                        precision={0.5}
                        sx={styles.filterRating}
                    />
                </Grid>
                <Grid item xs={1}>
                    <Typography gutterBottom sx={styles.actionTitle} variant="body1">
                        Ecologia
                    </Typography>
                    <Rating
                        name="rating"
                        onChange={onChangeFilter('Ecology')}
                        defaultValue={0}
                        value={Number(filters.Ecology.score)}
                        precision={0.5}
                        sx={styles.filterRating}
                    />
                </Grid>
            </Grid>
        )
    } else {
        return null
    }
}

function ModalReview({ handleClose, open, selectedItem }) {
    const [state, setState] = useState({
        makeReview: false
    })

    function handleMakeReview() {
        setState((prvSt) => ({ ...prvSt, makeReview: true }))
    }

    function handleRollBack() {
        setState((prvSt) => ({ ...prvSt, makeReview: false }))
    }

    function handleCloseModal() {
        handleClose()
        setState((prvSt) => ({ ...prvSt, makeReview: false }))
    }

    const currentDialogContent = state.makeReview ? (
        <>
            <DialogTitle sx={styles.dialogTitle} id="scroll-dialog-title">
                Reseña | {selectedItem.title}
            </DialogTitle>
            <DialogContent dividers={true}>
                <MakeReview selectedItem={selectedItem} handleRollBack={handleRollBack} />
            </DialogContent>
        </>
    ) : (
        <>
            <DialogTitle sx={styles.dialogTitle} id="scroll-dialog-title">
                {selectedItem.title}
            </DialogTitle>
            <DialogContent dividers={true}>
                <ListReviews selectedItem={selectedItem} handleMakeReview={handleMakeReview} />
            </DialogContent>
        </>
    )

    return (
        <div>
            <Modal open={open} handleCloseModal={handleCloseModal}>
                {currentDialogContent}
            </Modal>
        </div>
    )
}

// Menu.getInitialProps = (context) => {
//     const { query } = context
//     const { restaurantId } = query
//     return { restaurantId }
// }

Menu.propTypes = {
    restaurantId: PropTypes.string.isRequired,
    defaultItemName: PropTypes.string,
    defaultMealTypeId: PropTypes.string
}

CurrentItems.propTypes = {
    items: PropTypes.array,
    handleOpen: PropTypes.func
}

SearchBar.propTypes = {
    handleRefresh: PropTypes.func,
    searchInput: PropTypes.string,
    onChangeSearchBar: PropTypes.func
}

AdvancedFilters.propTypes = {
    showAdvancedFilters: PropTypes.bool,
    filters: PropTypes.object,
    onChangeFilter: PropTypes.func
}

Categories.propTypes = {
    mealtypes: PropTypes.array,
    onClick: PropTypes.func,
    currentMealTypeId: PropTypes.string
}

Mealtype.propTypes = {
    name: PropTypes.string,
    id: PropTypes.string,
    onClick: PropTypes.func,
    currentMealTypeId: PropTypes.string
}

ModalReview.propTypes = {
    handleClose: PropTypes.func,
    open: PropTypes.bool,
    selectedItem: PropTypes.object,
    makeReview: PropTypes.bool
}
