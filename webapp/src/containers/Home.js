import { useState } from 'react'
import PropTypes from 'prop-types'
import { InputAdornment, TextField, Grid } from '@mui/material'
import SearchIcon from '@mui/icons-material/Search'
import IconButton from '@mui/material/IconButton'
import Image from 'next/image'
import { getRestaurantsByWord } from '@/services/restaurant'
import { getImgUrl } from '@/services/image'
import { BasicRestaurantCard } from '@/components/BasicRestaurantCard'
import Carousel from '@/components/Carousel'

const style = {
    title: {
        textAlign: 'center'
    },
    search: {
        maxWidth: '70%',
        margin: '0 auto',
        marginTop: '50px'
    },
    textField: {
        '& .MuiInputBase-root ': {
            padding: '5px',
            borderRadius: 13
        }
    },
    imageContainer: {
        paddingTop: '50px',
        display: 'flex',
        justifyContent: 'center'
    },
    divider: {
        my: 1
    },
    searchedRestaurant: {
        cursor: 'pointer',
        minWidth: '265px',
        width: '40%',
        minHeight: '100%',
        margin: '0 10px 0 10px'
    }
}

export default function Home() {
    const [state, setState] = useState({
        filter: '',
        restaurants: null,
        searchType: 'restaurant'
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }
                return newState
            }
            setState(updateState)
        }
    }

    const onClickSearchButton = async (evt) => {
        evt.preventDefault()
        const { res, isSuccess } = await getRestaurantsByWord(state.filter, 0, 10)
        if (isSuccess) {
            setState((prevSt) => ({
                ...prevSt,
                restaurants: res
            }))
        } else {
            console.log('ERROR')
        }
    }

    return (
        <div>
            <div style={style.imageContainer}>
                <Image src="/logo_nobackground.png" alt="bg-image" width={300} height={250} />
            </div>
            <div style={style.search}>
                <form onSubmit={onClickSearchButton}>
                    <TextField
                        sx={style.textField}
                        label=""
                        placeholder="Inserta un restaurante..."
                        variant="outlined"
                        size="small"
                        fullWidth
                        value={state.filter}
                        onChange={handleInputChange('filter')}
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <IconButton onClick={onClickSearchButton}>
                                        <SearchIcon />
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                    />
                </form>
            </div>
            <br />
            <RestaurantsResults restaurants={state.restaurants} />
        </div>
    )
}

function RestaurantsResults({ restaurants }) {
    if (!restaurants) {
        return null
    }

    if (restaurants.length <= 0) {
        return (
            <Grid container justifyContent="center">
                <Grid item>
                    <p>No se han encontrado restaurantes</p>
                </Grid>
            </Grid>
        )
    }

    const restaurantResults = restaurants.map((restaurant) => {
        const { id, name, address, phone_number, description, email, background_image_id, logo_id } = restaurant
        const backgroundUrl = getImgUrl(background_image_id, 1000, 700)
        const logoUrl = getImgUrl(logo_id, 500, 500)

        return (
            <div key={id} style={style.searchedRestaurant}>
                <BasicRestaurantCard
                    restaurantInfo={{
                        id,
                        name,
                        address,
                        phone_number,
                        description,
                        email,
                        logo: logoUrl,
                        background: backgroundUrl
                    }}
                />
            </div>
        )
    })

    return <Carousel contentLength={restaurantResults.length}>{restaurantResults}</Carousel>
}

RestaurantsResults.propTypes = {
    restaurants: PropTypes.array
}
