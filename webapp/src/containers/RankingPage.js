import React, { useEffect, useState } from 'react'
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material'
import PropTypes from 'prop-types'
import { getUserRanking } from '@/services/user'

const style = {
    tableRow: {
        '&:last-child td, &:last-child th': { border: 0 }
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalContent: {
        backgroundColor: '#fff',
        padding: '20px',
        borderRadius: '4px'
    },
    position1: {
        backgroundColor: 'gold',
        fontWeight: 'bold'
    },
    position2: {
        backgroundColor: 'silver',
        fontWeight: 'bold'
    },
    position3: {
        backgroundColor: 'tan',
        fontWeight: 'bold'
    }
}

export default function RankingPage({ restaurantId }) {
    const [rankingData, setRankingData] = useState([])

    useEffect(() => {
        const fetchRankingData = async () => {
            const from = 0
            const to = 10

            const { res, isSuccess, error } = await getUserRanking(restaurantId, from, to)

            if (isSuccess) {
                setRankingData(res)
            } else {
                console.error(error)
            }
        }

        fetchRankingData()
    }, [restaurantId])

    const fields = [
        { text: 'Nombre', value: 'name' },
        { text: 'Número de Reviews', value: 'reviews' },
        { text: 'Número de Likes', value: 'likes' }
    ]

    return (
        <>
            <div style={style.modalContent}>
                <h2>Ranking de Usuarios</h2>
                <RankingTable fields={fields} rows={rankingData} />
            </div>
        </>
    )
}

const RankingTable = ({ fields, rows }) => {
    return (
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Posición</TableCell>
                        {fields.map((field, index) => {
                            return <TableCell key={index}>{field.text}</TableCell>
                        })}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, index) => (
                        <TableRow key={index}>
                            <TableCell sx={index < 3 ? style[`position${index + 1}`] : null}>{index + 1}</TableCell>
                            {fields.map((field, columnIndex) => {
                                const cellStyle = index < 3 ? style[`position${index + 1}`] : null
                                return (
                                    <TableCell key={columnIndex} sx={cellStyle}>
                                        {row[`${field.value}`]}
                                    </TableCell>
                                )
                            })}
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

RankingTable.propTypes = {
    fields: PropTypes.array.isRequired,
    rows: PropTypes.array.isRequired
}

RankingPage.propTypes = {
    restaurantId: PropTypes.string.isRequired
}
