import React, { useEffect, useState } from 'react'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import TextField from '@mui/material/TextField'
import Link from '@mui/material/Link'
import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import AccessibilityIcon from '@mui/icons-material/Accessibility'
import Typography from '@mui/material/Typography'
import Container from '@mui/material/Container'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import { basicAuth, mkPutUserPayload } from '@/services/user'
import { decodeJWT, getUserSession, mkSession, storeUserSession } from '@/utils/session'
import { signinValidator } from '@/utils/validators'
import { TextFieldError } from '@/components/TextFieldError'

const style = {
    boxContainer: {
        marginTop: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    iconForm: {
        m: 1,
        bgcolor: '#FF6161'
    },
    boxContainerForm: {
        mt: 3
    },
    logInButtom: {
        mt: 3,
        mb: 2
    }
}

export default function SignIn() {
    const router = useRouter()
    const [state, setState] = useState({
        email: '',
        password: '',
        submitDisabled: true,
        notSucess: false,
        validator: null
    })

    const handleInputChange = (attr) => {
        return (evt) => {
            evt.preventDefault()
            const updateState = (prevSt) => {
                let newState = { ...prevSt, [attr]: evt.target.value }
                const validator = signinValidator(newState)

                if (validator && validator.allValid) {
                    newState.submitDisabled = false
                } else {
                    newState.submitDisabled = true
                }
                newState.validator = validator

                return newState
            }

            setState(updateState)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        if (state.validator && state.validator.allValid) {
            const userData = mkPutUserPayload(state.email, state.password)

            const result = await basicAuth(userData.email, userData.password)

            if (result.isSuccess) {
                const jwt = result.data.jwt
                const payload = decodeJWT(jwt)
                const session = mkSession(payload.sub, payload.aud, jwt, result.data.isAdmin)

                storeUserSession(session)

                router.push('/')
            } else {
                setState({ ...state, notSucess: true })
                console.error(result.error.description)
            }
        } else {
            console.log('Null parameters')
        }
    }

    useEffect(() => {
        const userSession = getUserSession()
        if (userSession) {
            if (userSession.isAdmin) {
                router.push('/admin/profile')
            } else {
                router.push('/user/profile')
            }
        }
    }, [router])

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box sx={style.boxContainer}>
                <Avatar sx={style.iconForm}>
                    <AccessibilityIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Bienvenido a SmartMeals!
                </Typography>
                <Box component="form" noValidate onSubmit={handleSubmit} sx={style.boxContainerForm}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                id="email"
                                label="Correo electrónico"
                                name="email"
                                autoComplete="email"
                                onChange={handleInputChange('email')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                required
                                fullWidth
                                name="password"
                                label="Contraseña"
                                type="password"
                                id="password"
                                autoComplete="new-password"
                                onChange={handleInputChange('password')}
                            />
                        </Grid>
                        <FieldError notSuccess={state.notSucess} />
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={style.logInButtom}
                        disabled={state.submitDisabled}
                    >
                        Iniciar Sesión
                    </Button>
                    <Grid container justifyContent="flex-end">
                        <Grid item>
                            <Link href="/signup" variant="body2">
                                ¿No tienes cuenta? Registrate
                            </Link>
                        </Grid>
                    </Grid>
                </Box>
            </Box>
        </Container>
    )
}

function FieldError({ notSuccess }) {
    const fieldError = !notSuccess ? null : <TextFieldError message="Error de autenticacion" />
    return fieldError
}

FieldError.propTypes = {
    notSuccess: PropTypes.bool.isRequired
}
