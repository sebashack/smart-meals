import SignUp from '@/containers/SignUp'
import { Layout } from '@/components/Layout'

export default function SignInPage() {
    return (
        <Layout>
            <SignUp />
        </Layout>
    )
}
