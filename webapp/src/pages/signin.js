import SignIn from '@/containers/SignIn'
import { Layout } from '@/components/Layout'

export default function SignInPage() {
    return (
        <Layout>
            <SignIn />
        </Layout>
    )
}
