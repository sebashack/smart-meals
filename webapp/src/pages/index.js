import { Layout } from '../components/Layout'
import Home from '@/containers/Home'

export default function HomePage() {
    return (
        <Layout>
            <Home />
        </Layout>
    )
}
