import Menu from '@/containers/Menu'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'

export default function MenuPage({ restaurantId, defaultMealTypeId, defaultItemName }) {
    return (
        <Layout>
            <Menu restaurantId={restaurantId} defaultMealTypeId={defaultMealTypeId} defaultItemName={defaultItemName} />
        </Layout>
    )
}

MenuPage.propTypes = {
    restaurantId: PropTypes.string.isRequired,
    defaultMealTypeId: PropTypes.string,
    defaultItemName: PropTypes.string
}

MenuPage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId, defaultMealTypeId, defaultItemName } = query
    return { restaurantId, defaultMealTypeId, defaultItemName }
}
