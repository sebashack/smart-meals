import Restaurant from '@/containers/Restaurant'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'

export default function RestaurantPage({ restaurantId }) {
    return (
        <Layout>
            <Restaurant restaurantId={restaurantId} />
        </Layout>
    )
}

RestaurantPage.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

RestaurantPage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query

    return { restaurantId }
}
