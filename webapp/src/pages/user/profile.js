import React from 'react'
import { Layout } from '@/components/Layout'
import { GuardUser } from '@/components/GuardUser'
import Profile from '@/containers/user/Profile'

export default function ProfilePage() {
    return (
        <GuardUser>
            <Layout>
                <Profile />
            </Layout>
        </GuardUser>
    )
}
