import React from 'react'
import { Layout } from '@/components/Layout'
import { GuardAdmin } from '@/components/GuardAdmin'
import AdminProfile from '@/containers/admin/Profile'

export default function ProfilePage() {
    return (
        <GuardAdmin>
            <Layout>
                <AdminProfile />
            </Layout>
        </GuardAdmin>
    )
}
