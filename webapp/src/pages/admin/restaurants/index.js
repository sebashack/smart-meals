import React from 'react'
import { Layout } from '@/components/Layout'
import { GuardAdmin } from '@/components/GuardAdmin'
import ListRestaurants from '@/containers/admin/restaurant/ListRestaurants'

export default function ListRestaurantsPage() {
    return (
        <GuardAdmin>
            <Layout>
                <ListRestaurants />
            </Layout>
        </GuardAdmin>
    )
}
