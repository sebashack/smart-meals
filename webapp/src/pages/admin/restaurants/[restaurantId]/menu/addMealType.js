import AddMealType from '@/containers/admin/mealType/AddMealType'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function AddMealTypePage({ restaurantId }) {
    return (
        <GuardAdmin>
            <Layout>
                <AddMealType restaurantId={restaurantId} />
            </Layout>
        </GuardAdmin>
    )
}

AddMealTypePage.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

AddMealTypePage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query
    return { restaurantId }
}
