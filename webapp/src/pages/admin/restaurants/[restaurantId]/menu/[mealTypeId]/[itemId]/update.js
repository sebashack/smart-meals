import UpdateItem from '@/containers/admin/item/UpdateItem'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function UpdateItemPage({ restaurantId, mealTypeId, itemId }) {
    return (
        <GuardAdmin>
            <Layout>
                <UpdateItem restaurantId={restaurantId} mealTypeId={mealTypeId} itemId={itemId} />
            </Layout>
        </GuardAdmin>
    )
}

UpdateItemPage.propTypes = {
    itemId: PropTypes.string.isRequired,
    restaurantId: PropTypes.string.isRequired,
    mealTypeId: PropTypes.string.isRequired
}

UpdateItemPage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId, itemId, mealTypeId } = query
    return { restaurantId, itemId, mealTypeId }
}
