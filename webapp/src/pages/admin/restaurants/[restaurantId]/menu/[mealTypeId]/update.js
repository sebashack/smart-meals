import UpdateMealType from '@/containers/admin/mealType/UpdateMealType'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function UpdateMealTypePage({ restaurantId, mealTypeId }) {
    return (
        <GuardAdmin>
            <Layout>
                <UpdateMealType restaurantId={restaurantId} mealTypeId={mealTypeId} />
            </Layout>
        </GuardAdmin>
    )
}

UpdateMealTypePage.propTypes = {
    restaurantId: PropTypes.string.isRequired,
    mealTypeId: PropTypes.string.isRequired
}

UpdateMealTypePage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId, mealTypeId } = query
    return { restaurantId, mealTypeId }
}
