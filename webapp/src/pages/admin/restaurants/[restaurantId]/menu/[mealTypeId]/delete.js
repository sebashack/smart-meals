import DeleteMealType from '@/containers/admin/mealType/DeleteMealType'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function DeleteMealTypePage({ mealTypeId }) {
    return (
        <GuardAdmin>
            <Layout>
                <DeleteMealType mealTypeId={mealTypeId} />
            </Layout>
        </GuardAdmin>
    )
}

DeleteMealTypePage.propTypes = {
    restaurantId: PropTypes.string.isRequired,
    mealTypeId: PropTypes.string.isRequired
}

DeleteMealTypePage.getInitialProps = (context) => {
    const { query } = context
    const { mealTypeId } = query
    return { mealTypeId }
}
