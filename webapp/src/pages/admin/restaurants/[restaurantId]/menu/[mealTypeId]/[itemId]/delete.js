import DeleteItem from '@/containers/admin/item/DeleteItem'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function DeleteItemPage({ itemId }) {
    return (
        <GuardAdmin>
            <Layout>
                <DeleteItem itemId={itemId} />
            </Layout>
        </GuardAdmin>
    )
}

DeleteItemPage.propTypes = {
    itemId: PropTypes.string.isRequired
}

DeleteItemPage.getInitialProps = (context) => {
    const { query } = context
    const { itemId } = query
    return { itemId }
}
