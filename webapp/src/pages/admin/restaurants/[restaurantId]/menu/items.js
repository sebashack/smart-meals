import ListItems from '@/containers/admin/item/ListItems'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function ListItemsPage({ restaurantId }) {
    return (
        <GuardAdmin>
            <Layout>
                <ListItems restaurantId={restaurantId} />
            </Layout>
        </GuardAdmin>
    )
}

ListItemsPage.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

ListItemsPage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query
    return { restaurantId }
}
