import AddItem from '@/containers/admin/item/AddItem'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function AddItemPage({ restaurantId }) {
    return (
        <GuardAdmin>
            <Layout>
                <AddItem restaurantId={restaurantId} />
            </Layout>
        </GuardAdmin>
    )
}

AddItemPage.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

AddItemPage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query
    return { restaurantId }
}
