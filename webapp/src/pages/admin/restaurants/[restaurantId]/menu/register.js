import RegisterMenu from '@/containers/admin/menu/RegisterMenu'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function RegisterMenuPage({ restaurantId }) {
    return (
        <GuardAdmin>
            <Layout>
                <RegisterMenu restaurantId={restaurantId} />
            </Layout>
        </GuardAdmin>
    )
}

RegisterMenuPage.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

RegisterMenuPage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query
    return { restaurantId }
}
