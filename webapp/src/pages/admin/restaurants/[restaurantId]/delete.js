import DeleteRestaurant from '@/containers/admin/restaurant/DeleteRestaurant'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function DeleteRestaurantPage({ restaurantId }) {
    return (
        <GuardAdmin>
            <Layout>
                <DeleteRestaurant restaurantId={restaurantId} />
            </Layout>
        </GuardAdmin>
    )
}

DeleteRestaurantPage.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

DeleteRestaurantPage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query
    return { restaurantId }
}
