import UpdateRestaurant from '@/containers/admin/restaurant/UpdateRestaurant'
import { Layout } from '@/components/Layout'
import PropTypes from 'prop-types'
import { GuardAdmin } from '@/components/GuardAdmin'

export default function UpdateRestaurantPage({ restaurantId }) {
    return (
        <GuardAdmin>
            <Layout>
                <UpdateRestaurant restaurantId={restaurantId} />
            </Layout>
        </GuardAdmin>
    )
}

UpdateRestaurantPage.propTypes = {
    restaurantId: PropTypes.string.isRequired
}

UpdateRestaurantPage.getInitialProps = (context) => {
    const { query } = context
    const { restaurantId } = query
    return { restaurantId }
}
