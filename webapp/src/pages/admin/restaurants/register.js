import React from 'react'
import { Layout } from '@/components/Layout'
import { GuardAdmin } from '@/components/GuardAdmin'
import RegisterRestaurant from '@/containers/admin/restaurant/RegisterRestaurant'

export default function ProfilePage() {
    return (
        <GuardAdmin>
            <Layout>
                <RegisterRestaurant />
            </Layout>
        </GuardAdmin>
    )
}
