import { encode } from 'js-base64'

export async function postUser(userPayload) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')

    const response = await fetch(`${host}/users`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(userPayload)
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.json()
        const data = { jwt: res }

        return { data, isSuccess: true }
    } else {
        const description = await response.text()
        const error = { description, isDuplicatedEmail: false }
        if (description.toLocaleLowerCase() === 'duplicated email') {
            error.isDuplicatedEmail = true
        }

        return { error, isSuccess: false }
    }
}

export async function basicAuth(email, password) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.set('Authorization', 'Basic ' + encode(email + ':' + password))

    const response = await fetch(`${host}/users/auth`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code == 200) {
        const res = await response.json()
        const data = {
            id: res.id,
            email: res.email,
            name: res.name,
            isAdmin: res.is_admin,
            country: res.country,
            city: res.city,
            birthdate: res.birthdate,
            jwt: res.jwt
        }
        return { data, isSuccess: true }
    } else {
        const description = await response.text()
        const error = { description }

        return { error, isSuccess: false }
    }
}

export async function deleteToken(jwt, uid) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/users/auth`, {
        method: 'DELETE',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code == 200) {
        return { data: null, isSuccess: true }
    } else {
        const description = await response.text()
        const error = { description }

        return { error, isSuccess: false }
    }
}

export async function getUserInfo(jwt, uid) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/users/profile`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status
    if (code == 200) {
        const res = await response.json()
        const data = {
            id: res.id,
            email: res.email,
            name: res.name,
            isAdmin: res.is_admin,
            country: res.country,
            city: res.city,
            birthdate: res.birthdate
        }
        return { data, isSuccess: true }
    } else {
        const description = await response.text()
        const error = { description }

        return { error, isSuccess: false }
    }
}

export async function getUserReviews(jwt, uid, category, from, to) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/users/reviews?category=${category}&from=${from}&to=${to}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status
    if (code == 200) {
        const res = await response.json()

        return { data: res, isSuccess: true }
    } else {
        const description = await response.text()
        const error = { description }

        return { error, isSuccess: false }
    }
}

export async function getUserRecommendationByCategory(jwt, uid, category, offset, to) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/users/recommendations?category=${category}&offset=${offset}&to=${to}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status
    if (code == 200) {
        const res = await response.json()

        return { data: res, isSuccess: true }
    } else {
        const description = await response.text()
        const error = { description }

        return { error, isSuccess: false }
    }
}

export async function getUserGlobalScores(jwt, uid) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/users/scores`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status
    if (code == 200) {
        const res = await response.json()
        return { data: res, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function getUserRanking(restaurantId, from, to) {
    const host = process.env.API_HOST
    const headers = new Headers()
    headers.append('Content-Type', 'application/json')

    const response = await fetch(`${host}/users/ranking?restaurant_id=${restaurantId}&offset=${from}&to=${to}`, {
        method: 'GET',
        mode: 'cors'
    })

    const code = response.status

    if (code === 200) {
        const res = await response.json()
        return { res, isSuccess: true }
    } else {
        const error = await response.text()
        return { error, isSuccess: false }
    }
}

export function mkPostUserPayload(email, name, password, city, country, birthdate, isAdmin) {
    return { email, name, password, city, country, birthdate, is_admin: isAdmin }
}

export function mkPutUserPayload(email, password) {
    return { email, password }
}
