export async function getMenusAndItemsByRestaurantId(restaurantId) {
    const host = process.env.API_HOST

    const response = await fetch(`${host}/restaurants/${restaurantId}/menus`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code == 200) {
        const data = await response.json()
        return data
    } else if (code === 404) {
        return null
    } else {
        const error = await response.text()
        throw error
    }
}

export async function postMenu(jwt, adminId, restaurantId, menuData) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/${restaurantId}/menus`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(menuData)
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.json()

        return { res, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function getItemsByGlobalScore(mealTypeId, globalScore) {
    const host = process.env.API_HOST

    const response = await fetch(
        `${host}/restaurants/menus/meal-types/${mealTypeId}/global-search/items/${globalScore}`,
        {
            method: 'GET',
            mode: 'cors'
        }
    )

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.json()

        return { res, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function getItemsByCategoriesScore(mealTypeId, categoriesScores) {
    const host = process.env.API_HOST

    const response = await fetch(`${host}/restaurants/menus/meal-types/${mealTypeId}/items?${categoriesScores}`, {
        method: 'GET',
        mode: 'cors'
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.json()

        return { res, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

// /restaurants/menus/<menu_id>/global-scores

export async function getMenuGlobalScores(menuId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')

    const response = await fetch(`${host}/restaurants/menus/${menuId}/global-scores`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code == 200) {
        const res = await response.json()
        return { data: res, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export function mkMenuData(title, meal_types) {
    return { title, meal_types }
}
