export async function postRestaurant(jwt, restaurantPayload, adminId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(restaurantPayload)
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.json()
        const data = { jwt: res }

        return { data, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function getRestaurants(jwt, adminId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function getRestaurantById(restaurantId) {
    const host = process.env.API_HOST

    const response = await fetch(`${host}/restaurants/${restaurantId}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin'
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function putRestaurant(adminId, jwt, restaurantPayload, restaurantId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/${restaurantId}`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(restaurantPayload)
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.text()
        const data = { jwt: res }

        return { data, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function deleteRestaurant(adminId, jwt, restaurantId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/${restaurantId}`, {
        method: 'DELETE',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.text()
        const data = { jwt: res }

        return { data, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function postRestaurantBackgroundImage(jwt, adminId, restaurantId, image) {
    console.log(image)

    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'image/jpeg')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/background-image/${restaurantId}`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: image
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function postRestaurantLogoImage(jwt, adminId, restaurantId, image) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'image/jpeg')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/logo/${restaurantId}`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: image
    })
    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function getRestaurantsByWord(filter, from, to) {
    const host = process.env.API_HOST
    const headers = new Headers()
    headers.append('Content-Type', 'application/json')

    const response = await fetch(`${host}/restaurants/search?word=${filter}&offset=${from}&to=${to}`, {
        method: 'GET',
        mode: 'cors'
    })

    const code = response.status

    if (code === 200) {
        const res = await response.json()
        return { res, isSuccess: true }
    } else {
        const error = await response.text()
        return { error, isSuccess: false }
    }
}

export function mkPutRestaurantPayload(name, email, city, country, address, phone_number, description) {
    return { name, email, city, country, address, phone_number, description }
}

export function mkPostRestaurantPayload(name, email, city, country, address, phone_number, description) {
    return { name, email, city, country, address, phone_number, description }
}
