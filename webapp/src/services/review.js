export async function postReview(jwt, uid, itemId, reviewData) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/restaurants/menus/items/${itemId}/reviews`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(reviewData)
    })

    const code = response.status

    if (![200, 201].includes(code)) {
        const error = await response.text()
        throw error
    } else {
        return null
    }
}

export async function getLikes(reviewId) {
    const host = process.env.API_HOST
    const headers = new Headers()
    headers.append('Content-Type', 'application/json')

    const response = await fetch(`${host}/restaurants/menus/items/reviews/${reviewId}/likes`, {
        method: 'GET',
        mode: 'cors'
    })

    const code = response.status

    if (code === 200) {
        const res = await response.json()
        return { res, isSuccess: true }
    } else {
        const error = await response.text()
        return { error, isSuccess: false }
    }
}

export async function putLike(jwt, uid, reviewId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', uid)

    const response = await fetch(`${host}/restaurants/menus/items/reviews/${reviewId}/likes`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code === 200) {
        const res = await response.json()
        return { res, isSuccess: true }
    } else {
        const error = await response.text()
        return { error, isSuccess: false }
    }
}

export function mkPostReviewPayload(uid, itemId, category, score, content) {
    return { uid, itemId, category: category.toLowerCase(), score: Number(score), content }
}
