export async function postItem(jwt, itemPayload, adminId, mealTypeId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menus/meal-types/${mealTypeId}/items/add`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(itemPayload)
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.json()
        const data = { jwt: res }
        return { data, isSuccess: true }
    } else {
        const error = await response.text()
        return { error, isSuccess: false }
    }
}

export async function putItem(jwt, putItemPayload, adminId, itemId, mealTypeId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menu/meal-types/items/${mealTypeId}/${itemId}`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: JSON.stringify(putItemPayload)
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        return { isSuccess: true }
    } else {
        const error = await response.text()
        return { error, isSuccess: false }
    }
}

export async function deleteItem(adminId, jwt, itemId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menus/meal-types/items/${itemId}`, {
        method: 'DELETE',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.text()
        const data = { jwt: res }

        return { data, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function postItemImage(jwt, adminId, itemId, image) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'image/jpeg')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menu/meal-types/items/${itemId}`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers,
        body: image
    })

    const code = response.status

    if (code != 200) {
        const error = await response.text()
        throw error
    } else {
        const data = await response.json()
        return data
    }
}

export async function getItemReviews(itemId, category, offset, to) {
    const host = process.env.API_HOST
    const headers = new Headers()

    const response = await fetch(
        `${host}/restaurants/menus/items/${itemId}/reviews?category=${category}&offset=${offset}&to=${to}`,
        {
            method: 'GET',
            mode: 'cors',
            credentials: 'same-origin',
            headers
        }
    )

    const code = response.status

    if (code == 200) {
        const res = await response.json()
        return { data: res, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function getItemGlobalScores(itemId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')

    const response = await fetch(`${host}/restaurants/menus/items/${itemId}/global-scores`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code == 200) {
        const res = await response.json()
        return { data: res, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export function mkPostItemPayload(title, description, price) {
    return { title, description, price: Number(price) }
}

export function mkPutItemPayload(title, description, price, meal_type_id) {
    return { title, description, price: Number(price), meal_type_id }
}
