export async function putMealType(jwt, mealTypePayload, adminId, mealTypeId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menus/meal-types/${mealTypeId}/${mealTypePayload.title}`, {
        method: 'PUT',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        return { isSuccess: true }
    } else {
        const error = await response.text()
        return { error, isSuccess: false }
    }
}

export async function deleteMealType(adminId, jwt, mealTypeId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menus/meal-types/${mealTypeId}`, {
        method: 'DELETE',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        const res = await response.text()
        const data = { jwt: res }

        return { data, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export async function postMealType(adminId, jwt, mealTypePayload, menuId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.set('Authorization', 'Bearer ' + jwt)
    headers.set('User', adminId)

    const response = await fetch(`${host}/restaurants/menus/${menuId}/meal-types/add/${mealTypePayload.title}`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if ([200, 201].includes(code)) {
        return { isSuccess: true }
    } else {
        const error = await response.text()
        return { error, isSuccess: false }
    }
}

export async function getMealtypeGlobalScores(mealTypeId) {
    const host = process.env.API_HOST
    const headers = new Headers()

    headers.append('Content-Type', 'application/json')

    const response = await fetch(`${host}/restaurants/menus/meal-types/${mealTypeId}/global-scores`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'same-origin',
        headers
    })

    const code = response.status

    if (code == 200) {
        const res = await response.json()
        return { data: res, isSuccess: true }
    } else {
        const error = await response.text()

        return { error, isSuccess: false }
    }
}

export function mkPutMealType(title) {
    return { title }
}
export function mkPostMealType(title) {
    return { title }
}
