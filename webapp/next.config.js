/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    env: {
        API_HOST: 'http://127.0.0.1:8082'
    },
    images: {
        remotePatterns: [
            {
                protocol: 'http',
                hostname: '**'
            }
        ]
    }
}

module.exports = nextConfig
