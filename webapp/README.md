## Install dependencies

```
npm install
```

## Run in development mode

```
npm run dev

```

## Fix style and linter

```
npm run style
npm run lint

```
