module.exports = {
    tabWidth: 4,
    printWidth: 120,
    endOfLine: 'lf',
    arrowParens: 'always',
    trailingComma: 'none',
    semi: false,
    useTabs: false,
    singleQuote: true,
    bracketSpacing: true
}
