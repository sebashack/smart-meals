#!/bin/bash

set -xeuf -o pipefail

source "$HOME/.cargo/env"
export SQLX_OFFLINE=true

make prod-webapp
make check-style
# make tests
make package
