#!/bin/bash

export ROOT="$( readlink -f "$( dirname "${BASH_SOURCE[0]}" )" )"

rm -rf ${ROOT}/backend/dist
rm -rf ${ROOT}/frontend/webapp

cd ${ROOT}/..

make build-dist

mv _build/dist ${ROOT}/backend

rm -rf webapp/.next && rm -rf webapp/node_modules

cp -r webapp ${ROOT}/frontend/

cd -
