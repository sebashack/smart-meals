#!/bin/bash

set -xeuf -o pipefail

ROOT="$( readlink -f "$( dirname "${BASH_SOURCE[0]}" )" )"
DOCKER_FILE="${ROOT}/docker-compose.yaml"

mkdir -p ${ROOT}/app-data

export BACKEND_CONFIG="${ROOT}/app-data/Rocket.toml"
export FRONTEND_CONFIG="${ROOT}/app-data/next.config.js"
export PSQL_DATA_DIR="${ROOT}/app-data/data"
export NGINX_CONFIG="${ROOT}/app-data/nginx.conf"
export LETSENCRYPT_DIR="${ROOT}/app-data/letsencrypt"

if [[ $1 == "up" ]]; then
    docker-compose --file "$DOCKER_FILE" up --detach
fi

if [[ $1 == "down" ]]; then
    docker-compose --file "$DOCKER_FILE" down
fi
