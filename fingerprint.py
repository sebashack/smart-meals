#!/usr/bin/python3

from __future__ import print_function
import os
import sys
import argparse
import hashlib


def is_exe(fpath):
    return os.access(fpath, os.X_OK)


def md5(fname):
    hash = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash.update(chunk)
    return hash.hexdigest()


def string_hash(string):
    m = hashlib.md5()
    m.update(string)
    return m.hexdigest()


def fingerprintDirectory(maybe_relative_directory):
    directory = os.path.realpath(maybe_relative_directory)

    assert os.path.exists(directory), "Input directory `{}` does not exist".format(directory)

    contentsAndHashExec = []
    for root, dirs, files in os.walk(directory):
        contentsAndHashExec.append((root, None))
        for f in files:
            fileName = os.path.join(root, f)
            fileHash = md5(fileName)
            isExecutable = is_exe(fileName)
            contentsAndHashExec.append((fileName, (fileHash, isExecutable)))

    def getFileName(entry):
        fileName = entry[0]
        if fileName.startswith(directory):
            return fileName[len(directory):]
        else:
            raise Exception("programming error")
    sortedcontentsAndHashExec = sorted(contentsAndHashExec, key=getFileName)

    contentsLines = list(map(getFileName, sortedcontentsAndHashExec))
    assert contentsLines[0] == "", "First entry of contents should be package root"

    def dropLeadingSlash(fileName):
        if fileName.startswith("/"):
            return fileName[1:]
        else:
            raise Exception("programming error")
    contentsLines = map(dropLeadingSlash, contentsLines[1:])
    contents = '\n'.join(contentsLines) + '\n'

    contentsHash = string_hash(contents.encode('utf-8'))

    hashesAndExecLines = []
    for entry in sortedcontentsAndHashExec:
        maybeHashAndExec = entry[1]
        if maybeHashAndExec is not None:
            (hash_string, is_exec) = maybeHashAndExec
            if is_exec:
                hashesAndExecLines.append(hash_string + ",x")
            else:
                hashesAndExecLines.append(hash_string)

    hashes = '\n'.join([contentsHash] + hashesAndExecLines) + '\n'

    sys.stdout.write(string_hash(hashes.encode('utf-8')))


def main(argv):
    parser = argparse.ArgumentParser(description="Computes directory fingerprint")

    parser.add_argument('directory', type=str,
                        help="Path to directory to fingerprint")

    args = parser.parse_args(argv[1:])

    return fingerprintDirectory(getattr(args, 'directory'))

if __name__ == '__main__':
    sys.exit(main(sys.argv))
