#!/bin/bash

set -xeuf -o pipefail

if [[ -v CI ]]; then
    SUDO=""
    apt-get update
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata
    echo "en_US UTF-8" > /etc/locale.gen
else
    SUDO="sudo"
fi

# Install Rust
${SUDO} apt update
${SUDO} apt-get install -y curl build-essential tar lzip libpq-dev python3 openssl pkg-config

curl --proto '=https' --tlsv1.3 https://sh.rustup.rs -sSf | sh -s -- -y
source "$HOME/.cargo/env"

rustup self update
rustup default nightly
cargo install sqlx-cli --version 0.6.3 --no-default-features --features native-tls,postgres

# Install NodeJS
curl -sL https://deb.nodesource.com/setup_18.x -o nodesource_setup.sh
${SUDO} bash nodesource_setup.sh
${SUDO} apt-get install nodejs
rm -f nodesource_setup.sh

# Install docker
if ! type "docker" > /dev/null; then
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | ${SUDO} gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | ${SUDO} tee /etc/apt/sources.list.d/docker.list > /dev/null

    ${SUDO} apt update

    apt-cache policy docker-ce

    if [[ -v CI ]]; then
        echo "Skipping creation of docker group"
    else
        ${SUDO} apt install -y docker-ce
        ${SUDO} usermod -aG docker ${USER}
    fi
else
    echo 'docker already installed'
fi

# Install docker-compose
if ! type "docker-compose" > /dev/null; then
    ${SUDO} curl -SL https://github.com/docker/compose/releases/download/v2.3.3/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
    ${SUDO} chmod +x /usr/local/bin/docker-compose
else
    echo 'docker-compose already installed'
fi
